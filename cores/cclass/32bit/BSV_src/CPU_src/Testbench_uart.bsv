package Testbench_uart;
import Vector       :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import Connectable  :: *;
import StmtFSM      :: *;
import List         ::*;
import myRS232      ::*;
import GetPut       ::*;
import ahb  ::*;

function Bit#(32) convertString (String str);
  Bit#(32) ascii=0;
  for(Integer i=0;i<length(stringToCharList(str));i=i+1)begin
    ascii={ascii[23:0],fromInteger(charToInteger(stringToCharList(str)[i]))};
  end 
  return ascii;
endfunction


(*synthesize*)
module mkTestbench_uart(Empty);
  Intfc_top pc <-mkahb();
  UART#(3) uart0 <-mkUART(8);
  Reg#(Bit#(32)) length_of_string <-mkReg(0);
  Reg#(Bit#(40)) rg_input_command<-mkReg({convertString("DREG"),'d13});
  rule connect_uart0_and_core;
    uart0.parity_sel(NONE);
    uart0.stopbits_sel(STOP_1);
    uart0.divider('d5); // baud count value
  endrule

  rule transmit_command;
    if(length_of_string<5)begin // this rule will put 8 characters into the UART transmission fifo.
      $display("Sending Character: %s from TB",rg_input_command[39:32]);
      uart0.rx.put(rg_input_command[39:32]);
      length_of_string<=length_of_string+1;
      rg_input_command<=rg_input_command<<8;
    end
//    else begin
//      length_of_string<=0;
//      rg_input_command<=({convertString(""),'d13});
//    end
  endrule

  rule rl_wait_to_recieve_data_from_uart;
      let data_recv<-uart0.tx.get;
      $display($time,"\tTB: Character:%s",data_recv);
      uart0.dequeue_rx_fifo();
  endrule

  rule rx_line;
    uart0.rs232.sin(pc.sout);
  endrule
  rule tx_line;
    pc.sin(uart0.rs232.sout);
  endrule


  

endmodule
endpackage
