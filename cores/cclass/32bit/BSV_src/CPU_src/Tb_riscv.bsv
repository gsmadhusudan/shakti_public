package Tb_riscv;
	import defined_types::*;
	`include "defined_parameters.bsv"
	import riscv::*;
	import RegFile::*;
	module mkTb_riscv(Empty);
		
      RegFile#(Bit#(TSub#(`Addr_space,2)), Bit#(32)) dmem <-mkRegFileFullLoad("./code.hex");

			Ifc_riscv riscv <-mkriscv();
  
      rule read_instruc_req_from_proc(riscv._address_instruction_out matches tagged Valid .addr);
					Bit#(2) burst_size =3;
          let data0 = dmem.sub(addr[`Addr_space-1:2]);
					Bit#(32) data=data0;
          // Making A response TLM packet
            if(burst_size=='d3) // word transfer
              data = (zeroExtend(data0));
            else if (burst_size=='d1)begin // half_word
              if(addr[1:0] ==0)
                data = (zeroExtend(data0[15:0]));
              else if(addr[1:0] ==2)
                data = (zeroExtend(data0[31:16]));
            end
            else if (burst_size=='d0) begin// one byte
              if(addr[1:0] ==0)
                data = (zeroExtend(data0[7:0]));
              else if(addr[1:0] ==1)
                data = (zeroExtend(data0[15:8]));
              else if(addr[1:0] ==2)
                data = (zeroExtend(data0[23:16]));
              else if(addr[1:0] ==3)
                data = (zeroExtend(data0[31:24]));
            end
						riscv._instruction_in(tagged Valid data);
            $display($time,"	 Main Mem : Received single transaction request from D-cache READ for address : %h Size : %d sending data : %h data0:%h",addr,burst_size,data,data0,$time);
      endrule

      rule read_data_request_from_proc(riscv._address_data_out matches tagged Valid .addr);
          let data0 = dmem.sub(addr[`Addr_space-1:2]);
          // Making A response TLM packet
					Bit#(2) burst_size =(riscv.byte_halfword_word_==2)?3:riscv.byte_halfword_word_;
          if(riscv._data_out matches tagged Invalid)begin // dmem read request.
						Bit#(32) data=data0;
            if(burst_size=='d3) // word transfer
              data = (zeroExtend(data0));
            else if (burst_size=='d1)begin // half_word
              if(addr[1:0] ==0)
                data = (zeroExtend(data0[15:0]));
              else if(addr[1:0] ==2)
                data = (zeroExtend(data0[31:16]));
            end
            else if (burst_size=='d0) begin// one byte
              if(addr[1:0] ==0)
                data = (zeroExtend(data0[7:0]));
              else if(addr[1:0] ==1)
                data = (zeroExtend(data0[15:8]));
              else if(addr[1:0] ==2)
                data = (zeroExtend(data0[23:16]));
              else if(addr[1:0] ==3)
                data = (zeroExtend(data0[31:24]));
            end
            $display($time,"	 Main Mem : Received single transaction request from D-cache READ for address : %h Size : %d sending data : %h data0:%h",addr,burst_size,data,data0,$time);
						riscv._data_in(tagged Valid data);
          end
          else if(riscv._data_out matches tagged Valid .data) begin // dmem write request.
            Bit#(32) new_data=0;
            if( burst_size=='d3)begin // word transfer
              new_data=data[31:0];
            end
            else if (burst_size=='d1)begin // half_word
               if(addr[1:0] ==0)
                  new_data={data0[31:16],data[15:0]}; 
               else if(addr[1:0] ==1)
                  new_data={data0[31:24],data[15:0],data0[7:0]}; 
               else if(addr[1:0] ==2)
                  new_data={data[15:0],data0[15:0]}; 
            end
            else if (burst_size=='d0)begin // one byte
               if(addr[1:0] ==0)
                  new_data={data0[31:8],data[7:0]}; 
               else if(addr[1:0] ==1)
                  new_data={data0[31:16],data[7:0],data0[7:0]}; 
               else if(addr[1:0] ==2)
                  new_data={data0[31:24],data[7:0],data0[15:0]}; 
               else if(addr[1:0] ==3)
                  new_data={data[7:0],data0[23:0]}; 
            end
            dmem.upd(addr[`Addr_space-1:2],new_data);
						riscv._data_in(tagged Valid new_data);
            $display($time," Main Mem : Received request from D-cache Write for address : %h Size : %d sending data : %h",addr, burst_size,new_data);
          end
      	endrule
	endmodule
endpackage
