/*Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Module Name: Tb_integer_multiplier_riscv
Author Name: Rishi Naidu
Email id:    rishinaidu.rn@gmail.com
Last updated on : 29th October 2013

This is a parameterized bit integer divider that completes the division operation in 34 cycles for 64 by 64 division and 18 cycles for 32 by 32 division using "Non Restoring Division Algorithm". 
Here the divider is implemented in form of a state machine, which a CPI of 34 cycles. Using a state machine instead of Pipelining helps to decrease the area by huge amount.
Here SIZEDFIFO'S are used as mkFIFO gives a FIFO which can have two inputs and hence deq and enq in same cycles. So mkFIFO is good for debugging, but to have a rigid code its better to use mkSizedFIFO

Change the parameters in "defined_parameters.bsv" as per the requirement.
*****For RV64 Instructions*******
set Reg_width==64
and define divider64 (there should be no define divider32 when executing RV64)
*****For RV32 Instructions*******
set Reg_width==32
and define divider32 (there shouldb be no define divider64 when executing RV32)

Comparing with Intel's Haswell and Meron(65nm) which takes 32-96 and 29-61 cycles for execution of 64bit divider, we implemented a simple NON RESTORING DIVIDER which takes 34 cycles for RV64 division instructions and 18 cycles for RV32 instructions 

***** Special Cases******
Special cases as mentioned in riscv2.0 ISA for Division and remainder  and a case when the dividend<divisor is taken care of in the first stage(method _start) and hence takes only 1 cylce for execution

***** Performance*********
Using UMCIP 65nm library in SYNOPSYS
Critical Path Length    :    0.5679 ns
Frequency 	        :    1.724 GHz
Combinational Cell Count:    10169
Sequential Cell Count   :    559

Critical path : Stage3

The main 4 stage of divider of which stage3 undergoes recursive execution which helps to save the area

************Stage1**************
It's the start stage. Input is dividend, divisor, div_type and word_flag. div_type helps to detemine the type of division and remainder instruction. word_flag help to detect if its a word instruction. In this stage final dividend and divisor are determined to be carried out for unsigned division. This stage pass the dividend,divisor to be used in next stage and div_type, word_flag, dividend_sign_bit and divisor_sign_bit to be used in final stage to evaluate the final output

************Stage2**************
This stage is a buffer stage which gets the input from FIFO.A buffer stage is introduced to reset to inital stage once the division is completed and hence deq the FIFO.As stage 3 uses a register in place of FIFO (which has implicit condition) we need a buffer to avoid conflict from next instruction.
In a non restoring division of n bit number it takes n steps. In this stage we perform 2 steps and pass the output which is dividend, divisor, partial_remainder and divisor 2's complement to the next stage. Along with it we pass the div_type,word_flag, dividend_sign_bit and divisor_sign_bit to be used in last stage. All these are store in register

************Stage3**************
This is a recursive stage which is executed 31 times for 64 bit number and 15 times for 32 bit number and hence complete remaining steps of non restoring division. The output after each execution is stored in rg_stage. This rg_stage is again read in next cycle.
The final output dividend, divisor, divisor_2's_complement, div_type,word_flag,dividend_sign_bit,divisor_sign_bit,partial_rem are stored in rg_stage which is finally read in the stage4.

************Stage4**************
THis is the final stage.The output from unsigned division is obtained from the rg_state. This output is then changed based on the division or remainder type to get the final quotient and remainder.div_type, word_flag,dividend_sign_bit,divisor_sign_bit is used to get the final output. Final quotiend and remainder is stored in ff_quotien and ff_remainder respectively

*/
package integer_divider_riscv;
import FIFO::*;
import DReg::*;
import SpecialFIFOs ::*;
import DReg::*;

`define REG_WIDTH 32
`define divider32
`define SIZE 34
`define Loop 2 



interface Ifc_integer_divider_riscv;

   /* Input Methods */
   
		 method Action _start(Bit#(`REG_WIDTH) inp1, Bit#(`REG_WIDTH) inp2,Bit#(1) _div_type, Bit#(1) _div_or_rem, Bit#(1) mul_or_div); //_div_name 00 : DIV/REM 01: DIVU/REMU
	  /* Output Methods */
   method Maybe#(Bit#(`REG_WIDTH)) result_();
   

endinterface:Ifc_integer_divider_riscv

  function Bit#(TAdd#(1,TMul#(2,34))) single_step(Bit#(TAdd#(1,34)) p, Bit#(34) a, Bit#(34) d);
    for (Integer i=0;i<`Loop;i=i+1)begin
       let p_x1= {p,a}<<1;
       let p_x2=p_x1;
       Bit#(TAdd#(1,34)) y = p_x1[2*34:34] + signExtend(~d+1);
       p_x2[2*34:34]=y;
       if(p_x2[2*34]==1)begin // restore
         p= p_x1[2*34:34];
         a= p_x1[34-1:0];
       end
       else begin
         p= p_x2[2*34:34]; // do not restore
         a= {p_x2[34-1:1],1'b1};
       end
    end
    return {p,a};
  endfunction


(*synthesize*)
module mkinteger_divider_riscv(Ifc_integer_divider_riscv);
   
   //FIFO#(Bit#(`REG_WIDTH)) ff_final_result <- mkBypassFIFO(); 					//FIFO to store the final quotient 
   Reg#(Maybe#(Bit#(`REG_WIDTH))) wr_final_result <- mkDReg(tagged Invalid); 					//FIFO to store the final quotient 
   Reg#(Bit#(1)) rg_mul_or_div <-mkReg(0);    // 0 = MUL* 1 = DIV/DIVU/REM/REMU
   Reg#(int) rg_state_counter <-mkDReg(0);  								//Register for state machine counter

   Reg#(Bit#(34)) rg_inp1 <-mkReg(0);
   Reg#(Bit#(34)) rg_inp2 <-mkReg(0);
   Reg#(Bit#(1)) rg_inp1_sign <-mkReg(0);
   Reg#(Bit#(1)) rg_inp2_sign <-mkReg(0);
   Reg#(Bit#(1)) div_or_rem <-mkReg(0);
   Reg#(Bit#(1)) div_type <-mkReg(0);
   Reg#(Bit#(1)) rg_take_complement <-mkReg(0);
   Reg#(Bit#(TAdd#(1,(TMul#(`REG_WIDTH,2))))) partial_prod <-mkReg(0);
   Reg#(Bit#(TAdd#(1,TMul#(1,34)))) partial <-mkReg(0);

     rule division(rg_mul_or_div==1 && rg_state_counter<=(34/`Loop)-1 && rg_state_counter!=0);
        let temp = single_step(partial,rg_inp1,rg_inp2);
        if(rg_state_counter==(34/`Loop)-1)begin
          partial<=0;
          Bit#(TSub#(34,2)) x= temp[2*34-3:34];
          Bit#(TSub#(34,2)) y= temp[34-3:0];
          if(div_or_rem==0) // DIV/DIVU
            if(rg_take_complement==1)begin
              wr_final_result<= tagged Valid (~y+1);
              $display("Taking Complement");
            end
            else begin
              wr_final_result<= tagged Valid (y);
              $display("NOT Taking Complement");
            end
          else // REM/REMU
            if(div_type==0 && x[31]!=rg_inp1_sign)
              wr_final_result<= tagged Valid (~x+1);
            else
              wr_final_result<= tagged Valid (x);
          rg_state_counter<=0;
          Bit#(34) z1= temp[2*34-1:34];
          Bit#(34) z2= temp[34-1:0];
          $display($time,"\tRemainder:%b Quotient: %b",z1,z2);
        end
        else begin
          partial<=temp[2*34:34];
          rg_inp1<=temp[34-1:0];
          Bit#(34) x= temp[2*34-1:34];
          Bit#(34) y= temp[34-1:0];
          $display($time,"\tRemainder:%b Quotient: %b",x,y);
          rg_state_counter<=rg_state_counter+1;
        end
     endrule

     rule multiply(rg_mul_or_div==0 && rg_state_counter<=32 && rg_state_counter!=0 );
       Bit#(TAdd#(1,(TMul#(2,`REG_WIDTH)))) temp = partial_prod;
       if(temp[1:0]=='b01)begin
         temp[64:32]=zeroExtend(temp[63:32])+truncate(rg_inp1);
         temp=temp>>2;
         partial_prod<=temp;
       end
       else if(temp[1:0]=='b10)begin
         temp=temp>>1;
         temp[64:32]=zeroExtend(temp[63:32])+truncate(rg_inp1);
         temp=temp>>1;
         partial_prod<=temp;
       end
       else if(temp[1:0]=='b11)begin
         temp[64:32]=zeroExtend(temp[63:32])+truncate(rg_inp1);
         temp=temp>>1;
         temp[64:32]=zeroExtend(temp[63:32])+truncate(rg_inp1);
         temp=temp>>1;
         partial_prod<=temp;
       end
       else begin
        temp=temp>>2;
        partial_prod<=temp;
       end
       if(rg_state_counter==16)begin
         rg_state_counter<=0;
         if(div_or_rem==0 && div_type ==0)// return the lower bits
            wr_final_result<= tagged Valid truncate(temp);
         else begin
            bit lv_take_complement=0;
            if(div_type==1 && div_or_rem==0)// MULH
							lv_take_complement = (rg_inp1_sign ^ rg_inp2_sign)==1 ? 1:0 ;	
            else if(div_type==0 && div_or_rem==1)// MULHSU
							lv_take_complement = (rg_inp1_sign==1) ? 1:0 ;	

            Bit#(TMul#(2,`REG_WIDTH)) final_result= (lv_take_complement==1)? truncate(~temp+1) : truncate(temp);

            wr_final_result<= tagged Valid final_result[2*`REG_WIDTH-1:`REG_WIDTH];
         end
       end
       else
        rg_state_counter<=rg_state_counter+1;

       // $display("Counter: %d, Output :%h",rg_state_counter,temp);
     endrule

		 
		 
		 //Word_flag is 1 only for the word RV64 instructions else it is 0.
		 //Non restoring division algorithm
		 //Stage1 (start method)
		 method Action _start(Bit#(`REG_WIDTH) inp1, Bit#(`REG_WIDTH) inp2,Bit#(1) _div_type, Bit#(1) _div_or_rem, Bit#(1) mul_or_div)if(rg_state_counter==0); //_div_name 00 : DIV/REM 01: DIVU/REMU
			
		 	// $display("start stage");
			
			rg_mul_or_div<=mul_or_div; 
      div_or_rem<=_div_or_rem;
      div_type<=_div_type;
      rg_inp1_sign<=inp1[`REG_WIDTH-1];
      rg_inp2_sign<=inp2[`REG_WIDTH-1];
      if(mul_or_div==1)begin // division operation
        if(_div_type==0)begin
          if(_div_or_rem==0 && (inp1[31]^inp2[31])==1 && inp2!=0)
            rg_take_complement<=1;
          else
            rg_take_complement<=0;
          inp1=inp1[31]==1?~inp1+1:inp1;
          inp2=inp2[31]==1?~inp2+1:inp2;
        end
        else 
          rg_take_complement<=0;
        rg_inp2<=zeroExtend(inp2);

        $display("Division selected: Op1: %h, Op2: %h",inp1,inp2);
        let temp = single_step(partial,zeroExtend(inp1),zeroExtend(inp2));
        partial<=temp[2*34:34];
        rg_inp1<=temp[34-1:0];
          Bit#(34) x= temp[2*34-1:34];
          Bit#(34) y= temp[34-1:0];
          $display($time,"\tRemainder:%b Quotient: %b",x,y);
        rg_state_counter<=rg_state_counter+1;
      end
      else begin // Multiplier operation 
        // $display("Multiplication selected: Op1: %h, Op2: %h",inp1,inp2);
        rg_inp2<=zeroExtend(inp2);
        if(_div_type==1 && _div_or_rem==0)begin// MULH
          rg_inp1<=(inp1[`REG_WIDTH-1]==1)?zeroExtend(~inp1+1):zeroExtend(inp1);
          Bit#(`REG_WIDTH) temp = (inp2[`REG_WIDTH-1]==1)?~inp2+1:inp2;
          partial_prod<=zeroExtend(temp);
        end
        else if(_div_type==0 && _div_or_rem==1)begin// MULHSU
          rg_inp1<=(inp1[`REG_WIDTH-1]==1)?zeroExtend(~inp1+1):zeroExtend(inp1);
          partial_prod<=zeroExtend(inp2);
        end
        else begin
          partial_prod<=zeroExtend(inp2);
          rg_inp1<=zeroExtend(inp1);
        end
        rg_state_counter<=rg_state_counter+1;
			   
		  end 
		 
		 endmethod					
		 
		 /*
		 method Bit#(`REG_WIDTH) get_quotient_();   //Method to get the result
		 return ff_final_quotient.first();
		 endmethod

		 method Bit#(`REG_WIDTH) get_remainder_();   //Method to get the result
		 return ff_final_remainder.first();
		 endmethod
		 */
   method Maybe#(Bit#(`REG_WIDTH)) result_();
			return wr_final_result;
		 endmethod
endmodule : mkinteger_divider_riscv


(*synthesize*)
module mkTb();
Reg#(Bit#(32)) rg_clock <- mkReg(0);

Ifc_integer_divider_riscv inst <- mkinteger_divider_riscv();

rule rl_count_clock;
	rg_clock <= rg_clock + 1;
	// $display("Clock=%d", rg_clock);
	if(rg_clock == 'd25) 
		$finish(0);
endrule

rule rl_input(rg_clock==1);
    inst._start(32'hfffffb3b,32'hfffffb3b,'b1,0,1);
    // inst._start(32'h00000002,32'h00000002,'b1,1,1);
endrule

rule rl_check;
	if(inst.result_() matches tagged Valid .instr) begin
	let lv_result = instr;
	$display("Q=%h",lv_result);
end
endrule


endmodule


endpackage: integer_divider_riscv
