
start.riscv:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <main>:
   0:	0001f137          	lui	sp,0x1f
   4:	00115113          	srli	sp,sp,0x1
   8:	7ff16113          	ori	sp,sp,2047
   c:	1d4000ef          	jal	1e0 <help>

00000010 <Save_Template_Commands>:
  10:	0000e337          	lui	t1,0xe
  14:	44524237          	lui	tp,0x44524
  18:	54726213          	ori	tp,tp,1351
  1c:	00432023          	sw	tp,0(t1) # e000 <_end+0xdae6>
  20:	00000263          	beqz	zero,24 <Init1>

00000024 <Init1>:
  24:	02000713          	li	a4,32
  28:	00d00793          	li	a5,13
  2c:	00000893          	li	a7,0
  30:	00000c13          	li	s8,0
  34:	00000e93          	li	t4,0
  38:	00200c93          	li	s9,2

0000003c <Start>:
  3c:	00300a93          	li	s5,3
  40:	00300813          	li	a6,3
  44:	060000ef          	jal	a4 <ReadCommand>

00000048 <Entry1>:
  48:	00000593          	li	a1,0
  4c:	00068593          	mv	a1,a3
  50:	0dd89a63          	bne	a7,t4,124 <Compare>
  54:	00700a93          	li	s5,7
  58:	00200813          	li	a6,2
  5c:	048000ef          	jal	a4 <ReadCommand>

00000060 <Entry2>:
  60:	00068993          	mv	s3,a3
  64:	0dd89063          	bne	a7,t4,124 <Compare>
  68:	00200a93          	li	s5,2
  6c:	00200813          	li	a6,2
  70:	034000ef          	jal	a4 <ReadCommand>

00000074 <Entry3>:
  74:	00068a13          	mv	s4,a3
  78:	00068913          	mv	s2,a3
  7c:	0e0000ef          	jal	15c <HextoDec>
  80:	000c0913          	mv	s2,s8
  84:	0bd89063          	bne	a7,t4,124 <Compare>
  88:	00700a93          	li	s5,7
  8c:	00200813          	li	a6,2
  90:	00000c13          	li	s8,0
  94:	01000def          	jal	s11,a4 <ReadCommand>

00000098 <Entry4>:
  98:	00068a13          	mv	s4,a3
  9c:	09d89463          	bne	a7,t4,124 <Compare>
  a0:	08000263          	beqz	zero,124 <Compare>

000000a4 <ReadCommand>:
  a4:	ffc10113          	addi	sp,sp,-4 # 1effc <_end+0x1eae2>
  a8:	00112223          	sw	ra,4(sp)
  ac:	77e02673          	csrr	a2,0x77e
  b0:	10067413          	andi	s0,a2,256
  b4:	fe0408e3          	beqz	s0,a4 <ReadCommand>
  b8:	0ff67613          	andi	a2,a2,255
  bc:	02e60c63          	beq	a2,a4,f4 <Check_for_end>
  c0:	02f60a63          	beq	a2,a5,f4 <Check_for_end>
  c4:	05981463          	bne	a6,s9,10c <Store>
  c8:	00400393          	li	t2,4
  cc:	00000213          	li	tp,0
  d0:	00060213          	mv	tp,a2
  d4:	0f027213          	andi	tp,tp,240
  d8:	00425213          	srli	tp,tp,0x4
  dc:	00725663          	ble	t2,tp,e8 <alpha3>
  e0:	00f67613          	andi	a2,a2,15
  e4:	02000463          	beqz	zero,10c <Store>

000000e8 <alpha3>:
  e8:	00960613          	addi	a2,a2,9
  ec:	00f67613          	andi	a2,a2,15
  f0:	00000e63          	beqz	zero,10c <Store>

000000f4 <Check_for_end>:
  f4:	00000c13          	li	s8,0
  f8:	00f61463          	bne	a2,a5,100 <Exit_ReadCommand>
  fc:	00100e93          	li	t4,1

00000100 <Exit_ReadCommand>:
 100:	00412083          	lw	ra,4(sp)
 104:	00410113          	addi	sp,sp,4
 108:	00008067          	ret

0000010c <Store>:
 10c:	010a9b33          	sll	s6,s5,a6
 110:	01661bb3          	sll	s7,a2,s6
 114:	017c0c33          	add	s8,s8,s7
 118:	000c0693          	mv	a3,s8
 11c:	fffa8a93          	addi	s5,s5,-1
 120:	f80002e3          	beqz	zero,a4 <ReadCommand>

00000124 <Compare>:
 124:	0000e337          	lui	t1,0xe
 128:	00032483          	lw	s1,0(t1) # e000 <_end+0xdae6>
 12c:	08958e63          	beq	a1,s1,1c8 <DREG>
 130:	00432483          	lw	s1,4(t1)
 134:	00832483          	lw	s1,8(t1)
 138:	00c32483          	lw	s1,12(t1)
 13c:	01032483          	lw	s1,16(t1)
 140:	01432483          	lw	s1,20(t1)
 144:	01832483          	lw	s1,24(t1)
 148:	08958463          	beq	a1,s1,1d0 <HELP>
 14c:	01c32483          	lw	s1,28(t1)
 150:	02032483          	lw	s1,32(t1)
 154:	06d68e63          	beq	a3,a3,1d0 <HELP>
 158:	ec0006e3          	beqz	zero,24 <Init1>

0000015c <HextoDec>:
 15c:	ffc10113          	addi	sp,sp,-4
 160:	00112223          	sw	ra,4(sp)
 164:	00a00d13          	li	s10,10
 168:	00000e13          	li	t3,0
 16c:	00f97b13          	andi	s6,s2,15
 170:	0f097b93          	andi	s7,s2,240
 174:	004bdb93          	srli	s7,s7,0x4
 178:	00000c13          	li	s8,0

0000017c <AddTen>:
 17c:	017c0c33          	add	s8,s8,s7
 180:	001e0e13          	addi	t3,t3,1
 184:	ffae1ce3          	bne	t3,s10,17c <AddTen>
 188:	016c0c33          	add	s8,s8,s6
 18c:	00000e13          	li	t3,0
 190:	06400d13          	li	s10,100
 194:	0000fd37          	lui	s10,0xf
 198:	004d5d13          	srli	s10,s10,0x4
 19c:	01a97bb3          	and	s7,s2,s10
 1a0:	008bdb93          	srli	s7,s7,0x8
 1a4:	00000b13          	li	s6,0
 1a8:	06400d13          	li	s10,100

000001ac <AddHundred>:
 1ac:	017b0b33          	add	s6,s6,s7
 1b0:	001e0e13          	addi	t3,t3,1
 1b4:	ffae1ce3          	bne	t3,s10,1ac <AddHundred>
 1b8:	016c0c33          	add	s8,s8,s6
 1bc:	00412083          	lw	ra,4(sp)
 1c0:	00410113          	addi	sp,sp,4
 1c4:	00008067          	ret

000001c8 <DREG>:
 1c8:	068000ef          	jal	230 <display_register>
 1cc:	e4000ce3          	beqz	zero,24 <Init1>

000001d0 <HELP>:
 1d0:	010000ef          	jal	1e0 <help>
 1d4:	e40008e3          	beqz	zero,24 <Init1>

000001d8 <exit>:
 1d8:	0000006f          	j	1d8 <exit>
 1dc:	0000                	unimp
	...

000001e0 <help>:
 1e0:	ff010113          	addi	sp,sp,-16
 1e4:	00d12823          	sw	a3,16(sp)
 1e8:	00c12623          	sw	a2,12(sp)
 1ec:	00b12423          	sw	a1,8(sp)
 1f0:	00712223          	sw	t2,4(sp)
 1f4:	00112023          	sw	ra,0(sp)
 1f8:	00000697          	auipc	a3,0x0
 1fc:	1b868693          	addi	a3,a3,440 # 3b0 <str1>
 200:	02400593          	li	a1,36

00000204 <rep>:
 204:	00068603          	lb	a2,0(a3)
 208:	00c58e63          	beq	a1,a2,224 <exit_help>

0000020c <rep1>:
 20c:	77f023f3          	csrr	t2,0x77f
 210:	1003f393          	andi	t2,t2,256
 214:	fe038ce3          	beqz	t2,20c <rep1>
 218:	77f61073          	csrw	0x77f,a2
 21c:	00168693          	addi	a3,a3,1
 220:	fea502e3          	beq	a0,a0,204 <rep>

00000224 <exit_help>:
 224:	00012083          	lw	ra,0(sp)
 228:	01010113          	addi	sp,sp,16
 22c:	00008067          	ret

00000230 <display_register>:
 230:	00012023          	sw	zero,0(sp)
 234:	fe112e23          	sw	ra,-4(sp)
 238:	fe212c23          	sw	sp,-8(sp)
 23c:	fe312a23          	sw	gp,-12(sp)
 240:	fe412823          	sw	tp,-16(sp)
 244:	fe512623          	sw	t0,-20(sp)
 248:	fe612423          	sw	t1,-24(sp)
 24c:	fe712223          	sw	t2,-28(sp)
 250:	fe812023          	sw	s0,-32(sp)
 254:	fc912e23          	sw	s1,-36(sp)
 258:	fca12c23          	sw	a0,-40(sp)
 25c:	fcb12a23          	sw	a1,-44(sp)
 260:	fcc12823          	sw	a2,-48(sp)
 264:	fcd12623          	sw	a3,-52(sp)
 268:	fce12423          	sw	a4,-56(sp)
 26c:	fcf12223          	sw	a5,-60(sp)
 270:	fd012023          	sw	a6,-64(sp)
 274:	fb112e23          	sw	a7,-68(sp)
 278:	fb212c23          	sw	s2,-72(sp)
 27c:	fb312a23          	sw	s3,-76(sp)
 280:	fb412823          	sw	s4,-80(sp)
 284:	fb512623          	sw	s5,-84(sp)
 288:	fb612423          	sw	s6,-88(sp)
 28c:	fb712223          	sw	s7,-92(sp)
 290:	fb812023          	sw	s8,-96(sp)
 294:	f9912e23          	sw	s9,-100(sp)
 298:	f9a12c23          	sw	s10,-104(sp)
 29c:	f9b12a23          	sw	s11,-108(sp)
 2a0:	f9c12823          	sw	t3,-112(sp)
 2a4:	f9d12623          	sw	t4,-116(sp)
 2a8:	f9e12423          	sw	t5,-120(sp)
 2ac:	f9f12223          	sw	t6,-124(sp)
 2b0:	02000693          	li	a3,32
 2b4:	00010713          	mv	a4,sp
 2b8:	00400513          	li	a0,4
 2bc:	00000393          	li	t2,0
 2c0:	00800493          	li	s1,8
 2c4:	00a00a93          	li	s5,10
 2c8:	00000e93          	li	t4,0
 2cc:	00000c93          	li	s9,0
 2d0:	00400f93          	li	t6,4

000002d4 <main_subr>:
 2d4:	00400f93          	li	t6,4
 2d8:	03a00913          	li	s2,58
 2dc:	001e8e93          	addi	t4,t4,1
 2e0:	00600993          	li	s3,6
 2e4:	00000b93          	li	s7,0
 2e8:	000c8593          	mv	a1,s9
 2ec:	00000e13          	li	t3,0

000002f0 <loopr>:
 2f0:	01f5da33          	srl	s4,a1,t6
 2f4:	ffcf8f93          	addi	t6,t6,-4
 2f8:	00fa7a13          	andi	s4,s4,15
 2fc:	015a5663          	ble	s5,s4,308 <alpha1r>
 300:	030a0a13          	addi	s4,s4,48
 304:	00a50663          	beq	a0,a0,310 <printr>

00000308 <alpha1r>:
 308:	057a0a13          	addi	s4,s4,87
 30c:	00a50263          	beq	a0,a0,310 <printr>

00000310 <printr>:
 310:	08800f6f          	jal	t5,398 <waitr>
 314:	77fa1073          	csrw	0x77f,s4
 318:	00198993          	addi	s3,s3,1
 31c:	fc999ae3          	bne	s3,s1,2f0 <loopr>
 320:	035e0463          	beq	t3,s5,348 <nextr>
 324:	00090a13          	mv	s4,s2

00000328 <spacesr>:
 328:	07000f6f          	jal	t5,398 <waitr>
 32c:	77fa1073          	csrw	0x77f,s4
 330:	00072303          	lw	t1,0(a4)
 334:	01c00f93          	li	t6,28
 338:	00030593          	mv	a1,t1
 33c:	00a00e13          	li	t3,10
 340:	00000993          	li	s3,0
 344:	faa506e3          	beq	a0,a0,2f0 <loopr>

00000348 <nextr>:
 348:	00900a13          	li	s4,9
 34c:	04c00f6f          	jal	t5,398 <waitr>
 350:	77fa1073          	csrw	0x77f,s4
 354:	02ae8863          	beq	t4,a0,384 <line_changer>

00000358 <next1r>:
 358:	ffc70713          	addi	a4,a4,-4
 35c:	00138393          	addi	t2,t2,1
 360:	001c8c93          	addi	s9,s9,1
 364:	000c8813          	mv	a6,s9
 368:	00fcfc93          	andi	s9,s9,15
 36c:	015cc663          	blt	s9,s5,378 <herer>
 370:	0f087813          	andi	a6,a6,240
 374:	01080813          	addi	a6,a6,16

00000378 <herer>:
 378:	00080c93          	mv	s9,a6
 37c:	f4d39ce3          	bne	t2,a3,2d4 <main_subr>
 380:	02a50463          	beq	a0,a0,3a8 <exit_DREG>

00000384 <line_changer>:
 384:	00a00e93          	li	t4,10
 388:	01000f6f          	jal	t5,398 <waitr>
 38c:	77fe9073          	csrw	0x77f,t4
 390:	00000e93          	li	t4,0
 394:	fca502e3          	beq	a0,a0,358 <next1r>

00000398 <waitr>:
 398:	77f02c73          	csrr	s8,0x77f
 39c:	100c7b13          	andi	s6,s8,256
 3a0:	fe0b0ce3          	beqz	s6,398 <waitr>
 3a4:	000f0067          	jr	t5

000003a8 <exit_DREG>:
 3a8:	ff812083          	lw	ra,-8(sp)
 3ac:	00008067          	ret

Disassembly of section .data:

000003b0 <str1>:
 3b0:	4420                	lw	s0,72(s0)
 3b2:	4552                	lw	a0,20(sp)
 3b4:	6f742047          	0x6f742047
 3b8:	4420                	lw	s0,72(s0)
 3ba:	7369                	lui	t1,0xffffa
 3bc:	6c70                	flw	fa2,92(s0)
 3be:	7961                	lui	s2,0xffff8
 3c0:	4120                	lw	s0,64(a0)
 3c2:	4c4c                	lw	a1,28(s0)
 3c4:	5220                	lw	s0,96(a2)
 3c6:	4745                	li	a4,17
 3c8:	5349                	li	t1,-14
 3ca:	4554                	lw	a3,12(a0)
 3cc:	5352                	lw	t1,52(sp)
 3ce:	0a20                	addi	s0,sp,280
 3d0:	4320                	lw	s0,64(a4)
 3d2:	524c                	lw	a1,36(a2)
 3d4:	6f742053          	0x6f742053
 3d8:	4320                	lw	s0,64(a4)
 3da:	454c                	lw	a1,12(a0)
 3dc:	5241                	li	tp,-16
 3de:	5320                	lw	s0,96(a4)
 3e0:	45455243          	0x45455243
 3e4:	204e                	fld	ft0,208(sp)
 3e6:	200a                	fld	ft0,128(sp)
 3e8:	4d44                	lw	s1,28(a0)
 3ea:	4d45                	li	s10,17
 3ec:	7420                	flw	fs0,104(s0)
 3ee:	4944206f          	j	42882 <_end+0x42368>
 3f2:	414c5053          	0x414c5053
 3f6:	2059                	jal	47c <str1+0xcc>
 3f8:	454d                	li	a0,19
 3fa:	4f4d                	li	t5,19
 3fc:	5952                	lw	s2,52(sp)
 3fe:	0a20                	addi	s0,sp,280
 400:	4d20                	lw	s0,88(a0)
 402:	204d444f          	fnmadd.s	fs0,fs10,ft4,ft4,rmm
 406:	2d20                	fld	fs0,88(a0)
 408:	754e                	flw	fa0,240(sp)
 40a:	626d                	lui	tp,0x1b
 40c:	7265                	lui	tp,0xffff9
 40e:	6f20                	flw	fs0,88(a4)
 410:	2066                	fld	ft0,88(sp)
 412:	6f6c                	flw	fa1,92(a4)
 414:	69746163          	bltu	s0,s7,a96 <_end+0x57c>
 418:	20736e6f          	jal	t3,36e1e <_end+0x36904>
 41c:	202d                	jal	446 <str1+0x96>
 41e:	72617453          	0x72617453
 422:	6974                	flw	fa3,84(a0)
 424:	676e                	flw	fa4,216(sp)
 426:	4d20                	lw	s0,88(a0)
 428:	6d65                	lui	s10,0x19
 42a:	2079726f          	jal	tp,97e30 <_end+0x97916>
 42e:	202d                	jal	458 <str1+0xa8>
 430:	6156                	flw	ft2,84(sp)
 432:	756c                	flw	fa1,108(a0)
 434:	2065                	jal	4dc <str1+0x12c>
 436:	6f74                	flw	fa3,92(a4)
 438:	6220                	flw	fs0,64(a2)
 43a:	2065                	jal	4e2 <str1+0x132>
 43c:	6e616863          	bltu	sp,t1,b2c <_end+0x612>
 440:	20646567          	0x20646567
 444:	202d                	jal	46e <str1+0xbe>
 446:	4f54                	lw	a3,28(a4)
 448:	4320                	lw	s0,64(a4)
 44a:	4148                	lw	a0,4(a0)
 44c:	474e                	lw	a4,208(sp)
 44e:	2045                	jal	4ee <str1+0x13e>
 450:	454d                	li	a0,19
 452:	4f4d                	li	t5,19
 454:	5952                	lw	s2,52(sp)
 456:	0a20                	addi	s0,sp,280
 458:	4d20                	lw	s0,88(a0)
 45a:	2052444f          	fnmadd.s	fs0,ft4,ft5,ft4,rmm
 45e:	2d20                	fld	fs0,88(a0)
 460:	2020                	fld	fs0,64(s0)
 462:	5220                	lw	s0,96(a2)
 464:	6765                	lui	a4,0x19
 466:	7369                	lui	t1,0xffffa
 468:	6574                	flw	fa3,76(a0)
 46a:	2072                	fld	ft0,280(sp)
 46c:	6e49                	lui	t3,0x12
 46e:	6564                	flw	fs1,76(a0)
 470:	2078                	fld	fa4,192(s0)
 472:	202d                	jal	49c <str1+0xec>
 474:	6156                	flw	ft2,84(sp)
 476:	756c                	flw	fa1,108(a0)
 478:	2065                	jal	520 <_end+0x6>
 47a:	6f74                	flw	fa3,92(a4)
 47c:	6220                	flw	fs0,64(a2)
 47e:	2065                	jal	526 <_end+0xc>
 480:	6e616863          	bltu	sp,t1,b70 <_end+0x656>
 484:	20646567          	0x20646567
 488:	202d                	jal	4b2 <str1+0x102>
 48a:	4f54                	lw	a3,28(a4)
 48c:	4320                	lw	s0,64(a4)
 48e:	4148                	lw	a0,4(a0)
 490:	474e                	lw	a4,208(sp)
 492:	2045                	jal	532 <_end+0x18>
 494:	6552                	flw	fa0,20(sp)
 496:	74736967          	0x74736967
 49a:	7265                	lui	tp,0xffff9
 49c:	0a20                	addi	s0,sp,280
 49e:	5220                	lw	s0,96(a2)
 4a0:	20544553          	0x20544553
 4a4:	5320                	lw	s0,96(a4)
 4a6:	5754464f          	0x5754464f
 4aa:	5241                	li	tp,-16
 4ac:	2045                	jal	54c <_end+0x32>
 4ae:	4552                	lw	a0,20(sp)
 4b0:	20544553          	0x20544553
 4b4:	200a                	fld	ft0,128(sp)
 4b6:	4548                	lw	a0,12(a0)
 4b8:	504c                	lw	a1,36(s0)
 4ba:	2020                	fld	fs0,64(s0)
 4bc:	4944                	lw	s1,20(a0)
 4be:	414c5053          	0x414c5053
 4c2:	2059                	jal	548 <_end+0x2e>
 4c4:	4d4d4f43          	0x4d4d4f43
 4c8:	4e41                	li	t3,16
 4ca:	5344                	lw	s1,36(a4)
 4cc:	0a20                	addi	s0,sp,280
 4ce:	4420                	lw	s0,72(s0)
 4d0:	4552                	lw	a0,20(sp)
 4d2:	52202053          	0x52202053
 4d6:	6765                	lui	a4,0x19
 4d8:	7369                	lui	t1,0xffffa
 4da:	6574                	flw	fa3,76(a0)
 4dc:	2072                	fld	ft0,280(sp)
 4de:	6e49                	lui	t3,0x12
 4e0:	6564                	flw	fs1,76(a0)
 4e2:	2078                	fld	fa4,192(s0)
 4e4:	202d                	jal	50e <str1+0x15e>
 4e6:	4f54                	lw	a3,28(a4)
 4e8:	4420                	lw	s0,72(s0)
 4ea:	5349                	li	t1,-14
 4ec:	4c50                	lw	a2,28(s0)
 4ee:	5941                	li	s2,-16
 4f0:	5320                	lw	s0,96(a4)
 4f2:	4e49                	li	t3,18
 4f4:	20454c47          	fmsub.s	fs8,fa0,ft4,ft4,rmm
 4f8:	4552                	lw	a0,20(sp)
 4fa:	54534947          	0x54534947
 4fe:	5245                	li	tp,-15
 500:	0a20                	addi	s0,sp,280
 502:	4c20                	lw	s0,88(s0)
 504:	2044414f          	fnmadd.s	ft2,fs0,ft4,ft4,rmm
 508:	5320                	lw	s0,96(a4)
 50a:	7265                	lui	tp,0xffff9
 50c:	6169                	addi	sp,sp,208
 50e:	206c                	fld	fa1,192(s0)
 510:	6f6c                	flw	fa1,92(a4)
 512:	6461                	lui	s0,0x18
 514:	2020                	fld	fs0,64(s0)
 516:	200a                	fld	ft0,128(sp)
 518:	0024                	addi	s1,sp,8

Disassembly of section .comment:

00000000 <.comment>:
   0:	5254                	lw	a3,36(a2)
   2:	4149                	li	sp,18
   4:	004c                	addi	a1,sp,4
