load:
	li a1, 0x00
	addi a1, x19, 0x00
	li s5, 0x00
	addi s5, x19, 0x00
	li a0, 0x00
	li s0, 0x23
	li s1, 0x24
	li s2, 0x0d
	li a2, 0x00
	li a5, 0x00
	li a3, 0x00

	  sw a1, 0(a0)   
	
	li a4, 0x0a
	
beg1:	 csrrs a5, 0x77f, x0
	 andi a3, a5, 0x100
         beq a3, a0, beg1
         csrrw x0, 0x77f, a4

polling: csrrs a2, 0x77e, x0
	 andi a3, a2, 0x100
         beq a3, a0, polling
	 andi a4, a2, 0xff
	 bne a4, s0, polling

beg:	 csrrs a2, 0x77e, x0
	 andi a3, a2, 0x100
         beq a3, a0, beg
	 andi a4, a2, 0xff
	 beq a4, s1, end1
	 beq a4, s2, endl
	 sb a4, 0(a1) 
	 csrrs a5, 0x77f, x0
	 andi a3, a5, 0x100
         beq a3, a0, beg
         csrrw x0, 0x77f, a4
	 addi a1, a1, 0x01
	 beq a0, a0, beg

endl:	 li a4, 0x0a
	 sb a4, 0(a1)
	 addi a1, a1, 0x01
	 beq a0, a0, beg	 

end1:	 sb a4, 0(a1)
	 beq a0, a0, welcome

welcome:addi a1, s5, 0x00 
	reps:	
	lb t1,0(a1)           
        beq s1,t1,exits 

rep1s:	csrrs a2,0x77f,x0    
        andi a3,a2,0x100      
        beq a3,a0,rep1s     
        csrrw x0,0x77f,t1       
        addi a1,a1,1             
        beq a0,a0,reps

exits:   li x18, 0x20
	#li x19, 0x400
		  
display_memory:
   
#	li a3, 0x06
	#lui a4,0x00000
#	li a4, 0x400
	#sw a3, 0(a4)
	li a3, 0x00
	li a4, 0x00
	
	addi a3, x18, 0x00
	addi a4, s5, 0x00
	
	li a0, 0x04        #byte threshold 
#	lui s2, 0x93a
#	srli s2, s2, 0x04  #stores ascii values for <space>:<tab>
#	ori s2,s2, 0x20
#	li s2, 0x20  
	li t2, 0x00	   #counter to keep track of the number of mem locations
	li s1, 0x08        #nibble threshold
	li s5, 0x0a        #hex for 'a'
	li t4, 0x00        #line change counter
	li a1, 0x00
	li s4, 0x00
       
 
main_sub:
	li t6, 28
	li s2, 0x3a
	addi t4, t4, 0x01
	addi a1, a4, 0x00  #store address in a1
	li s3, 0x00        #nibble counter
	li s7, 0x00        #byte threshold
	li t3, 0x00
loop:
	srl s4, a1, t6
	addi t6, t6, -4
	andi s4, s4, 0x0f  #to mask the first nibble alone
	bge s4, s5, alpha1  #check if alphabet
	addi s4, s4, 0x30  #offset for numbers
	beq a0, a0, print

alpha1:
	addi s4, s4, 0x57  #offset for alphabets
	beq a0, a0, print
    	
print:	
        jal t5, wait       #call the wait loop
	csrrw x0,0x77f,s4 #output the value via 0x77f
#	srli a1, a1, 0x04  #shift right a1 by four places to access next nibble
	addi s3, s3, 0x01  #upgrade the nibble counter
	bne s3, s1, loop   #check if all 8 nibbles are over
	beq t3, s5, next   #jump to next if the display was for the value
	addi s4, s2, 0x00  

spaces:
	jal t5, wait
	csrrw x0, 0x77f, s4
#	srli s4, s4, 0x08
#	addi s7, s7, 0x01
#	bne s7, a0, spaces
	lw t1, 0(a4)
	li t6, 28
	addi a1, t1, 0x00
	li t3, 0x0a
	li s3, 0x00
	beq a0, a0, loop

next:	li s2, 0x9
	jal t5, wait
	csrrw x0, 0x77f, s2
	beq t4, a0, line_change
next1:	
	addi a4, a4, 0x04
	addi t2, t2, 0x01
	bne t2, a3, main_sub
#	jr a5
	beq a0, a0, Welcome

line_change:
	li t4, 0xa
	jal t5, wait
	csrrw x0, 0x77f, t4
	li t4, 0x00
	beq a0, a0, next1
wait:
	csrrs s8,0x77f,x0   
	andi s6,s8,0x100     
	beq s6,x0,wait
	jr t5

Welcome:
	   la a3, str
	   li a1, 0x24
	   rep:	
	   lb a2,0(a3)            # read the next byte of string into register x1
           beq a1,a2,exit2         # if $ symbol read then end display
  rep1:  
     csrrs t2,0x77f,x0     # read rg_tx.
     andi t3,t2,0x100      # get the ready_bit.
     beq t3,x0,rep1       # unless the bit is 1 keep polling. a 1 bit indicates the FIFO has value.  
     csrrw x0,0x77f,a2       # write lower 8 bits to rg_rtx. **This will only work if rd is x0**
     addi a3,a3,1              # increment a1 to point to next byte
     beq a0,a0,rep   # keep doing this crap forever n ever

exit2:

	jr x27
	

.data

 str :    .asciz "Welcome to SHAKTI C-Class controller\n\r>$"
	

