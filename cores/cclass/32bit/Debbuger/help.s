.text
.align	4
.globl help
help:
  
  addi sp,sp,-16
  sw a3,16(sp)
  sw a2, 12(sp)
  sw a1, 8(sp)
  sw t2, 4(sp)
  sw ra, 0(sp)
  la a3, str1       # load pointer to string in a3.
   
  li a1, 0x24
  rep:	
	   lb a2,0(a3)            # read the next byte of string into register x1
     beq a1,a2,exit_help        # if $ symbol read then end display

  rep1:  
     csrrs t2,0x77f,x0         # read rg_tx.
     andi t2,t2,0x100          # get the ready_bit.
     beq t2,x0,rep1            # unless the bit is 1 keep polling. a 1 bit indicates the FIFO has value.  
     csrrw x0,0x77f,a2         # write lower 8 bits to rg_rtx. **This will only work if rd is x0**
     addi a3,a3,1              # increment a1 to point to next byte
     beq a0,a0,rep             # keep doing this crap forever n ever
  
exit_help: 
  lw ra,0(sp)
  addi sp,sp,16
  ret

.data
	  str1 :    .asciz " DREG to Display ALL REGISTERS \n CLRS to CLEAR SCREEN \n DMEM to DISPLAY MEMORY \n MODM  -Number of locations - Starting Memory - Value to be changed - TO CHANGE MEMORY \n MODR  -   Register Index - Value to be changed - TO CHANGE Register \n RSET  SOFTWARE RESET \n HELP  DISPLAY COMMANDS \n DRES  Register Index - TO DISPLAY SINGLE REGISTER \n LOAD  Serial load  \n $"
	  #str1 :    .asciz " Work\n $"

   

    
   
