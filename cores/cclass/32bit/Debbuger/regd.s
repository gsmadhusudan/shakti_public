.text
.align	4
.globl display_register

display_register:
Init:
 
	sw x0, 0(sp)
	sw x1, -4(sp)
	sw x2, -8(sp)
	sw x3, -12(sp)
	sw x4, -16(sp)
	sw x5, -20(sp)
	sw x6, -24(sp)
	sw x7, -28(sp)
	sw x8, -32(sp)
	sw x9, -36(sp)
	sw x10, -40(sp)
	sw x11, -44(sp)
	sw x12, -48(sp)
	sw x13, -52(sp)
	sw x14, -56(sp)
	sw x15, -60(sp)
	sw x16, -64(sp)
	sw x17, -68(sp)
	sw x18, -72(sp)
	sw x19, -76(sp)
	sw x20, -80(sp)
	sw x21, -84(sp)
	sw x22, -88(sp)
	sw x23, -92(sp)
	sw x24, -96(sp)
	sw x25, -100(sp)
	sw x26, -104(sp)
	sw x27, -108(sp)
	sw x28, -112(sp)
	sw x29, -116(sp)
	sw x30, -120(sp)
	sw x31, -124(sp)

	

	li a3, 0x20
	addi a4,sp, 0
	li a0, 0x04        #byte threshold 
	li t2, 0x00	   #counter to keep track of the number of mem locations
	li s1, 0x08        #nibble threshold
	li s5, 0x0a        #hex for 'a'
	li t4, 0x00        #line change counter
	li s9, 0x00
	li t6, 4
main_subr:
	li t6,  4   #28
	li s2, 0x3a
	addi t4, t4, 0x01
	li s3, 0x06        #nibble counter
	li s7, 0x00        #byte threshold
	addi a1, s9, 0x00
	li t3, 0x00
	
loopr:
	srl s4, a1, t6
	addi t6, t6, -4
	andi s4, s4, 0x0f  #to mask the first nibble alone
	bge s4, s5, alpha1r  #check if alphabet
	addi s4, s4, 0x30  #offset for numbers
	beq a0, a0, printr

alpha1r:
	addi s4, s4, 0x57  #offset for alphabets
	beq a0, a0, printr
    	
printr:	
        jal t5, waitr       #call the wait loop
	csrrw x0,0x77f,s4 #output the value via 0x77f
	addi s3, s3, 0x01  #upgrade the nibble counter
	bne s3, s1, loopr   #check if all 8 nibbles are over
	beq t3, s5, nextr   #jump to next if the display was for the value
	addi s4, s2, 0x00  

spacesr:
	jal t5, waitr
	csrrw x0, 0x77f, s4
	lw t1, 0(a4)
	li t6, 28
	addi a1, t1, 0x00
	li t3, 0x0a
	li s3, 0x00
	beq a0, a0, loopr

nextr:	li s4, 0x9
	jal t5, waitr
	csrrw x0, 0x77f, s4
	beq t4, a0, line_changer
next1r:	
	addi a4, a4, -4
	addi t2, t2, 0x01
	addi s9, s9, 0x01
	addi a6, s9, 0x00
	andi s9, s9, 0x0f
	blt s9, s5,herer
	andi a6, a6, 0xf0
	addi a6, a6, 0x10
herer:
	addi s9, a6, 0x00
	bne t2, a3, main_subr
	beq a0, a0, exit_DREG

line_changer:
	li t4, 0xa
	jal t5, waitr
	csrrw x0, 0x77f, t4
	li t4, 0x00
	beq a0, a0, next1r
waitr:
	csrrs s8,0x77f,x0   
	andi s6,s8,0x100     
	beq s6,x0,waitr
	jr t5

exit_DREG:  
  lw ra, -8(sp) # since the stack pointer hasn't changed and ra was already stored in -8(sp) at the begining.
  ret
