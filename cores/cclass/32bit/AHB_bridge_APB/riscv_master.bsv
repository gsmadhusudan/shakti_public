/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Venkatesh.B, Arjun Menon
Email ID : ee13m083@ee.iitm.ac.in, c.arjunmenon@gmail.com


This package has been developed based on the AMBA 3 AHB lite protocol Version 1.0 PDF from ARM.
Web: www.arm.com

AHB-lite is a high-performance bus interface that supports single master and multiple slaves.We design single AHB slave in this module.The overall architecture is "Processor-AHB master-Bridge-APB-dummy​ slave"
*/

package riscv_master;

import riscv::*;
import ConfigReg::*;

    interface Ifc_riscv_master;
        method Bit#(32) haddr_();               // address going to the bus
        method bit hwrite_();                   // indicate whether read or write operation 0 for read and 1 for write
        method Bit#(3) hsize_();                // size of the byte transfer. 0=byte, 1=halfword, 2=word. Confirmed from page 3-8
        method Bit#(3) hburst_();               // burst type. 0=single, 1=incr, 2=wrap4, 3=incr4 etc 
        method Bit#(4) hprot_();                // 0th bit=0 opcode fetch. 0th bit=1 data access. 1st bit = 0 supervisor mode else user mode. 2nd and 3rd bit not required.
        method Bit#(2) htrans_();               // 11 when doing burst tranfer. 10 when doing single tranfer. 00 when not doing anything. Default to 00.
        method bit hmastlock_();                // TODO:This is highly useful when performing atomic operations.
        method Bit#(32) hwdata_();              // data to be written in case of write operation.

        method Action _hready( bit _ready);     // when high indicates the completion of the transfer
        method Action _hresp( bit response);    // when low indicates the status as okay. Else ERROR.
        method Action _hrdata(Bit#(32) data);   // the 32bit data read from the slave.
    endinterface

    typedef enum {IDLE,BUSY,NONSEQ,SEQ} Htrans_type deriving(Eq,Bits);

    (*synthesize*)
    module mk_riscv_master(Ifc_riscv_master);
        Reg#(Bit#(32)) rg_address <-mkConfigReg(0);   // 32bit address to the slave.
        Reg#(Bit#(1)) rg_read_write <-mkReg(0); // default is read.
        Reg#(Bit#(3)) rg_hsize <-mkConfigReg(0);      // default is byte size.
        Reg#(Bit#(3)) rg_hburst <-mkReg(0);     // single burst mode.
        Reg#(Bit#(4)) rg_hprot <-mkReg(4'b0011);// non cacheable, non-bufferable, supervisor, data access
        Reg#(Bit#(2)) rg_htrans <-mkReg(0);     // default to IDLE state.
        Reg#(Bit#(1)) rg_hmastlock <-mkReg(0);  // default no lock is done.
        Reg#(Bit#(32)) rg_hwdata <-mkReg(0);    // 32bit data to be written to the slave.
	Reg#(Bit#(4)) rg_beat_count <-mkConfigReg(0);   //counter for maintaining the data packet count during burst mode
	Reg#(Bit#(4)) rg_loop_counter <-mkConfigReg(0);   //counter to send the corresponding bits of data during burst transfers

        Wire#(bit) wr_hready<-mkDWire(0);   //ready signal from apb bridge
        Wire#(bit) wr_hresp<-mkDWire(1);    //default is invalid.ERROR
        Wire#(Bit#(32)) wr_hdata<-mkDWire(0); //data coming from the slave
    
        Reg#(Bit#(2)) rg_data_ins_read_write <-mkReg(0); // 1stbit =0 data =1 instruction. 0thbit = 0 read =1 write.
        Reg#(Bit#(512)) rg_temp_data<-mkReg(0);  //temporary data register used for sending the data to slave
        Reg#(Htrans_type) rg_state <-mkReg(IDLE);  //state of the master

        Ifc_riscv processor <-mkriscv(); // instantiating the processor.
        
        rule read_request_from_the_processor;

            case (rg_state) matches
                IDLE: begin
                    if(processor._address_data_out matches tagged Valid .addr)begin // data cache requested access
			rg_address <= addr;  //slave address from the processor
                        rg_hburst<=processor.burst_mode_();         // capture burst mode from the processor.
                        rg_hsize<={1'b0,processor.byte_halfword_word_()};        // size of byte transefer indicated.
                        rg_hprot<={processor.cacheable_,processor.bufferable_,processor.supervisor_,1'b1};
                        rg_hmastlock<=1;
                        if(processor._data_out matches tagged Valid .data) begin // write process initiated.
                            rg_temp_data<=data;
                            rg_read_write<=1;                       // write signal activated.
                            rg_data_ins_read_write<=2'b01;          // data and write
                        end
                        else begin                                  // read request
                            rg_read_write<=0;                       // read signal activated.
                            rg_data_ins_read_write<=2'b00;
                        end
                        rg_state<=NONSEQ;
                        rg_htrans<=2'b10;                           // NONSEQ state
                    end     
                    else if(processor._address_instruction_out matches tagged Valid .addr )begin // instruction cache requested access
			rg_address <= addr;  //slave address from the processor
                        rg_hprot<={processor.cacheable_,processor.bufferable_,processor.supervisor_,1'b0};
                        rg_hsize<={1'b0,processor.byte_halfword_word_()}; // size of byte transefer indicated.
                        rg_read_write<=0;                           // read signal activated
                        rg_htrans<=2'b10;
                        rg_state<=NONSEQ;
                        rg_data_ins_read_write<=2'b10;
                    end
		//TODO if none of them match then any transaction should be invalid.so processor._bus_error(1); should be used here
                end

                NONSEQ: begin
                   if(rg_data_ins_read_write[0]==1)                 // write started by data cache
                      rg_hwdata<= rg_temp_data[(8*rg_hsize)-1:0];  //depending on the size of transfer the corresponding data is transferred
                   
                    if(rg_hburst==0)begin                           // single transaction. Non burst mode activated.
                       if(wr_hready ==1 && wr_hresp==0) begin       // transaction over signal recieved from the slave with okay response
                           processor._bus_error(0);                 // no bus error generated
                           rg_state<=IDLE;                          // go back to idle state to finish transaction.
                           if(rg_data_ins_read_write[1]==0)         // data memory
                                   processor._data_in( tagged Valid wr_hdata); // send data from slave to the processor
                           else // instruction memory
                                   processor._instruction_in(tagged Valid wr_hdata); // send instruction from slave to the processor.
                       end
                       else if(wr_hresp==1) begin                  // error occured over the bus transaction
                           processor._bus_error(1);                 // send error signal to the processor
                           rg_state<=IDLE;                          // abandon transaction
                       end
                   end
                   else begin//burst mode activated from the processor.
                       rg_state<=SEQ; 
			Bit #(4) lv_beat_count_next=0;
			case(rg_hburst)
				3'b001: lv_beat_count_next = 16//incrementing burst of undefined length.Max beat count is 16.needs to be terminated by the processor
				3'b010: lv_beat_count_next = 4; //wrap around burst of beat count 4
				3'b011: lv_beat_count_next = 4; //incremental burst of beat count 4
				3'b100: lv_beat_count_next = 8; //wrap around burst of beat count 8
				3'b101: lv_beat_count_next = 8; //incremental burst of beat count 8
				3'b110: lv_beat_count_next = 16; //wrap around burst of beat count 16
				3'b111: lv_beat_count_next = 16; //incremental burst of beat count 16
			endcase
		   rg_loop_counter <= lv_beat_count_next;
		   rg_beat_count<=lv_beat_count_next-1;//since one transaction already happened in the NONSEQ mode,-1 is required for beats
                   end
                end

                SEQ: begin
			Bit#(32) offset = 0;
/*depending on the size of transfer we get the new address.The starting address is sent by the processor when the master is idle.Later on
 the master itself has to develop the new addresses,depending on the transfer type.Hence we use these 2 case statements which help us to
 complete the sequential mode of transfer*/
			case(rg_hsize)  
				3'b000:offset = 'd1; //byte transfer
				3'b001:offset = 'd2; //halfword
				3'b010:offset = 'd4; //word
				3'b011:offset = 'd8; //doubleword
				3'b100:offset = 'd16; //4 word line
				3'b101:offset = 'd32; //8 word line
				3'b110:offset = 'd64; //16 word line
				3'b111:offset = 'd128; //32 word line
			endcase

			case(rg_hburst)
				3'b001:rg_address <= rg_address+offset;//undefined length incremental transfer. next address calculation
				3'b010:begin                            //wrap4
						if((rg_address+offset) >= {(rg_address[31:(rg_hsize+'d2)]+1),3'd0})
							rg_address <= {(rg_address[31:(rg_hsize+'d2)]),3'd0};
						else
							rg_address <= rg_address+offset;
					end
				3'b011:rg_address <= rg_address+offset;//incremental transfer of 4-beat
				3'b100:begin                            //wrap8
						if((rg_address+offset) >= {(rg_address[31:(rg_hsize+'d3)]+1),3'd0})
							rg_address <= {(rg_address[31:(rg_hsize+'d3)]),3'd0};
						else
							rg_address <= rg_address+offset;
					end
				3'b101:rg_address <= rg_address+offset;//incremental transfer of 8-beat
				3'b110:begin                            //wrap16
						if((rg_address+offset) >= {(rg_address[31:(rg_hsize+'d4)]+1),3'd0})
							rg_address <= {(rg_address[31:(rg_hsize+'d4)]),3'd0};
						else
							rg_address <= rg_address+offset;
					end
				3'b111:rg_address <= rg_address+offset;//incremental transfer of 16-beat
			endcase

			if(rg_beat_count > 0)begin 
				if(rg_data_ins_read_write[0]==1)                 // write started by data cache
                      			rg_hwdata<= rg_temp_data[((rg_loop_counter-rg_beat_counter)*(8*offset))+((8*offset)-1):((rg_loop_counter-rg_beat_counter)*(8*offset))]; //depending on the size of transfer the write data is sent to the slave.The no.of loops of the burst is also taken care of.
				end

				if(wr_hready ==1 && wr_hresp==0) begin // transaction over signal recieved from the slave with okay response
					processor._bus_error(0);                 // no bus error generated
                           		rg_state<=SEQ;                          // go back to idle state to finish transaction.
                           		if(rg_data_ins_read_write[1]==0)         // data memory
                                	   	processor._data_in( tagged Valid wr_hdata); // send data from slave to the processor
                           		else 				// instruction memory
                                	   	processor._instruction_in(tagged Valid wr_hdata); // send instruction from slave to the processor.
                       			end
                       		else if(wr_hresp==1) begin                  // error occured over the bus transaction
                           		processor._bus_error(1);                 // send error signal to the processor
                           		rg_state<=IDLE;                          // abandon transaction
                       		end
				rg_beat_count <= rg_beat_count - 1;//decrementing the counter by 1
		    	end
			else begin
				rg_state <= IDLE; //at the end of the burst the bus needs to enter the idle state always
				rg_loop_counter <= 0;  //the loop counter to take care of the beats is also made 0
				rg_address <= 0;  //the address sent to the slave is also made 0
				rg_hsize <= 0;   //the size of transfer is also made 0
			end
	endrule

        method Bit#(32) haddr_();       // address going to the bus
            return rg_address;
        endmethod

        method bit hwrite_();           // indicate whether read or write operation 0 for read and 1 for write
            return rg_read_write;
        endmethod

        method Bit#(3) hsize_();        // size of the byte transfer. 0=byte, 1=halfword, 2=word. Confirmed from page 3-8
            return rg_hsize;
        endmethod

        method Bit#(3) hburst_();       // burst type. 0=incremental, 1=incr, 2=wrap4, 3=incr4 etc 
            return rg_hburst;
        endmethod

        method Bit#(4) hprot_();        // 0th bit=0 opcode fetch. 0th bit=1 data access. 1st bit = 0 supervisor mode else user mode. 2nd and 3rd bit not required.
            return rg_hprot;
        endmethod

        method Bit#(2) htrans_();       // 11 when doing burst tranfer. 10 when doing single tranfer. 00 when not doing anything. Default to 00.
            return rg_htrans;
        endmethod

        method bit hmastlock_();        // TODO:This is highly useful when performing atomic operations.
            return rg_hmastlock;
        endmethod

        method Bit#(32) hwdata_();      // data to be written in case of write operation.
            return rg_hwdata;
        endmethod

        method Action _hready( bit _ready);     // when high indicates the completion of the transfer
            wr_hready<=_ready;
        endmethod

        method Action _hresp( bit response);    // when low indicates the status as okay. Else ERROR.
            wr_hresp<=response;
        endmethod

        method Action _hrdata(Bit#(32) data);   // the 32bit data read from the slave.
            wr_hdata<=data;
        endmethod
    endmodule
endpackage
