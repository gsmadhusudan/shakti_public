/*
Package Name : AMBA Peripheral Bus[APB]
Authors: Venkatesh,Arjun Menon,Patanjali
Email Id: ee13m083@ee.iitm.ac.in,c.arjunmenon@gmail.com,patanjali.slpsk@gmail.com
Last Updated:6/1/2015

This package has been developed based on the AMBA 3 APB protocol Version 1.0 PDF from ARM.
Web: www.arm.com
*/

//our 1st try at APB master.

package ambaperipheral;

import ConfigReg::*;
`include "defined_parameters.bsv"

/*
(*synthesize*)
module mkTb_apb(Empty);
	Apb_ifc apb <- mkapb;
	Reg#(int) counter <- mkReg(0);

	rule rest(counter == 0);
		apb.reset_from_proc(1'b1);
		counter <= counter + 1;
	endrule

	rule start(counter == 1);
		apb.address_from_bridge('d1,1'b1,'d2,'d4,'d0,2'b10,1'b1,2'b11);
		counter <= counter + 1;
	endrule

	rule start1(counter == 2);
		let data = apb.read_data();
		let valid_signal = apb.valid_signal_from_slave();
		$display("data is %b\t counter is %d\n",data,counter);
		counter <= counter + 1;
	endrule

	rule transfer(counter == 3);
		let data = apb.read_data();
		$display("data is %b\t counter is %d\n",data,counter);
		apb.data_from_bridge('d284);
		apb.address_from_bridge('d1,1'b1,'d2,'d4,'d0,2'b10,1'b1,2'b10);
		counter <= counter + 1;
	endrule

	rule transfer2(counter == 4);
		apb.address_from_bridge('d1,1'b1,'d2,'d4,'d0,2'b10,1'b1,2'b00);
		let data = apb.write_data();
		$display("data from bridge is %b\n",data);
		apb.data_from_bridge(data);
		counter <= counter + 1;		
	endrule

	rule transfer3(counter == 5);
		apb.data_from_slave('d1111);
		let data = apb.read_data();
		$display("data is %b \n",data);
		counter <= counter + 1;
	endrule

	rule transfer4(counter == 6);
		counter <= counter + 1;
	endrule

	rule transfer5(counter == 7);
		apb.address_from_bridge('d1,1'b1,'d2,'d4,'d0,2'b10,1'b1,2'b01);
		let address = apb.address_of_slave();
		let selecting = apb.select_slave();
		let noterror = apb.valid_signal_to_bridge();
		$display("address of slave is %d \t and selected slave is %b\n",address,selecting);
		$display($time,"\tsignal's validity is %b\n",noterror);
		counter <= counter + 1;
	endrule

	rule transfer6(counter == 8);
		let read_write = apb.write_enable();
		let ready = apb.ready_to_bridge();
		let enable = apb.enable_transfer_from_master();
		$display("enable is %d\t ready is %d\t read_write is %d\n",enable,ready,read_write);
		$finish;
	endrule
endmodule
*/

interface  Apb_ifc;

method Action address_from_bridge(Bit#(`address_bus) address,bit write, Bit#(3) data_size,Bit#(3) burst_type,Bit#(4) prot_type,Bit#(2) transfer_type,bit mast_lock,Bit#(2) psel);                //address from processor sent to apb master via the bridge.fire signal is enable.
method Action reset_from_proc(bit reset);                                                                //reset signal from the processor.
method Action data_from_slave(Bit#(`data_bus) slave_data);                                                    //input read data from slave.
method Action data_from_bridge(Bit#(`data_bus) bridge_data);                                                //input write data from bridge.
method Action valid_signal_from_slave(bit pslverr);                     //response from the slave to master regarding the validity of data.
method Action ready_from_slave(bit pready);                       //ready from the slave to master for transfer initiation from slave side.

method bit valid_signal_to_bridge();            //pslverr signal from apb slave.sent to bridge via master to indicate that transfer failed.
method Bit#(`data_bus) read_data();      //prdata signal from apb slave.the bridge reads data from the slave via master when pwrite is low.
method Bit#(`data_bus) write_data();    //pwdata signal from apb bridge.the bridge writes data to the slave via master when pwrite is high.
method Bit#(`address_bus) address_of_slave();             //paddr signal from apb bridge.this is the apb address bus,generally 32 bit wide.
method Bit#(2) select_slave();                       //pselx signal from apb bridge.master selects 1 slave ,from a group, to transfer data.
method bit enable_transfer_from_master();                        //penable signal to apb slave.master is enabled to transfer data to slave.
method bit ready_to_bridge();                                   //pready signal to apb bridge.Master uses this signal to extend transfers.
method bit write_enable();                        //pwrite signal to apb slave.When low,it is a read access.When high,it is a write access.

endinterface

//use enable master signal while coding!!!!We removed bridge dequeue from the original.bus busy signal also. Removed the valid signal from APB master to bridge[then what is pslverr??].processor-idle state and apb idle state, are they mutually exclusive?In case of multiple slaves we require the bus busy,bus deque,different signal for psel and hsel.Remove Bool enable for now and check the test bench.If gives the same outputs then we can include that by modifying other parts accordingly.

(*synthesize*)

module mkapb(Apb_ifc);

	//apb has 3 states.IDLE,SETUP & ACCESS.2 bits required to identify the state.

/*When do transitions occur?
a) when pselx is asserted then FSM transitions from IDLE to SETUP.Stays here only for 1 clock cycle.Next state is ACCESS state.
b) when in SETUP state the FSM transitions to ACCESS.
c) when in ACCESS, pselx is asserted and pready = 1,then FSM transitions from ACCESS to SETUP.
d) when in ACCESS, pselx is not asserted and pready = 1,then FSM transitions from ACCESS to IDLE.
in the remaining cases,the FSM remains in its present state.*/

//Observation from the PDF file
//if write is enabled,then the data will be ready in setup state itself.write is disabled,then data will be read only in the access state.



	Reg#(Bit#(2)) state <- mkConfigReg(0);                                                                            //state variable.
	Wire#(Bool) fire <- mkDWire(False);                                                         //enable from bridge to the apb master.
	Wire#(bit)   proc_reset <- mkDWire(1);                                                        //reset from processor is active low.

	Wire #(Bit#(`address_bus)) wr_address_of_slave      <- mkDWire(0);	      //haddr signal.address from processor via the bridge.
	Wire #(bit)      wr_write                 <- mkDWire(0);	                                 //hwrite signal.for read or write.
	Wire #(Bit#(3))  wr_data_size             <- mkDWire(0);	                                   //hsize signal.the size of data.
	Wire #(Bit#(3))  wr_burst_type            <- mkDWire(0);	                            //hburst signal.type of the burst mode.
	Wire #(Bit#(4))  wr_prot_type             <- mkDWire(4'b0011);	/*hprot signal.indicates if the access is instruction fetch or data
                                                                               fetch,privileged or user mode,bufferable,cacheable or not.*/
	Wire #(Bit#(2))  wr_transfer_type         <- mkDWire(0);	                              //htrans signal.type of the transfer.
	Wire #(bit)      wr_mast_lock             <- mkDWire(1);	/*hmastlock signal.if more than one master can access a slave,then 
                                                                          to maintain the integrity of the transfer i.e; for the slave not
                                                                              to perform any transactions else where,master can assert this
                                                                                    signal saying that the present transaction is atomic.*/
	Wire #(Bit#(2))      wr_sel                  <- mkDWire(2'b10);	                     //hsel signal.ahb select one particular slave.
	Wire #(Bit#(`data_bus)) wr_write_data            <- mkDWire(0);	//hwdata to be written into the slave by the bridge via the master.


	Reg #(bit)       rg_ready_signal          <- mkConfigReg(1);	 //pready signal from apb master to bridge for transfer initiation.
	Reg #(bit)       rg_valid_signal          <- mkConfigReg(1);         //pslverr signal.error signal from slave to bridge via master.

	Reg#(Bit#(`data_bus))   rg_read_data      <- mkConfigReg(0);         //prdata signal.data read by the bridge from the slave via the
                                                                                                                                  //master.


	Wire#(Bit#(`data_bus))  wr_read_data             <- mkDWire(0);	                         //prdata signal.apb reads data from slave.
	Wire#(bit)       wr_valid_signal          <- mkDWire(0);	         //pslverr signal.valid signal from slave to master in apb.


	Wire#(bit)       wr_slave_ready           <- mkDWire(0);	                   //pready signal.ready response from slave to apb


	Reg#(Bit#(`data_bus))   rg_write_data     <- mkConfigReg(0); 	                        //pwdata signal.master sends data to slave.
        Reg#(Bit#(`address_bus))   rg_address_of_slave      <- mkConfigReg(0);	        //paddr signal.master selects the address of slave.
	Reg#(bit)        rg_write                 <- mkConfigReg(0);	                      //pwrite signal.read or write into the slave.
	Reg#(bit)	 rg_enable_master	  <- mkConfigReg(0);	               //penable signal.enable response to slave by master.
	Reg#(Bit#(2))	 rg_sel                   <- mkConfigReg(2'b10);                               //pselx signal.slave  select signal.

//introduce the concept of slaveerror only in the case of enable,ready and selx are high.Now we are using only ready signal.

rule stat1(state==0);
                                                                         /* the rule will fire when processor enters idle state.it assigns
                                                                              appropriate values to signals and registers based on current 
                                                                        control inputs.it also assigns the next state based on appropriate
                                                                                                                             conditions.*/
	if(rg_ready_signal==0 || (rg_ready_signal==1 && !fire))begin
		rg_write_data 			<= 0;			      //reset state.all the signals are marked 0 when reset occurs.
		rg_address_of_slave   		<= 0;
		rg_write	 		<= 0;
		rg_enable_master 		<= 0;
		rg_sel		 		<= 0;
		state		   		<= 0;
		rg_read_data			<= 0;
		rg_ready_signal  		<= wr_slave_ready;
		$display("--------------- FSM remains in idle state -----------------");
	end
	else if(fire)begin
		state				<= 1;                            //state changes to setup.all the signals vary accordingly.
		rg_sel				<= wr_sel;
		rg_enable_master		<= 0;
		rg_read_data			<= 0;
		rg_address_of_slave		<= wr_address_of_slave;
		rg_write 			<= wr_write;
		rg_ready_signal	 		<= wr_slave_ready;
		if(wr_write==0)begin
			rg_write_data		<= 0;
		end
		else begin
			rg_write_data		<= wr_write_data;
		end
		$display("------------------- FSM state changes from idle to setup ------------------------");	
	end	
endrule


rule stat2(state==1);                                                               /*the rule will fire when processor enters setup state.
                                                                                     it assigns appropriate values to signals and registers
                                                                                  based on current control inputs. it also assigns the next
                                                                                                               state based on conditions.*/
	if(proc_reset==0)begin   	                                                   //in case of reset all the signals are set to 0.
		state   		<= 0;
		rg_write_data 		<= 0;
		rg_address_of_slave	<= 0;
		rg_write		<= 0;
		rg_enable_master	<= 0;
		rg_sel   		<= 2'b10;
		rg_read_data 		<= 0;
		rg_ready_signal  	<= 1; //in the reset state the ready signal sent to the master is high
			

	$display("-------------- FSM is reset to idle state -----------------");
	end		
	else if(fire)begin      	                                                //state changes to access.all the signals vary accordingly.
		state   		<= 2;	
		rg_write_data  		<= rg_write_data;
		rg_address_of_slave	<= rg_address_of_slave;
		rg_write  		<= rg_write;
		rg_enable_master   	<= 1;
		rg_sel  	   	<= rg_sel;
		rg_ready_signal	 	<= wr_slave_ready;
		if(wr_valid_signal == 1)
				rg_read_data 	<= wr_read_data;
		else 
			rg_read_data 	<= 0;
		$display("------------------ FSM changes state from setup to access ---------------------");	
	end
endrule


rule stat3(state==2);                                                                    //the rule will fire when processor enters access
                                                                                      //state. it assigns appropriate values to signals and
                                                                                       //registers based on current control inputs. it also
                                                                                              //assigns the next state based on conditions.
	if(proc_reset == 0)begin                                                            //incase of reset all the signals are set to 0.
		state 	 		<= 0;
		rg_sel			<= 2'b10;
		rg_enable_master	<= 0;
		rg_address_of_slave	<= 0;
		rg_write		<= 0;
		rg_write_data		<= 0;
		rg_read_data		<= 0;
		rg_ready_signal		<= 1;//in the reset state the ready signal sent to the master is high.
		$display("-------------------------- FSM changes state from access to idle ----------------------------");
	end
	else if(wr_slave_ready==0)begin                                                                  //in case of wait states insertion
		state  	        	<= 2;
		rg_ready_signal		<= wr_slave_ready;
		rg_read_data 		<= rg_read_data;                                       			   //data remains the same.
		$display("------------------------- FSM remains in the access state -----------------------------");
	end
	else if (fire && (wr_sel==2'b01||wr_sel==2'b00))//in case of other transfers then the state changes to setup.
	begin	
		state  			<= 1;
		rg_sel			<= wr_sel;
		rg_enable_master	<= 0;
		rg_read_data 		<= 0;
		rg_address_of_slave	<= wr_address_of_slave;
		rg_write		<= wr_write;
		rg_ready_signal		<= wr_slave_ready;
		if(wr_write==0)begin
			rg_write_data	<= 0;
		end
		else begin
			rg_write_data	<= wr_write_data;
		end
		$display("------------------------ FSM changes state from access to setup ------------------------");	
	end
	else if(wr_transfer_type==2'b00 ||wr_transfer_type==2'b01)begin
		state 	 		<= 0;                                       //if there are no extended transfers and no reset,then
                                                                                                                   //return to idle state.
		rg_sel			<= 0;
		rg_enable_master	<= 0;
		rg_address_of_slave	<= 0;
		rg_write		<= 0;
		rg_write_data		<= 0;
		rg_read_data		<= 0;
		rg_ready_signal		<= wr_slave_ready;
		$display("-------------------------- FSM changes state from access to idle ----------------------------");
	end
endrule

rule error1;                                                       //rule fires to assign an error response to apb which is taken by bridge
	if(proc_reset == 0)					                      //in case of reset the valid signal is also set to 1.
		rg_valid_signal<=1;      						   //valid = 0 => data is valid else it is invalid.
	else if (wr_transfer_type==2'b00)			        //in case of idle state,any transfer should be rendered as error.
		rg_valid_signal<=1;                          
	else 							     //in case of regular transfers,this would be the regular error signal.
		rg_valid_signal <= wr_valid_signal;
endrule

/*rule selecting;	     //this logic should be present in the bridge or in the ahb since the source is apb bridge according to the pdf
	if(rg_address_of_slave<=4095)
		rg_sel <= 2'b00;
	else 
		rg_sel <= 2'b01;
endrule*/

method Action address_from_bridge(Bit#(`address_bus) address,bit write,Bit#(3) data_size, Bit#(3) burst_type, Bit#(4) prot_type,Bit #(2) transfer_type, bit mast_lock,Bit#(2) psel);                      //method for address and control signals generated from the APB interface.

	wr_address_of_slave	<=  address;	
	wr_write    		<=  write;
	wr_data_size  		<=  data_size;
	wr_burst_type  		<=  burst_type;
	wr_prot_type   		<=  prot_type;
	wr_transfer_type	<=  transfer_type;
	wr_mast_lock		<=  mast_lock;
	wr_sel     		<=  psel;
	fire			<=  True;
endmethod

method Action reset_from_proc(bit reset);                                           //method for reset signal generated from the processor.
	proc_reset   <= reset;
endmethod

method Action data_from_slave(Bit#(`data_bus) slave_data);                                   //method for reading data received from slave.
	wr_read_data   		<= slave_data;
	wr_valid_signal  	<= 1;
endmethod

method Action data_from_bridge(Bit#(`data_bus) bridge_data);                                //method for writing data received from bridge.
	wr_write_data  	     <= bridge_data;
endmethod

method Action valid_signal_from_slave(bit pslverr);          //method for signal generated to indicate validity of reading data from slave.
	wr_valid_signal   <= pslverr;
endmethod

method Action ready_from_slave(bit pready);                              //method which indicates the readiness of the slave for transfers.
	wr_slave_ready    <= pready;
endmethod

method Bit#(2) select_slave();                                                         //method for generating select signal for the slave.
	return rg_sel;
endmethod

method bit enable_transfer_from_master();                                    //method for generating enable signal sent by master to slave.
	return rg_enable_master;
endmethod

method bit write_enable();                                   //method for generating the read_write signal which will be sent to the slave.
	return rg_write;
endmethod

method Bit#(`address_bus) address_of_slave();//method for sending address location to slave from which data needs to be fetched or updated.
	return rg_address_of_slave;
endmethod

method bit ready_to_bridge();                                           //method for generating ready signal which will be sent to bridge.
	return rg_ready_signal;
endmethod

method Bit#(`data_bus) read_data();                                                           //method for sending the read data to bridge.
	return rg_read_data;
endmethod

method Bit#(`data_bus) write_data();                                                              //method for sending write data to slave.
	return rg_write_data;
endmethod

method bit valid_signal_to_bridge();                                         //method for sending valid signal to bridge for the read data.
	return rg_valid_signal;
endmethod

endmodule
endpackage
