
	.globl	initialMemory
	.section	.initialMemory,"aw",@progbits
	.align	3
	.type	initialMemory, @object
	.size	initialMemory, 4096
	
initialMemory:
		
	.word 0xed68274
	.word 0x967deef
	.word 0x7d29e6b
	.word 0x35f9589
	.word 0x4361279
	.word 0x522e8f5
	.word 0x1e2db70
	.word 0xe1729de
	.word 0x2e9a93a
	.word 0x6373fe2
	.word 0x689c53f
	.word 0xc79fe84
	.word 0xa2220bd
	.word 0x69b8f5
	.word 0x4607364
	.word 0x550f67f
	.word 0x265094d
	.word 0xcc19f1
	.word 0xf520644
	.word 0x830aee1
	.word 0x403a784
	.word 0x6b38825
	.word 0xe5cd6bd
	.word 0xa0540f9
	.word 0xafacaf1
	.word 0x5b0100b
	.word 0x14a2f3b
	.word 0x49c095b
	.word 0x72f16c4
	.word 0x410f0e6
	.word 0xdbde641
	.word 0xfccc341
	.word 0x76ca164
	.word 0x405c8ab
	.word 0x3fba5b9
	.word 0xfbc8813
	.word 0x91ea850
	.word 0x54f3161
	.word 0x194c686
	.word 0xb961903
	.word 0x78266a5
	.word 0x8194e5e
	.word 0xdba5954
	.word 0xf7cf3ab
	.word 0xad0aa5e
	.word 0x6a53037
	.word 0x3d960a9
	.word 0xcaf4fa7
	.word 0xdfe5b3a
	.word 0x12cc582
	.word 0x9e2099d
	.word 0xbed67f8
	.word 0x22471ee
	.word 0x12d69c1
	.word 0x6871bbf
	.word 0x6af535a
	.word 0xf7a75f1
	.word 0xc3809b5
	.word 0xb3d1af5
	.word 0x9ce64a3
	.word 0x54da5a5
	.word 0x9883b66
	.word 0x972a8aa
	.word 0x46913d9
	.word 0x4907dee
	.word 0x595b489
	.word 0xc71702a
	.word 0x2e3b25d
	.word 0xa145708
	.word 0xd344f2e
	.word 0x1775550
	.word 0xec3f02c
	.word 0xb8c72d3
	.word 0x56e2f65
	.word 0x1659f09
	.word 0x2aa5986
	.word 0x628169a
	.word 0x684d30e
	.word 0x62ba598
	.word 0x67b8c59
	.word 0x50b28a1
	.word 0xe925b4e
	.word 0x523d1e4
	.word 0x747ea0e
	.word 0x5573215
	.word 0x4141a4e
	.word 0x9cd5056
	.word 0xdeec0b4
	.word 0x3d46b55
	.word 0x425971e
	.word 0x789adaf
	.word 0x835485f
	.word 0x11265ef
	.word 0xe78a269
	.word 0x713cb18
	.word 0x28529ab
	.word 0x3935acb
	.word 0x4f1ffb4
	.word 0xcf1448d
	.word 0xf0d30a4
	.word 0xe901792
	.word 0x229694a
	.word 0x29d87b1
	.word 0xd6f80c1
	.word 0xb0be44b
	.word 0xfdeabe6
	.word 0x82ce5ba
	.word 0x3fccb60
	.word 0xf005cb1
	.word 0xab28e5b
	.word 0x7b2784d
	.word 0x12c0f1d
	.word 0xbd14e19
	.word 0x15e3e0e
	.word 0xfb089c4
	.word 0x1919ed7
	.word 0xcfc53af
	.word 0x64c60c0
	.word 0x3977001
	.word 0x5058fe4
	.word 0x1dc43d2
	.word 0x8e070b4
	.word 0x4c506a2
	.word 0x60773ac
	.word 0x73208f1
	.word 0x6a67914
	.word 0xa94aa95
	.word 0x439e002
	.word 0x58fd396
	.word 0xd09e298
	.word 0x6d33ec2
	.word 0xf477671
	.word 0xf29d378
	.word 0x1529ec3
	.word 0x84e10a9
	.word 0x511df79
	.word 0x34ed87e
	.word 0xfe936f9
	.word 0x758a2a8
	.word 0x99c163e
	.word 0x195ef54
	.word 0x43583d7
	.word 0xbc6c125
	.word 0x12ee999
	.word 0x7158878
	.word 0xa24b4d8
	.word 0x6e0589b
	.word 0x59352c2
	.word 0xb664bd6
	.word 0x2289a32
	.word 0x1d471fc
	.word 0x791170d
	.word 0x1dc956f
	.word 0xaab891
	.word 0x1cd7011
	.word 0xb1cb683
	.word 0xbc49b82
	.word 0xe6ecff
	.word 0x44368b
	.word 0x43839b6
	.word 0x66ed349
	.word 0xcef43fb
	.word 0x96f1e9f
	.word 0x43e6344
	.word 0x4a595c
	.word 0x900f21a
	.word 0xf19c52d
	.word 0x6f58e8
	.word 0x5ce77f1
	.word 0x46dc722
	.word 0xa851910
	.word 0x8105611
	.word 0x205ecc5
	.word 0xda52829
	.word 0x9e40999
	.word 0x5011b10
	.word 0x91a5e05
	.word 0x90d3a0b
	.word 0x76a0337
	.word 0x3868707
	.word 0xe18c696
	.word 0xa33916
	.word 0xd04bbf
	.word 0x3e33ea8
	.word 0xd14ac37
	.word 0xc69ea64
	.word 0x5aa7700
	.word 0x95e236a
	.word 0xe7e53b6
	.word 0xb536573
	.word 0x971ebae
	.word 0xfd76dac
	.word 0x3081f1f
	.word 0xde60cd4
	.word 0x7fff620
	.word 0xd89237b
	.word 0xcd3530f
	.word 0xdefd992
	.word 0xc66307
	.word 0x1b2d4c3
	.word 0xecab0c3
	.word 0x7f7d77
	.word 0x3ac136b
	.word 0x88e9dfe
	.word 0x2dec7d9
	.word 0xcf0bf83
	.word 0x8262767
	.word 0xf53e6aa
	.word 0x86521c1
	.word 0x183a602
	.word 0xe99191d
	.word 0x93ad86e
	.word 0xc71eb5f
	.word 0xfb44715
	.word 0x876844c
	.word 0x4588160
	.word 0x4556c2d
	.word 0x53ae2a5
	.word 0x51af730
	.word 0xcbb47f6
	.word 0xeba0806
	.word 0x2ee43ff
	.word 0x53d8f4d
	.word 0x9c90adb
	.word 0x5b54ff5
	.word 0x5e389af
	.word 0xd915c5e
	.word 0xfaca93b
	.word 0xe34d497
	.word 0xe7d8159
	.word 0xc224fc8
	.word 0x401c74f
	.word 0x84ae817
	.word 0x785f8b1
	.word 0x438961e
	.word 0xec81fe4
	.word 0xaaf9e0e
	.word 0x7d14402
	.word 0xf04e8f
	.word 0x839700a
	.word 0xfc3b43e
	.word 0xbcdb432
	.word 0x1f58be5
	.word 0xd0c45ae
	.word 0x677fd84
	.word 0xde31b8f
	.word 0x766f638
	.word 0x70c59ff
	.word 0x4fcc115
	.word 0x2a373f0
	.word 0x15260e6
	.word 0x13c177
	.word 0xd2ad9a6
	.word 0x4ee20c5
	.word 0xa11a8f7
	.word 0xa849cef
	.word 0x57332a0
	.word 0x1d01bb7
	.word 0xfbd1bf
	.word 0x98621cc
	.word 0x36fe505
	.word 0x590734f
	.word 0xb7c108d
	.word 0xbe6e75c
	.word 0x6abc551
	.word 0xb7b699
	.word 0x5451087
	.word 0x8a5fd1f
	.word 0x5c84b4
	.word 0x4f5f482
	.word 0x80f3b24
	.word 0xd6c3e9a
	.word 0x6fe5e12
	.word 0xfe261
	.word 0xc7d1c56
	.word 0x6159854
	.word 0x395681d
	.word 0x809d68c
	.word 0x2c61e00
	.word 0x116ad9
	.word 0xdbfe72b
	.word 0x2af1975
	.word 0x7450cc2
	.word 0xad93af8
	.word 0xd6b5ffe
	.word 0xc83b90f
	.word 0x5337ff7
	.word 0x47a4038
	.word 0xaf03244
	.word 0x1fe2089
	.word 0x3e1d7b0
	.word 0x60dbeb1
	.word 0xbe5ea3c
	.word 0x4700f5e
	.word 0xa64c2f6
	.word 0xf4a03a7
	.word 0x3da83a2
	.word 0xbb03e29
	.word 0xe76ce03
	.word 0x4423c95
	.word 0xe38de7f
	.word 0x16377e3
	.word 0x91648ca
	.word 0xb28933
	.word 0x6347292
	.word 0xdd7951e
	.word 0xa55a2ef
	.word 0x7804434
	.word 0x12144af
	.word 0x7f0ff3b
	.word 0x34d61cf
	.word 0xb8507cc
	.word 0xd1bc3e2
	.word 0xa0cfcdc
	.word 0xf096f92
	.word 0x743048a
	.word 0x939e03c
	.word 0x1edf5a1
	.word 0x5e8cc2e
	.word 0x17bd4be
	.word 0x9e9ed1
	.word 0x2c31d9d
	.word 0x536fca0
	.word 0x684a1f4
	.word 0x207b859
	.word 0x8ef4592
	.word 0x8c1831b
	.word 0x45e8c8e
	.word 0xad8c5bb
	.word 0xda0c876
	.word 0xfbddf4c
	.word 0xf7d162
	.word 0x66d0171
	.word 0xcfcb503
	.word 0xfcf823e
	.word 0x1b42e57
	.word 0x5ba42d6
	.word 0xef2975b
	.word 0x1a1c077
	.word 0x2a319a2
	.word 0x1d8e57a
	.word 0x25c46ea
	.word 0xfcfd2b2
	.word 0xa7ef26e
	.word 0x3e7a34e
	.word 0x3cc790c
	.word 0x8c398f5
	.word 0xb3d3e24
	.word 0xb5f6d66
	.word 0x5fcb2c1
	.word 0x8503890
	.word 0x36f42fc
	.word 0x75feda1
	.word 0x42376b6
	.word 0x28fc5df
	.word 0x5ea811f
	.word 0x147dc25
	.word 0xfe58ba6
	.word 0xd0a469
	.word 0x829952a
	.word 0x1a00d22
	.word 0x511dacc
	.word 0xec5300
	.word 0xf7a956b
	.word 0xdc36100
	.word 0x5c37fbb
	.word 0x2966cf2
	.word 0xe6364f0
	.word 0xb7a976c
	.word 0xf078855
	.word 0xd9a26c1
	.word 0xbbbdde2
	.word 0xb582e37
	.word 0xa1f2cf9
	.word 0x35ecbb7
	.word 0xfb06228
	.word 0x5dbfdc7
	.word 0xc3b4a07
	.word 0xbed5ccd
	.word 0xf1023d7
	.word 0xc9e4dce
	.word 0x980e508
	.word 0x39d4be4
	.word 0x4655ed3
	.word 0x955bfb9
	.word 0xb193008
	.word 0x55545bd
	.word 0xf2f5484
	.word 0x333aa1f
	.word 0xad57b83
	.word 0x29c0d73
	.word 0xe4a8509
	.word 0x34eb62c
	.word 0x70590dc
	.word 0x7dc373b
	.word 0x733ca11
	.word 0x7828adb
	.word 0xbf9fe34
	.word 0xb72f1c9
	.word 0xea33497
	.word 0x4d94feb
	.word 0x931ddb5
	.word 0xcf5065a
	.word 0xa4be9fe
	.word 0x5a32f9
	.word 0x675c902
	.word 0xa9d52ee
	.word 0xea7a1f2
	.word 0xabec2e
	.word 0x5a8ce4f
	.word 0xc3a8c88
	.word 0xec12764
	.word 0x6ec51c5
	.word 0x97ffa22
	.word 0xfc13503
	.word 0x9c2f804
	.word 0xd561a12
	.word 0xa04b46d
	.word 0xdeaf324
	.word 0x318fe06
	.word 0xd2b37b7
	.word 0x8f6b032
	.word 0x50e87e9
	.word 0xdfd288b
	.word 0x9839a36
	.word 0x9648600
	.word 0xba8bf56
	.word 0x7703cd6
	.word 0x97d6b16
	.word 0x61cc185
	.word 0x3dc3dc1
	.word 0x676c7a
	.word 0x8fe3372
	.word 0xb12eb72
	.word 0xadf6901
	.word 0x3d4e450
	.word 0xd3e7f86
	.word 0x312f69b
	.word 0xe3ad5ea
	.word 0xc41b328
	.word 0x18bb8c7
	.word 0xc2c1948
	.word 0x8f98a7e
	.word 0xb4f097c
	.word 0x86d579a
	.word 0xd358869
	.word 0x308d76b
	.word 0x9683b61
	.word 0xa286769
	.word 0xc906258
	.word 0xeff6882
	.word 0x7bc1fd1
	.word 0xb999143
	.word 0x431c466
	.word 0x3d63e89
	.word 0xfe5fb25
	.word 0xd39a7cd
	.word 0xbb362d1
	.word 0xd6cfba6
	.word 0x706212f
	.word 0xb2303d9
	.word 0x1d88c12
	.word 0x9c9dd30
	.word 0x50ebca5
	.word 0xd7b354
	.word 0x6083b56
	.word 0x540f936
	.word 0xcbbde61
	.word 0x4d12a0
	.word 0x277de84
	.word 0xe0b43eb
	.word 0x7ad11bf
	.word 0x8cddf25
	.word 0xed8b18f
	.word 0x985028d
	.word 0x2f0faed
	.word 0xbcc6ce3
	.word 0x66c790b
	.word 0xdef5bd4
	.word 0x4c838c
	.word 0xbafd50f
	.word 0x7a3449a
	.word 0xf469078
	.word 0x920de1f
	.word 0x3282bda
	.word 0xf663688
	.word 0x23abee
	.word 0x7b5d128
	.word 0xbeb429b
	.word 0x884d0e2
	.word 0xa95f3a3
	.word 0xa2ce408
	.word 0x9449dc2
	.word 0x3c6857
	.word 0xa1cd7e5
	.word 0x8fb9123
	.word 0xe92ffa7
	.word 0x8fbb1f6
	.word 0x87f0076
	.word 0x337501
	.word 0x26c135d
	.word 0x3f39a7e
	.word 0x60a0b5d
	.word 0xa4e46fc
	.word 0xa51a55a
	.word 0xb49dfa6
	.word 0xdbb3be4
	.word 0x4f4ebae
	.word 0xa0e2d74
	.word 0x82a8cac
	.word 0xb907221
	.word 0xa16a473
	.word 0x533617b
	.word 0x61eed9f
	.word 0xa4ab836
	.word 0xc1a647a
	.word 0x6d1a299
	.word 0xfb903aa
	.word 0x23e1971
	.word 0xbe0c7c9
	.word 0x70495bf
	.word 0x3b2a38a
	.word 0xdb7cf43
	.word 0x566abb2
	.word 0xe83cbc2
	.word 0x94c9cf2
	.word 0x16e03ce
	.word 0x8d34912
	.word 0x6d4f2f7
	.word 0x28709bb
	.word 0xd0063e8
	.word 0xd20a444
	.word 0xdff8a1
	.word 0xa69b0d8
	.word 0xedd596d
	.word 0x9ab3751
	.word 0xf0a32e7
	.word 0x39f5fbb
	.word 0x2395f2b
	.word 0xb076452
	.word 0xd09d51f
	.word 0x3a87dc4
	.word 0xaf549b3
	.word 0xb5c4df7
	.word 0xe7c4991
	.word 0x2dd04d6
	.word 0x4e39195
	.word 0x484909
	.word 0x217a77
	.word 0x52c0e48
	.word 0x2c20bc
	.word 0x1dbe226
	.word 0xd7b3ab9
	.word 0x40b3ac0
	.word 0x865b66d
	.word 0x92507be
	.word 0x4780ced
	.word 0x8b24e53
	.word 0x10d43d4
	.word 0xa4e9597
	.word 0x7fdf8ba
	.word 0x2c73de1
	.word 0x7f65f02
	.word 0x36c2450
	.word 0x2af0554
	.word 0x28e9b2
	.word 0x8de45a7
	.word 0x5f4bd51
	.word 0x6bcb151
	.word 0xd956c6f
	.word 0x3e9d056
	.word 0x593acfc
	.word 0x9ac7063
	.word 0x23be600
	.word 0x62a3a78
	.word 0x57d8e08
	.word 0xf31dfe
	.word 0xefc0207
	.word 0x943182
	.word 0x18d3d5e
	.word 0xe53c42
	.word 0xe44451c
	.word 0xf886677
	.word 0x6abcc7c
	.word 0x75bfaa5
	.word 0x3711d0b
	.word 0x4e54ba2
	.word 0xe66e465
	.word 0xbfa0ccf
	.word 0x2621a6
	.word 0xf09a6c6
	.word 0x37a2014
	.word 0xeb6532e
	.word 0xbf875b5
	.word 0x54766c5
	.word 0x991cd8d
	.word 0x128de51
	.word 0x70fafd7
	.word 0xbee12f
	.word 0x110bba3
	.word 0x83af29a
	.word 0xb1393d1
	.word 0xa844a61
	.word 0x2d6cf0f
	.word 0x8897739
	.word 0xc85b1bf
	.word 0x2af9b94
	.word 0x8c7a641
	.word 0x97cd301
	.word 0xe75001a
	.word 0xd0224d2
	.word 0x8df1ba
	.word 0x4735b4b
	.word 0xb56318a
	.word 0xcc3bfc
	.word 0x7e801c4
	.word 0x944be11
	.word 0xfde6967
	.word 0x284acad
	.word 0x444d1b4
	.word 0xb14244f
	.word 0xa8a405a
	.word 0x13285e8
	.word 0x59d3ae4
	.word 0x625d9d0
	.word 0x137240f
	.word 0x7421488
	.word 0xda3531b
	.word 0x7e5b2c6
	.word 0x1c46896
	.word 0xf8dbb6a
	.word 0x2892bc1
	.word 0xf8a78c2
	.word 0x8a7b826
	.word 0x90b1714
	.word 0xfc2abbe
	.word 0xf9d1c35
	.word 0x91421e1
	.word 0xf5ae303
	.word 0x3d01e28
	.word 0x7e33aa4
	.word 0x70883ed
	.word 0x5064e0b
	.word 0x193062d
	.word 0xfc4ecc9
	.word 0x7a689b2
	.word 0x499c8ca
	.word 0x9e2936e
	.word 0xe8a78d6
	.word 0x166d10b
	.word 0x28ec721
	.word 0xb2242c1
	.word 0xf05dded
	.word 0xf34a026
	.word 0x43a1795
	.word 0xbd93164
	.word 0x7c6eae2
	.word 0xbe8acaf
	.word 0x9a99ee3
	.word 0x35e235d
	.word 0x2dc779c
	.word 0xef0fba3
	.word 0xb09ca1c
	.word 0x4a7161
	.word 0x50b452d
	.word 0xf036c25
	.word 0xc125c1a
	.word 0x1aece33
	.word 0xe0c4bcb
	.word 0x17782a6
	.word 0x775e278
	.word 0x8dd5798
	.word 0x378f60a
	.word 0xbaaa3e5
	.word 0x35e36f9
	.word 0x6879f3d
	.word 0xff21989
	.word 0x4659ffb
	.word 0xb33c6a
	.word 0x3ff969
	.word 0xda8a595
	.word 0x7854a58
	.word 0xd9f6cdb
	.word 0xd425020
	.word 0x3512f2
	.word 0xfde99e5
	.word 0xda7d83a
	.word 0xaef7007
	.word 0x6997630
	.word 0x5adf22a
	.word 0x83605dd
	.word 0x7f21657
	.word 0xcc04d01
	.word 0x7856f19
	.word 0x8c591a8
	.word 0xe862f06
	.word 0x400a0c6
	.word 0x47f98c2
	.word 0xb4c3f1d
	.word 0x3943d33
	.word 0x6044473
	.word 0xe31baa9
	.word 0x4fdf65
	.word 0xd761b70
	.word 0x14e670
	.word 0x9756ef4
	.word 0x65bfd6a
	.word 0xb51a1dc
	.word 0x11c1a48
	.word 0x93d14f8
	.word 0x43da22b
	.word 0x84c1b9
	.word 0xf50aa52
	.word 0x49652e1
	.word 0xbb08edc
	.word 0xb0c02af
	.word 0x175e5a9
	.word 0x5d5dd5d
	.word 0xc8b8cfc
	.word 0x6258d99
	.word 0x540db04
	.word 0x41cfeb9
	.word 0x65458ef
	.word 0x1830ca8
	.word 0xe78d9ea
	.word 0xbd3911
	.word 0xbdafdfa
	.word 0xcdbeeb4
	.word 0xeff7a5
	.word 0x71d73c0
	.word 0xe5c38ab
	.word 0xdbe3001
	.word 0x21a831d
	.word 0xe39e590
	.word 0xe74f939
	.word 0x8be97be
	.word 0xb2d5501
	.word 0x2545a08
	.word 0xd7c0c34
	.word 0xbe181cf
	.word 0x1bdf552
	.word 0x1f15cfa
	.word 0xedbc2d8
	.word 0x36be36d
	.word 0xd2af608
	.word 0x2b97e51
	.word 0xb6042ef
	.word 0xcf1b9d0
	.word 0xa1968ea
	.word 0xf860dc3
	.word 0xaf049c5
	.word 0x714459d
	.word 0x8a34ae4
	.word 0x2f4f653
	.word 0xcf52d93
	.word 0xcf951e1
	.word 0xab4dc68
	.word 0xc2da13a
	.word 0x9fd0f21
	.word 0x9e8e7ee
	.word 0x8f46a3e
	.word 0xdb362cd
	.word 0x1ab1675
	.word 0x2966f14
	.word 0x317ba70
	.word 0xb6676de
	.word 0xf52ca7e
	.word 0xfcb1b75
	.word 0x767047
	.word 0x5e72626
	.word 0x278bab1
	.word 0x496c32c
	.word 0x43c7e2a
	.word 0x3fbb783
	.word 0x4da5e49
	.word 0xbdd2414
	.word 0xe50a61a
	.word 0xdb78977
	.word 0xb15c6e3
	.word 0x39ab37c
	.word 0x8a36e09
	.word 0xbb8194b
	.word 0xfc85319
	.word 0xccf5339
	.word 0xb914ef3
	.word 0xe911001
	.word 0xf661860
	.word 0x5b02a5a
	.word 0x6b5af14
	.word 0x334a0df
	.word 0x596c24
	.word 0x35de1b1
	.word 0xe74a039
	.word 0xe5ead9f
	.word 0xe0940fc
	.word 0x754c40d
	.word 0x2bcf801
	.word 0xd450955
	.word 0x62d5a63
	.word 0x8268ef8
	.word 0x6f4f917
	.word 0xbcd9eb
	.word 0xf6781bd
	.word 0xea7b78
	.word 0x17588ee
	.word 0x15976fc
	.word 0xbb3a4f1
	.word 0x73903c0
	.word 0xc60cbfe
	.word 0x5b2df40
	.word 0x7376495
	.word 0x70136
	.word 0x7b45c35
	.word 0x236887b
	.word 0xd997abe
	.word 0xbd21b33
	.word 0xf8444dc
	.word 0xbada9d4
	.word 0x56c8a89
	.word 0x465c213
	.word 0xddff90c
	.word 0x279c10d
	.word 0x60628b7
	.word 0xca1d0d3
	.word 0x6080332
	.word 0x88d8f2
	.word 0x18a6e77
	.word 0x78ffe00
	.word 0x3d73867
	.word 0xb594b39
	.word 0x3843860
	.word 0x2014073
	.word 0xa210cef
	.word 0x2767037
	.word 0xcbcda57
	.word 0x73a6479
	.word 0x2f89559
	.word 0x8014fbe
	.word 0xb8dd989
	.word 0x1217a93
	.word 0x7ecced9
	.word 0xaf0b898
	.word 0xff317b6
	.word 0x84066d4
	.word 0xc74a463
	.word 0xcdceab6
	.word 0x98124c1
	.word 0xe1fb655
	.word 0x2cc71fc
	.word 0xe875244
	.word 0x5b55b9e
	.word 0x961d844
	.word 0xcb138dd
	.word 0xc550231
	.word 0xf90e164
	.word 0x3ca179f
	.word 0xeb16ebf
	.word 0x2107785
	.word 0x1f65fe5
	.word 0x5a2ba12
	.word 0x9f3f57
	.word 0xd4da8c0
	.word 0xc7a7c1a
	.word 0xbba8a13
	.word 0xb837524
	.word 0x5700041
	.word 0xc255fe7
	.word 0x38403f9
	.word 0x73a685e
	.word 0x64c4579
	.word 0xb6c50dc
	.word 0xa11f079
	.word 0x94db7a0
	.word 0x9a1b219
	.word 0xbfba781
	.word 0xd3eb64d
	.word 0x5cbf54b
	.word 0xac02b93
	.word 0x7308d62
	.word 0x53b0ed4
	.word 0x893b1b2
	.word 0x79ae2a1
	.word 0xcac3f73
	.word 0x1deac17
	.word 0xcaec4b7
	.word 0xc4eff77
	.word 0x254b08d
	.word 0x8138ec2
	.word 0xcd60578
	.word 0x1970a98
	.word 0x98a745e
	.word 0x2ec8905
	.word 0x94e9de5
	.word 0x5be0126
	.word 0xf473321
	.word 0xb9afb6c
	.word 0xb010cde
	.word 0x737c0c4
	.word 0xb09809d
	.word 0x8aa7496
	.word 0x8c485b9
	.word 0xde5d01
	.word 0x20a2378
	.word 0x6290591
	.word 0x40a59cb
	.word 0xe9f80f4
	.word 0xb531941
	.word 0x57745b4
	.word 0x8ac5541
	.word 0x18aa65b
	.word 0xc178e36
	.word 0x9ba41a5
	.word 0xb655268
	.word 0xcc4b3c2
	.word 0x3032b61
	.word 0xab56011
	.word 0x9ce89c3
	.word 0xda7a74e
	.word 0x6a3c361
	.word 0xb7d1854
	.word 0x40b6d84
	.word 0xff3ac76
	.word 0xed52dc4
	.word 0x78647db
	.word 0xe705744
	.word 0xb97fba8
	.word 0x84dbe8e
	.word 0x775ba60
	.word 0xd38a38e
	.word 0x896f4be
	.word 0x5121c8f
	.word 0xeecdb29
	.word 0xba762f7
	.word 0xaba2694
	.word 0xcb98eb0
	.word 0xf644a98
	.word 0x8abc332
	.word 0xce6822d
	.word 0xd26c4f8
	.word 0x500dd77
	.word 0xb0f8f66
	.word 0x2937f26
	.word 0x579b616
	.word 0xcd5e372
	.word 0xfd96f54
	.word 0x4017e2c
	.word 0xd12e762
	.word 0x9c3df1c
	.word 0x214c3f3
	.word 0xda3655
	.word 0xccdbd63
	.word 0x767e97f
	.word 0x559487b
	.word 0xc33e289
	.word 0x98f2fc6
	.word 0xffbc481
	.word 0xdf5523c
	.word 0x10d9ad5
	.word 0xa9ca054
	.word 0x7701583
	.word 0x30c3032
	.word 0x2eeb094
	.word 0xf53651c
	.word 0x71ea587
	.word 0x7b4c056
	.word 0x3baa561
	.word 0x23af0c9
	.word 0xb4253ee
	.word 0x2ef0506
	.word 0x6f96ef6
	.word 0x7ac0cfd
	.word 0x1b249fe
	.word 0x5d5ec91
	.word 0x3037eb1
	.word 0x457592e
	.word 0x8d076af
	.word 0xb8d909e
	.word 0xc753dca
	.word 0x521c462
	.word 0x54ecfe9
	.word 0x7f374f5
	.word 0xdfe55fb
	.word 0xa9464ba
	.word 0x9d9b5da
	.word 0x8d8808b
	.word 0xcc01e63
	.word 0xd15dadf
	.word 0xc8d5e18
	.word 0x36c351
	.word 0x23e6777
	.word 0xee430f8
	.word 0xdced593
	.word 0xc0f788e
	.word 0x477b9a0
	.word 0xdbb8a33
	.word 0x31c6be1
	.word 0xc28cbba
	.word 0xa545f0e
	.word 0x5b2ea1
	.word 0xd8f6452
	.word 0xb2c389
	.word 0x70ae63a
	.word 0xa0d60b9
	.word 0x2460d6f
	.word 0x254866f
	.word 0x76832a9
	.word 0xbc1415b
	.word 0xe2c7f10
	.word 0x8e7cc3b
	.word 0xfcd2e30
	.word 0xa3e3aea
	.word 0xf37f86
	.word 0x6028a2
	.word 0x733d6df
	.word 0xbcb7311
	.word 0xc2d8311
	.word 0x1ed40e7
	.word 0x9002b48
	.word 0xfa57031
	.word 0x6753370
	.word 0xbbae013
	.word 0x58ceecf
	.word 0x9d266af
	.word 0x54c468b
	.text
	.align	2
	.globl	main
	.type	main, @function
main:
	lui	x8, %hi(initialMemory)
	add	x8, x8, %lo(initialMemory)
	addi x8, x8, 2040
  lui	t0,0x3					# enable FPU
  csrs	mstatus,t0			# enable FPU
	lw x1, 4(x8)
	lw x2, 8(x8)
	lw x3, 12(x8)
	lw x4, 16(x8)
	lw x5, 20(x8)
	lw x6, 24(x8)
	lw x7, 28(x8)
	lw x9, 36(x8)
	lw x10, 40(x8)
	lw x11, 44(x8)
	lw x12, 48(x8)
	lw x13, 52(x8)
	lw x14, 56(x8)
	lw x15, 60(x8)
	lw x16, 64(x8)
	lw x17, 68(x8)
	lw x18, 72(x8)
	lw x19, 76(x8)
	lw x20, 80(x8)
	lw x21, 84(x8)
	lw x22, 88(x8)
	lw x23, 92(x8)
	lw x24, 96(x8)
	lw x25, 100(x8)
	lw x26, 104(x8)
	lw x27, 108(x8)
	lw x28, 112(x8)
	lw x29, 116(x8)
	lw x30, 120(x8)
	lw x31, 124(x8)
	flw f0, 0(x8)
	flw f1, 4(x8)
	flw f2, 8(x8)
	flw f3, 12(x8)
	flw f4, 16(x8)
	flw f5, 20(x8)
	flw f6, 24(x8)
	flw f7, 28(x8)
	flw f8, 32(x8)
	flw f9, 36(x8)
	flw f10, 40(x8)
	flw f11, 44(x8)
	flw f12, 48(x8)
	flw f13, 52(x8)
	flw f14, 56(x8)
	flw f15, 60(x8)
	flw f16, 64(x8)
	flw f17, 68(x8)
	flw f18, 72(x8)
	flw f19, 76(x8)
	flw f20, 80(x8)
	flw f21, 84(x8)
	flw f22, 88(x8)
	flw f23, 92(x8)
	flw f24, 96(x8)
	flw f25, 100(x8)
	flw f26, 104(x8)
	flw f27, 108(x8)
	flw f28, 112(x8)
	flw f29, 116(x8)
	flw f30, 120(x8)
	flw f31, 124(x8)
i_3:
	addi x2, x12, -184
i_4:
	slt x7, x6, x9
i_5:
	add x7, x17, x26
i_6:
	divu x26, x15, x17
i_7:
	blt x28, x7, i_16
i_8:
	srli x29, x23, 4
i_9:
	auipc x7, 830951
i_10:
	andi x22, x9, 1332
i_11:
	addi x12, x0, 8
i_12:
	srl x14, x1, x12
i_13:
	lui x30, 581036
i_14:
	mulh x22, x26, x12
i_15:
	slli x12, x2, 4
i_16:
	xori x12, x30, -638
i_17:
	srli x3, x12, 1
i_18:
	sltiu x24, x13, 981
i_19:
	sub x18, x8, x14
i_20:
	fmadd.s f4, f24, f24, f11
i_21:
	addi x12, x27, -1003
i_22:
	auipc x12, 606433
i_23:
	addi x31, x0, 12
i_24:
	sra x11, x16, x31
i_25:
	div x20, x1, x17
i_26:
	slt x18, x15, x13
i_27:
	xori x12, x31, 1820
i_28:
	div x7, x7, x4
i_29:
	mul x18, x7, x14
i_30:
	sltiu x31, x1, -1419
i_31:
	fmax.s f23, f24, f21
i_32:
	slt x28, x18, x18
i_33:
	sltiu x14, x22, -1293
i_34:
	auipc x25, 351640
i_35:
	blt x7, x7, i_38
i_36:
	addi x11, x0, 2
i_37:
	srl x11, x11, x11
i_38:
	sub x1, x22, x25
i_39:
	mul x22, x24, x11
i_40:
	rem x11, x4, x19
i_41:
	srai x24, x3, 1
i_42:
	mulhsu x11, x29, x21
i_43:
	and x11, x9, x9
i_44:
	fmadd.s f19, f8, f24, f0
i_45:
	mulh x15, x10, x1
i_46:
	slt x26, x24, x15
i_47:
	rem x27, x26, x27
i_48:
	fmax.s f30, f24, f2
i_49:
	srli x15, x20, 3
i_50:
	and x18, x10, x14
i_51:
	blt x11, x1, i_61
i_52:
	fmadd.s f23, f6, f14, f7
i_53:
	mulhu x22, x28, x24
i_54:
	mulh x12, x16, x24
i_55:
	addi x12, x0, 25
i_56:
	sll x16, x30, x12
i_57:
	add x22, x22, x18
i_58:
	lui x16, 46665
i_59:
	divu x19, x16, x9
i_60:
	add x16, x21, x1
i_61:
	mulh x16, x18, x24
i_62:
	mul x18, x31, x22
i_63:
	ori x31, x9, -982
i_64:
	addi x4, x0, 19
i_65:
	sll x4, x6, x4
i_66:
	mulhsu x15, x11, x8
i_67:
	divu x7, x11, x28
i_68:
	sub x18, x31, x8
i_69:
	xor x11, x23, x30
i_70:
	or x21, x10, x9
i_71:
	sltiu x21, x25, -930
i_72:
	and x30, x14, x8
i_73:
	xor x21, x21, x16
i_74:
	mulhsu x21, x2, x17
i_75:
	div x21, x28, x4
i_76:
	bltu x18, x4, i_82
i_77:
	slti x21, x16, -187
i_78:
	add x21, x19, x20
i_79:
	mulhu x20, x30, x31
i_80:
	and x18, x10, x9
i_81:
	add x31, x29, x6
i_82:
	add x6, x8, x15
i_83:
	bge x4, x5, i_94
i_84:
	auipc x5, 327100
i_85:
	divu x22, x13, x17
i_86:
	addi x19, x0, 27
i_87:
	srl x2, x29, x19
i_88:
	fnmsub.s f29, f8, f29, f24
i_89:
	bltu x22, x11, i_92
i_90:
	rem x16, x6, x30
i_91:
	addi x16, x31, -1162
i_92:
	andi x14, x17, -1935
i_93:
	mulh x11, x18, x16
i_94:
	remu x2, x15, x30
i_95:
	fsqrt.s f22, f1
i_96:
	slti x5, x11, 376
i_97:
	andi x20, x2, 1557
i_98:
	auipc x22, 928272
i_99:
	sltu x19, x11, x14
i_100:
	nop
i_101:
	nop
i_102:
	nop
i_103:
	nop
i_104:
	nop
i_105:
	nop
i_106:
	nop
i_107:
	nop
i_108:
	nop
i_109:
	nop
i_110:
	nop
i_111:
	nop

  	csrw mtohost, 1;
1:
	j 1b
	.size	main, .-main
	.ident	"AAPG"
