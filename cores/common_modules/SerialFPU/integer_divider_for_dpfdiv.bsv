/*
-------------------------------------------------------------------------------

Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
-------------------------------------------------------------------------------

Module Name     : Integer Divider used in DP FP division 
Author Name     : Arjun C. Menon, Aditya Govardhan
Email Id        : c.arjunmenon@gmail.com, dtgovardhan@gmail.com
Last updaed on  : 2nd June, 2016.

This module performs the integer division used in the DP FP division calculation. The dividend is 114 bits wide and the divisor is 56 bits.
This division is performed in 28 steps or 28 clock cycles. During every clock cycle, we perform two steps of division by subtraction.
This is performed using the function fn_divide_step. At each cycle the copmuted value is enqueued in the next FIFO in the pipe.

*/

package integer_divider_for_dpfdiv;
import FIFO::*;
import SpecialFIFOs::*;

//function to perform two steps of division by subtraction
/*(*noinline*)
function Bit#(170) fn_divide_step(Bit#(56) denominator, Bit#(114) quotient_and_remainder);
	let temp=quotient_and_remainder;
    let x= temp[56:0]-{1'b0,denominator};
	if(quotient_and_remainder[56:0]<{1'b0,denominator})         // if Dr > Nr
		temp= {temp[113:58],1'b0,temp[56:0]};     // Quotient bit is 0
	else
		temp = {temp[113:58],1'b1,x};             // Quotient bit is 1 and new remainder is calculated by subtracting denominator from numerator
	temp = temp<<1;                               // shifting {Partial Quotient, Partial Remainder} which append a zero at the LSB of the partial remainder
    
   let y= temp[56:0]-{1'b0,denominator};          // if Dr > Nr
   if(temp[56:0]<{1'b0,denominator})              // Quotient bit is 0
       temp = {temp[113:58],1'b0,temp[56:0]};                                                                                                                
   else                                           // Quotient bit is 1 and new remainder is calculated by subtracting denominator from numerator
       temp = {temp[113:58],1'b1,y};              // shifting {Partial Quotient, Partial Remainder} which append a zero at the LSB of the partial remainder
    temp = temp<<1;
    
	return {denominator,temp};
endfunction
*/

(* noinline *)
function Bit#(170) fn_divide_step_64 (Bit#(56) _divisor, Bit#(58) _remainder, Bit#(56) _dividend, Bool final_stage);
	if(final_stage == False) begin
		Bit#(114) accumulator = 0;

	    if(_remainder[57]==1'b0) begin
			accumulator = ({_remainder,_dividend}<<1) - {1'b0,_divisor,1'b0,56'b0} ;
			accumulator[0] = 1'b1;
		end
		else begin
			accumulator = ({_remainder,_dividend}<<1) + {1'b0,_divisor,1'b0,56'b0} ;
			accumulator[0] = 1'b0;
		end
		_remainder = accumulator[113:56];
		_dividend = accumulator[55:0];
	end
	else begin
	    _dividend = _dividend - (_dividend ^ ('d-1));

	    if(_remainder[57] == 1'b1) begin
		    _remainder = _remainder + {1'b0,_divisor,1'b0};
		    _dividend = _dividend - 1;
	    end
	end
	return {_divisor, _remainder, _dividend};
endfunction

interface Ifc_integer_divider_for_dpfdiv;

		/* Input Methods */
	method Action _inputs(Bit#(56) _denominator, Bit#(56) _numerator);
	method Action _remove_last_entry();
    method Action _set_flush(Bool _flush);

		/* Output Methods */
	method Bit#(170) output_();
endinterface:Ifc_integer_divider_for_dpfdiv

(*synthesize*)
module mkinteger_divider_for_dpfdiv(Ifc_integer_divider_for_dpfdiv);
FIFO#(Bit#(170)) ff_stage1 <- mkPipelineFIFO();
FIFO#(Bit#(170)) ff_stage57 <-mkPipelineFIFO();
Reg#(Bit#(170)) rg_inter_stage <- mkReg(0);
Reg#(Bit#(6)) rg_state <- mkReg(0);

/* The following set of rules perform the computation. Each rule fires only when the previous FIFO is
filled. and the next FIFO is empty. 
Each rule calls the function fn_divide_step_64 to perform the single step of division.
*/
rule stage_1(rg_state == 1);
     let stage_data = ff_stage1.first();
     ff_stage1.deq();
     rg_inter_stage <= fn_divide_step_64(stage_data[169:114], stage_data[113:56], stage_data[55:0],False);
endrule

rule rl_inter_stage(rg_state > 1 && rg_state < 56);
    rg_inter_stage <= fn_divide_step_64(rg_inter_stage[169:114], rg_inter_stage[113:56], rg_inter_stage[55:0], False);
    rg_state <= rg_state + 1;
endrule

rule rl_end_stage(rg_state == 56);
    ff_stage57.enq(fn_divide_step_64(rg_inter_stage[169:114], rg_inter_stage[113:56], rg_inter_stage[55:0], True));
    rg_state <= rg_state + 1;
endrule



// rule rl_ff_stage_58(!wr_flush);
// 	ff_stage58.enq(fn_divide_step(ff_stage57.first()[169:114],ff_stage57.first()[113:56],ff_stage57.first()[55:0], True));
// 	$display("divisor = %b, remainder = %b, dividend = %b", ff_stage57.first()[169:114],ff_stage57.first()[113:56],ff_stage57.first()[55:0]);
// 	ff_stage57.deq();
// endrule:rl_ff_stage_58


method Action _inputs(Bit#(56) _denominator, Bit#(56) _numerator);
	ff_stage1.enq(fn_divide_step_64(_denominator, {2'b0,_numerator}, 56'b0, False));
endmethod

method Bit#(170) output_();
	return ff_stage57.first();
endmethod

method Action _remove_last_entry();
	ff_stage57.deq();
endmethod

// when a wr_flush is initiated in the processor this method will also be called.
// it sets the flsuh wire to True, hence no rule other flush_all_fifos will
// fire and hence clear all the buffer and intermediate register/ wires ...

endmodule:mkinteger_divider_for_dpfdiv


		/* 	TEST BENCH 	*/

//(*synthesize*)
module mkTb(Empty);

Reg#(Bit#(32)) rg_clock <-mkReg(0);

Ifc_integer_divider_for_dpfdiv instance_divider <-mkinteger_divider_for_dpfdiv();

rule rl_count_clock ;
	rg_clock<=rg_clock+1;
	$display("Clock=%d",rg_clock);
	if(rg_clock=='d60)
		$finish(0);
endrule:rl_count_clock

rule rl_input1(rg_clock==1);
	//instance_divider._inputs({6'b100011,'d0},{3'b111,'d0});
	instance_divider._inputs({3'b111, 53'b0}, {6'b100011, 50'b0});
	$display("%b %b", {3'b111, 53'b0}, {6'b100011, 50'b0});
endrule:rl_input1

rule rl_check;
	$display("Quotient=%b Remainder=%b",instance_divider.output_[55:0],instance_divider.output_[113:56]);
	instance_divider._remove_last_entry();
endrule
endmodule
endpackage:integer_divider_for_dpfdiv
