/*

Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Module Name     : Integer Divider for Single Precision Floating Point Divider
Author Name     : Arjun C. Menon, Vinod.G, Aditya Govardhan
Email ID        : c.arjunmenon@gmail.com, g.vinod1993@gmail.com, dtgovardhan@gmail.com 
Last updated on : 12th July, 2016

*/

package integer_divider_for_spdiv;


import FIFO :: *;
import SpecialFIFOs :: *;

typedef struct{
	Bit#(83) data;
	Bit#(10) exponent;
	Bit#(1) sign;
	Bit#(1) infinity;
	Bit#(1) invalid;
	Bit#(1) dz;
	Bit#(1) zero;
	Bit#(32) fsr;
	Bit#(3) rounding_mode;
	}Intermediate_data deriving (Bits,Eq);

(* noinline *)
function Bit#(83) fn_divide_step (Bit#(27) _divisor, Bit#(29) _remainder, Bit#(27) _dividend, Bit#(1) final_stage);
	if(final_stage == 0) begin
		Bit#(56) accumulator = 0;

	    if(_remainder[28]==1'b0) begin
			accumulator = ({_remainder,_dividend}<<1) - {1'b0,_divisor,1'b0,27'b0} ;
			accumulator[0] = 1'b1;
		end
		else begin
			accumulator = ({_remainder,_dividend}<<1) + {1'b0,_divisor,1'b0,27'b0} ;
			accumulator[0] = 1'b0;
		end
		_remainder = accumulator[55:27];
		_dividend = accumulator[26:0];
	end
	else begin
	    _dividend = _dividend - (_dividend ^ ('d-1));

	    if(_remainder[28] == 1'b1) begin
		    _remainder = _remainder + {1'b0,_divisor,1'b0};
		    _dividend = _dividend - 1;
	    end
	end
	return {_divisor, _remainder, _dividend};
endfunction

interface Ifc_integer_divider_for_spdiv;

	method Action _inputs(Bit#(27) _denominator, Bit#(27) _numerator, Bit#(10) exponent, Bit#(1) sign, Bit#(1) infinity, Bit#(1) invalid, Bit#(1) dz, Bit#(1) zero, Bit#(32) fsr, Bit#(3) rounding_mode);
	method Intermediate_data result_();
	method Action _remove_last_entry();

endinterface

(* synthesize *)
module mkinteger_divider_for_spdiv(Ifc_integer_divider_for_spdiv);

	Reg#(Intermediate_data) rg_inter_stage <- mkRegU();
	Reg#(Bit#(5)) rg_state <- mkReg(0);
	FIFO#(Intermediate_data) ff_stage1 <- mkPipelineFIFO();
	FIFO#(Intermediate_data) ff_final_out <- mkPipelineFIFO();


	rule stage_1(rg_state == 1);
		let stage_data = ff_stage1.first();
		ff_stage1.deq();
		rg_state <= rg_state + 1;
		rg_inter_stage <= Intermediate_data{
						  data: fn_divide_step(stage_data.data[82:56],stage_data.data[55:27],stage_data.data[26:0],0),
						  exponent : stage_data.exponent,
						  sign : stage_data.sign,
						  infinity : stage_data.infinity,
						  invalid : stage_data.invalid,
						  dz : stage_data.dz,
						  zero : stage_data.zero,
						  fsr : stage_data.fsr,
						  rounding_mode : stage_data.rounding_mode };

	endrule

	rule recursive_stage(rg_state > 1 && rg_state < 27);
	rg_state <= rg_state + 1;
	rg_inter_stage <= Intermediate_data{
						  data: fn_divide_step(rg_inter_stage.data[82:56],rg_inter_stage.data[55:27],rg_inter_stage.data[26:0],0),
						  exponent : rg_inter_stage.exponent,
						  sign : rg_inter_stage.sign,
						  infinity : rg_inter_stage.infinity,
						  invalid : rg_inter_stage.invalid,
						  dz : rg_inter_stage.dz,
						  zero : rg_inter_stage.zero,
						  fsr : rg_inter_stage.fsr,
						  rounding_mode : rg_inter_stage.rounding_mode };

	endrule

	rule end_stage(rg_state == 27);
        rg_state <= rg_state + 1;
		ff_final_out.enq(Intermediate_data{
						  data: fn_divide_step(rg_inter_stage.data[82:56],rg_inter_stage.data[55:27],rg_inter_stage.data[26:0],1),
						  exponent : rg_inter_stage.exponent,
						  sign : rg_inter_stage.sign,
						  infinity : rg_inter_stage.infinity,
						  invalid : rg_inter_stage.invalid,
						  dz : rg_inter_stage.dz,
						  zero : rg_inter_stage.zero,
						  fsr : rg_inter_stage.fsr,
						  rounding_mode : rg_inter_stage.rounding_mode }
					  );
     
	endrule

	method Action _inputs(Bit#(27) _denominator, Bit#(27) _numerator, Bit#(10) exponent, Bit#(1) sign, Bit#(1) infinity, Bit#(1) invalid, Bit#(1) dz, Bit#(1) zero, Bit#(32) fsr, Bit#(3) rounding_mode);
		rg_state <= rg_state + 1;
		ff_stage1.enq(	Intermediate_data{
						data: fn_divide_step(_denominator, {2'b0,_numerator}, 27'b0, 0), 
						exponent: exponent, 
						sign: sign, 
						infinity: infinity, 
						invalid: invalid, 
						dz: dz, 
						zero: zero, 
						fsr: fsr, 
						rounding_mode: rounding_mode}
						);
	endmethod

	method Intermediate_data result_();
		return ff_final_out.first();
	endmethod

	method Action _remove_last_entry();
		ff_final_out.deq();
		rg_state <= 0;
	endmethod

endmodule

// (*synthesize*)
// module mkTb (Empty);

//     Reg#(Bit#(32)) rg_clock <-mkReg(0);

//     Ifc_integer_divider_for_spdiv instance_divider <-mkinteger_divider_for_spdiv();

//     rule rl_count_clock ;
//       	rg_clock<=rg_clock+1;
//       	if(rg_clock=='d45) $finish(0);
//     endrule

//     rule rl_input1(rg_clock==1);
// 		$display("giving inputat %0d", rg_clock);
//         instance_divider._inputs(27'd7, 27'd36 ); // divisor, dividend 
//     endrule

//     rule rl_finish;
// 		$display("Quotient=%b mainder=%b at %0d",instance_divider.output_quotient,instance_divider.output_remainder, rg_clock);
//         instance_divider._remove_last_entry();
//     endrule

// endmodule

endpackage
