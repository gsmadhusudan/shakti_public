/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Author Names : Rahul Bodduna, Debiprasanna Sahoo.
Email ID : rahul.bodduna@gmail.com
*/


import riscv_types::*;

function PseudoLRU_type updatePseudoLRU(PseudoLRU_type pseudoLRU,Integer count);
	case(count)
	0	:	return	PseudoLRU_type	{
			 _ab_cd_select:'d1,
			    _ab_select:'d1, 
			    _cd_select:pseudoLRU._cd_select	};
	1	:	return	PseudoLRU_type	{
			 _ab_cd_select:'d1,
			    _ab_select:'d0, 
			    _cd_select:pseudoLRU._cd_select	};

	2	:	return	PseudoLRU_type	{
			 _ab_cd_select:'d0,
			    _ab_select:pseudoLRU._ab_select, 
			    _cd_select:'d1	};
	3	:	return	PseudoLRU_type	{
			 _ab_cd_select:'d0,
			    _ab_select:pseudoLRU._ab_select, 
			    _cd_select:'d0	};
	endcase
endfunction : updatePseudoLRU

function ReplacementInformation victimWay(PseudoLRU_type pseudolru);
	ReplacementInformation	replacementInfo;
	if(pseudolru._ab_cd_select == 'd0)
	begin
		if(pseudolru._ab_select == 'd0)
		begin
			replacementInfo.plru	=	PseudoLRU_type	{
					 _ab_cd_select:'d1,
					    _ab_select:'d1, 
					    _cd_select:pseudolru._cd_select	};
			replacementInfo.way	=	0;
		end
		else
		begin
			replacementInfo.plru	=	PseudoLRU_type	{
					 _ab_cd_select:'d1,
					    _ab_select:'d0,
					    _cd_select:pseudolru._cd_select	};
			replacementInfo.way	=	1;
		end
	end
	else
	begin
		if(pseudolru._cd_select == 'd0)
		begin
			replacementInfo.plru	=	PseudoLRU_type	{
					 _ab_cd_select:'d0,
					    _ab_select:pseudolru._ab_select,
					    _cd_select:'d1	};				
			replacementInfo.way	=	2;
		end
		else
		begin
			replacementInfo.plru	=	PseudoLRU_type	{
					 _ab_cd_select:'d0,
					    _ab_select:pseudolru._ab_select,						         
					    _cd_select:'d0	}; 
			replacementInfo.way	=	3;
		end
	end
	return replacementInfo;
endfunction : victimWay

