`define NUM_INSTRS 20200 
`define MEM_INIT_SIZE 1048576
`define STOP 100000


`define ENTRY_ROB_SIZE 16
`define ENTRY_ROB_INDEX_SIZE 4
`define IQ_SIZE 8
`define TOTAL_THREADS 4
`define REG_WIDTH 64
`define INSTR_WIDTH 32
`define SIMD_REG_WIDTH 256
`define REGFILE_SIZE 32
`define PRF_SIZE 128
`define ADDRESS_WIDTH 64
`define FETCH_WIDTH 2
`define ISSUE_WIDTH 4
`define MEMQ_SIZE 16
`define DCACHE_SIZE 64 
`define ICACHE_SIZE 64
`define IMM_BUF_SIZE 8
`define Addr_width 64
`define Word_size 8
`define CSR_ADDR 12
`define User_mode 0
`define Supervisor_mode 1
`define Hypervisor_mode 2
`define Machine_mode 3
`define MAX_LATENCY 6

//////////////////////////////////////// TLB 	     ////////////////////////////////////////////////////////////////

//////////////////////////////////////// Page and Frame related details /////////////////////////////////////////////

`define  _virtual_page_no_width			52	// Specifies the width of the virtual page number

`define  _physical_frame_no_width		28	// Specifies the width of the physical frame number

`define  _offset_width				12	// Specifies the width of the offset bits

`define  _virtual_page_no			[63:12] // Part of the virtual address which is page number

`define  _physical_frame_no			[39:12] // Part of the physical address which is frame number

`define  _offset				[11:0]  // part of the virtual or physical address used as offset



//////////////////////////////////////// Variable Page Size Configurations //////////////////////////////////////////

`define  _no_of_page_sizes			11

`define  _virtual_page_no_4Kb			[63:12]

`define  _4Kb					[51:0]

`define  _pfn_4Kb				[27:0]

`define  _offset_4Kb				[11:0]

`define  _itlb_vps_page_index_4Kb		[17:12]


`define  _virtual_page_no_16Kb			[63:14]

`define  _16Kb					[49:0]

`define  _pfn_16Kb				[25:0]

`define  _offset_16Kb				[13:0]

`define  _itlb_vps_page_index_16Kb		[19:14]


`define  _virtual_page_no_64Kb			[63:16]

`define  _64Kb					[47:0]

`define  _pfn_64Kb				[23:0]

`define  _offset_64Kb				[15:0]

`define  _itlb_vps_page_index_64Kb		[21:16]


`define  _virtual_page_no_256Kb			[63:18]

`define  _256Kb					[45:0]

`define  _pfn_256Kb				[21:0]

`define  _offset_256Kb				[17:0]

`define  _itlb_vps_page_index_256Kb		[23:18]


`define  _virtual_page_no_1Mb			[63:20]

`define  _1Mb					[43:0]

`define  _pfn_1Mb				[19:0]

`define  _offset_1Mb				[19:0]

`define  _itlb_vps_page_index_1Mb		[25:20]


`define  _virtual_page_no_4Mb			[63:22]

`define  _4Mb					[41:0]

`define  _pfn_4Mb				[17:0]

`define  _offset_4Mb				[21:0]

`define  _itlb_vps_page_index_4Mb		[27:22]



`define  _virtual_page_no_16Mb			[63:24]

`define  _16Mb					[39:0]

`define  _pfn_16Mb				[15:0]

`define  _offset_16Mb				[23:0]

`define  _itlb_vps_page_index_16Mb		[29:24]


`define  _virtual_page_no_64Mb			[63:26]

`define  _64Mb					[37:0]

`define  _pfn_64Mb				[13:0]

`define  _offset_64Mb				[25:0]

`define  _itlb_vps_page_index_64Mb		[31:26]


`define  _virtual_page_no_256Mb			[63:28]

`define  _256Mb					[35:0]

`define  _pfn_256Mb				[11:0]

`define  _offset_256Mb				[27:0]

`define  _itlb_vps_page_index_256Mb		[33:28]


`define  _virtual_page_no_1Gb			[63:30]

`define  _1Gb					[33:0]

`define  _pfn_1Gb				[9:0]

`define  _offset_1Gb				[29:0]

`define  _itlb_vps_page_index_1Gb		[35:30]


`define  _virtual_page_no_4Gb			[63:32]

`define  _4Gb					[31:0]

`define  _pfn_4Gb				[7:0]

`define  _offset_4Gb				[31:0]

`define  _itlb_vps_page_index_4Gb		[37:32]

///////////////////////////////////////// Page Identifier details      /////////////////////////////////////////////

`define  _page_identifier_width			17	// Specifies the width of the page identifier details from the CPU

`define  _process_id_size			8	// number of bits to identify the process_id

`define  _logical_partition_id_size		6	// Number of bits to identify the logical partition id

`define  _address_space_instruction		0	// Specifies the value for the request to be for Instruction

`define  _address_space_data			1	// Specifies the value for the request to be for Data


///////////////////////////////////////    L2 TLB	///////////////////////////////////////////////////////////

`define  _l2tlb_total_entries			512	// Defines the depth of the TLB

`define  _l2tlb_associativity			4	// Number of ways per set

`define  _l2tlb_page_index			[4:0]	// Bits in the virtual page number used to index TLB

`define  _l2tlb_frame_details_width		39	// The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc

`define  _l2tlb_access_ctrl_size		4	// Specifies the width of access control bits

`define  _l2tlb_user_read			[0]	// Specifies the user read bit

`define  _l2tlb_supervisor_read			[1]	// Specifies the supervisor read bit

`define  _l2tlb_user_write			[2]	// Specifies the user write bit

`define  _l2tlb_supervisor_write		[3]	// Specifies the supervisor write bit


///////////////////////////////////////    Instruction L1 TLB	//////////////////////////////////////////////////

`define  _l1itlb_total_entries			32	// Defines the depth of the TLB

`define  _l1itlb_associativity			4	// Number of ways per set

`define  _l1itlb_page_index			[4:0]	// Bits in the virtual page number used to index TLB

`define  _l1itlb_page_index_size		5	// Size of the virtual page number used to index TLB

`define  _l1itlb_frame_details			37	// The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc

`define  _l1itlb_access_ctrl_size		2	// Specifies the width of access control bits

`define  _l1itlb_user_read			[0]	// Specifies the user read bit

`define  _l1itlb_supervisor_read		[1]	// Specifies the supervisor read bit

`define  _l1itlb_access_ctrl_index		[3:2]

`define  _l1itlb_miss_buffer_size		2	// Number of miss to allow before the 1st is served (Hit under miss / Miss under miss)

///////////////////////////////////////    VPS TLB			//////////////////////////////////////////////////

`define  _l1itlb_vps_total_entries		4	// Defines the depth of the Variable Page Size ITLB

`define  _l1dtlb_vps_total_entries		4	// Defines the depth of the Variable Page Size DTLB

`define  _l2tlb_vps_total_entries		4	// Defines the depth of the Variable Page Size L2 TLB

///////////////////////////////////////    Data L1 TLB	     		//////////////////////////////////////////////////

`define  _l1dtlb_total_entries			32	// Defines the depth of the TLB

`define  _l1dtlb_associativity			4	// Number of ways per set

`define  _l1dtlb_page_index_size		5	// Size of the virtual page number used to index TLB

`define  _l1dtlb_page_index			[4:0]	// Bits in the virtual page number used to index TLB

`define  _l1dtlb_frame_details			39	// The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc

`define  _l1dtlb_access_ctrl_size		4	// Specifies the width of access control bits

`define  _l1dtlb_user_read			[2]	// Specifies the user read bit

`define  _l1dtlb_supervisor_read		[3]	// Specifies the supervisor read bit

`define  _l1dtlb_user_write			[0]	// Specifies the user write bit

`define  _l1dtlb_supervisor_write		[1]	// Specifies the supervisor write bit

`define  _l1dtlb_miss_buffer_size		1	// Number of miss to allow before the 1st is served (Hit under miss / Miss under miss)

///////////////////////////////////////    Page Table Walk 		//////////////////////////////////////////////////

`define  _pt_access_ctrl_size			4	// Specifies the width of access control bits

`define  _global_directory			[51:39] // Specifies the bits in virtual address which refers to Global directory

`define  _upper_directory			[38:26] // Specifies the bits in virtual address which refers to Upper directory

`define  _middle_directory			[25:13] // Specifies the bits in virtual address which refers to Middle directory

`define  _page_table				[12:0]	// Specifies the bits in virtual address which refers to Page Table

`define  _base					[39:15]	// Specifies the bits in base address which is used in translation from virtual address to machine address

`define  _base_hypervisor			[39:15]	// Specifies the bits in base address which is used in translation from guest physical address to machine address

`define  _page_table_hypervisor			[12:0]	// Specifies the bits in guest physical address which refers to Page Table

///////////////////////////////////////	  Address Related Configurations /////////////////////////

`define _virtual_address_width			64	// Specifies the width of the virtual address coming from the CPU

`define _physical_address_width		40	// Specifies the width of the physical address

///////////////////////////////////////   Data Related Configurations    /////////////////////////

`define _data_width				64	// Specifies the width of the data bus

`define _data_width_bus_error			65	// Specifies the width of the data bus with bus error

`define _bus_error				64	// Specifies Bit with bus error or not

`define _block_size				256	// Size of each block

///////////////////////////////////////   Counter Related Configations   //////////////////////////

`define _counter_width				32	// Specifies the width of counter registers

`define _counter_combined_width		64	// Specifies the width of two counter registers combined

`define _counter_LSBs				[31:0]	// Specifies the Lower 32 bits of the counter

`define _counter_MSBs				[63:32] // Specifies the higher 32 bits of the counter

////////////////////////////////////////   Other Configurations          //////////////////////////


`define _psw_width				32	// Specifies the width of PSW


`define RV64

`ifdef RV32
`define Multiplier32
`define Multiplier16
`define divider32
`endif

`ifdef RV64
`define Multiplier64
`define Multiplier32
`define Multiplier16
`define divider64
`endif

`define BTB_SIZE 16
`define BTB_BITS 4

`define FUN_UNITS 5
`define FPU_UNITS 4
`define ALU_UNITS 2
`define BRANCH_UNITS 4
`define MEM_UNITS 4

`define Ways 4
`define BLOCK_SIZE 2        // number of words in a block
`define Num_of_sets 512    // total number of sets
`define Num_of_tag_bits 19  // number of bits used to represent the tag.
`define Num_of_offset_bits 4 // number of bits used to represent the offset.
