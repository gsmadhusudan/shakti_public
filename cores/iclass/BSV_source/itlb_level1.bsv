/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Author Names : Rahul Bodduna, Debiprasanna Sahoo.
Email ID : rahul.bodduna@gmail.com


	Description : 
		It consist of n-way configurable entry TLB array. Each entry consist of virtual page number and corresponding physical
		frame number that can be used for address translation. It also consist of the page identifiers that includes
		*	logical partition identifier	:	Logical partition identifier of the guest operating system
		*	process identifier
		*	Guest state			:	States if hypervisor or Guest OS is running

		In order to provide protection to the given page, protection bits associated with the page	:
		*	supervisor read access
		*	User read access
	
		It also stores informations about the given page
		*	Inhibiting i.e. page is cacheable or not
		*	Cache coherancy support for the page is available for the page or not

	Replacement Policy	:

	It uses Tree based Pseudo LRU policy to replace given entry in a set.

	Working	:

	Step 1:	It receives the virtual page number, page identifier and read/write signal. Indexing is done using the lower order bits of the
		TLB. It matches the page identifiers and virtual page number of all the ways in the given entry in parallel. In the same cycle
		when a given way matches the access rights are checked against the store bits. If access is valid the physical page number and
		information stored about the page are forwarded to the MMU Controller otherwise interrupt is generated. In case of a miss the 
		request is buffered in a queue.The MMU controller, continously checks the queue and forwards the request to the next level.
		The Buffering is done in order to make the TLB free to support Hit under a miss or even miss under the miss

	Step 2: In case of miss when the MMU controller forwards the response from the page table, the array is updated with the incoming details
		and the Pseudo LRU policy is also updated.
*/
package itlb_level1;

import ConfigReg::*;
import DReg::*;
import FIFOF::*;
import SpecialFIFOs::*;

import riscv_types::*;
import replacement_policy::*;

`include "defined_parameters.bsv"
`define PseudoLRUDefault PseudoLRU_type{_ab_cd_select:0,_ab_select:0,_cd_select:0}

/****************************************************** Functions ************************************************************/

function Integer getHitWay(Bool isWayHit[], Integer no_of_ways);
	Integer way = no_of_ways;
	for(Integer count = 0; count < no_of_ways; count = count + 1)
		if(isWayHit[count])
			way	=	count;
	return way;
endfunction : getHitWay


/*
	Specifies the Input and output interfaces to the ITLB
*/
interface Ifc_itlb_level1;
	/* Input Methods */
	/* input port from the cpu: Virtual page,Page Identifier */
	method Action _virtual_page_no_from_cpu(Maybe#(Virtual_Page_No_Type) _virtual_page_no,Page_Identifier _page_identifier);
	/* The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc */
	method Action _physical_frame_from_l2_tlb(Maybe#(IFrame_details_type) _physical_frame);
	/* Flush the TLB */
	method Action _flush();

	/* Output Methods */
	/* The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc */
	method Maybe#(Frame_Output_Type) _physical_frame_to_l1_icache();
	/* Till the FIFO that stores the virtual address that needs to be forwarded to the page table has free space,then it enables MMU to take new request from the CPU.*/	
	method Bool _is_enable_get_page_from_mmu();
	/* If the FIFO that stores the virtual address that needs to be forwarded to the page table contains an item,then it requests the MMU to send request to the L2 TLB.*/	
	method Bool _is_enable_get_frame_from_l2_tlb();
	/* Gets if the interrupt is generated or not */
	method bit _interrupt();
	/* Forward the translation request to the L2 TLB */
	method Maybe#(Translation_Request) _get_Translation_Request_for_l2_TLB();

	/* Counts the number of hits and misses in ITLB */
	method Combined_Counter_Type _hit_miss_count();	
	method Counter_Type _access_count();
endinterface : Ifc_itlb_level1

(*synthesize*)
module mkitlb_level1(Ifc_itlb_level1);
	// Input Components
	Wire#(Maybe#(Virtual_Page_No_Type))	      wr_virtual_page_no_frm_cpu	<-	mkWire();	// Wire that contains the virtual page from the CPU
	Wire#(Page_Identifier)  	     		      wr_page_identifier	<-	mkWire();	// get the page identifier like process id, partition id etc

	Wire#(Maybe#(IFrame_details_type)) 		 wr_l1itlb_frame_details	<-	mkDWire(tagged Invalid);	// get the frame from the MMU that comes from L2 TLB

	Reg#(Maybe#(Translation_Request))	    rg_virtual_page_no_to_l2_tlb	<-	mkReg(tagged Invalid);

	Reg#(Maybe#(Virtual_Page_No_Type))	      rg_virtual_page_no_frm_cpu	<-	mkReg(tagged Invalid);	// Register that contains the virtual page from the CPU
	Reg#(Page_Identifier)  	     		      	      rg_page_identifier	<-	mkReg(Page_Identifier{
								_user_supervisor	:	0,
								    _guest_state	:	0,
							   _logical_partition_id	:	0,
								  _address_space	:	0,
							     _process_id	:	0});	// get the page identifier like process id, partition id etc

	Wire#(Bool) 							wr_flush	<-	mkDWire(False);	// the flush signal

	// Output Components
	Wire#(Maybe#(Frame_Output_Type)) 		wr_frame_no_to_l1_icache	<-	mkDWire(tagged Invalid);// Stores the frame details to be sent to the L1 ICache

	// Internal Components
	Reg#(Itlb_level1_type) 		       tlb_table[`_l1itlb_total_entries][`_l1itlb_associativity];	// TLB Table
	Reg#(PseudoLRU_type)	            	    plru[`_l1itlb_total_entries];				// PSeudo LRU Policy	

	ConfigReg#(Counter_Type)					rg_clock	<-	mkConfigReg(0);	// Clock counter
	Reg#(State)							rg_state	<-	mkReg(Handle_Request);// Stores the state of the TLB

	Reg#(Bool)						       rg_enable	<-	mkReg(True);		// Enable the TLB to take new request

	// Counters to count the number of hits and misses in the TLB
	Reg#(Counter_Type)					 rg_access_count	<-	mkReg(0);	// Access counter
	Reg#(Counter_Type)					    rg_hit_count	<-	mkReg(0);	// Hit counter
	Reg#(Counter_Type)					   rg_miss_count	<-	mkReg(0);	// Miss counter
	
	Reg#(bit)						    rg_interrupt	<-	mkReg(0);	// Interrupt notifier	
	Reg#(Way_Type)						   rg_victim_way	<-	mkReg(0);	// Way to be replaced

	// Initialize the entries of the ITLB
	for (Integer entry = 0; entry < `_l1itlb_total_entries; entry = entry + 1)
	begin
		plru[entry] <- mkReg(PseudoLRU_type {_ab_cd_select:0,_ab_select:0,_cd_select:0});
		for (Integer wayCounter = 0; wayCounter < `_l1itlb_associativity; wayCounter = wayCounter + 1)
		begin
		      	tlb_table [entry][wayCounter] <- mkReg (Itlb_level1_type {
	_virtual_page_no  : 0,	_physical_frame_no	: tagged Invalid, _guest_state     : 0,_logical_partition_id: 0,_process_id	    : 0,
	_access_ctrl      : 0,  _write_back_through	: 0		,_write_allocate   : 0,_inhibiting	    : 0,_coherancy_required : 0});
		end
	end

/**************************************************            RULES         **************************************************/

	/*
		Tracks the clock
	*/
	rule rl_count_clock;
		rg_clock <= rg_clock + 1;
	endrule
	
	/*
		If the flush signal is set the TLB is flushed i.e. the content of TLB are erased and all registers are tagged as invalid
	 */
	rule rl_flush(wr_flush);
		wr_frame_no_to_l1_icache	<=	tagged Invalid;					// Invalidate the frame to L1 ICache
		rg_state			<=	Handle_Request;					// Change the state of TLB to Handle Request
		rg_interrupt			<=	0;						// Reset interrupt signal
		rg_virtual_page_no_frm_cpu	<=	tagged Invalid;					// Reset the virtual page number from CPU
		rg_virtual_page_no_to_l2_tlb	<=	tagged Invalid;					// Reset the page number to be sent to L2 TLB
		rg_enable			<=	True;
		rg_page_identifier		<=	Page_Identifier{
								_user_supervisor	:	0,
								    _guest_state	:	0,
							   _logical_partition_id	:	0,
								  _address_space	:	0,
								     _process_id	:	0};	// Reset the page identifier like process id, partition id etc

		for (Integer entry = 0; entry < `_l1itlb_total_entries; entry = entry + 1)		// Reset the TLB
		begin
			plru[entry] <= PseudoLRU_type{_ab_cd_select:0,_ab_select:0,_cd_select:0};
			for (Integer wayCounter = 0; wayCounter < `_l1itlb_associativity; wayCounter = wayCounter + 1)
			begin
			      	tlb_table [entry][wayCounter] <= Itlb_level1_type {
		_virtual_page_no  : 0,	_physical_frame_no	: tagged Invalid, _guest_state     : 0,_logical_partition_id: 0,_process_id	    : 0,
		_access_ctrl      : 0,  _write_back_through	: 0		,_write_allocate   : 0,_inhibiting	    : 0,_coherancy_required : 0};
			end
		end
	endrule

	/*
		This rule fires if the following conditions are satisfied	:
		*	Virtual page number is received from the CPU.
		*	State of the TLB is "Handle_Request".

		This rule performs the following actions	:
		*	Index into the TLB using the lower n-bits of the virtual page number where n = log(number of entries)
		*	Match virtual page numbers of all the ways stored in the TLB for the given set with the incoming virtual page number.
		*	Each entry is matched with the requested page identifier from the CPU	:
			->	The guest state stored in the TLB is matched with the guest state stored in the MMU register.
			->	If the request is made by any guest OS, the virtual partition identifier is matched with the LPID stored in the MMU register.
			->	If the request is made by hypervisor, virtual partition identifier match should not be done.
			->	The process identifier stored in the TLB is matched against the PID stored in the MMU register.
			->	Check if the valid bit is set.
		*	If all the above matching gives a match, the way is set to hit	:
			->	Set the way as a hit.
			->	If the request is made by the supervisor and the supervisor read bit is set, or
			->	If the request is made by the user and the user read bit is set, then
			->	Read the frame details from the TLB and reset interrupt signal.
			->	In case, there is an invalid accces, then interrupt is generated.
		*	Get the entry that had a hit and perform the following action	:
			->	Reset virtual address from cpu, enable and state registers.
			->	Send the interrupt signal to the CPU and physical frame to the instruction cache.
			->	Update the replacement policy.
			->	Increment the hit counter.
		*	In case no entry matches, i.e. it is a miss
			->	Store the virtual address, page information and page size received from the CPU.
			->	Change the state to "Handle_Response".
			->	Reset interrupt signal.
			->	Set the enable register to "False" i.e. it won't take any more request.
	 */
	rule rl_handle_request(wr_virtual_page_no_frm_cpu matches tagged Valid ._virtual_page_no 
		&&& rg_state == Handle_Request && !wr_flush);

		rg_access_count		<=	rg_access_count + 1;

		rg_virtual_page_no_to_l2_tlb	<=	tagged Invalid;				// Invalidate signal from L2 TLB
		Bool isHitWay[`_l1itlb_associativity];
		Maybe#(Frame_Output_Type) frameOutput[`_l1itlb_associativity];		
		bit isInterrupt[`_l1itlb_associativity];
		$display("Clock = %d\tStep  4:\tITLB:\tState = Handle Request with Virtual page = %h",rg_clock,wr_virtual_page_no_frm_cpu);
		/* 
			If the guest state is set to 0 i.e guest operating system is running, then the logical partition identifier
			is matched against the virtual partition identifier.
		 */
		for(Integer count = 0; count < `_l1itlb_associativity; count = count + 1)
		begin
			isHitWay[count]			=	False;
			frameOutput[count]		=	tagged Invalid;
			// Index the TLB with lower bits of virtual page number
			Itlb_level1_type	_fetched_entry  =	tlb_table [_virtual_page_no`_l1itlb_page_index][count];
			// Perform page identification by matching guest state, LPID, PID and virtual page number
			if(_fetched_entry._guest_state == wr_page_identifier._guest_state
				&& ((_fetched_entry._guest_state == 0 && _fetched_entry._logical_partition_id == wr_page_identifier._logical_partition_id)	
				|| _fetched_entry._guest_state == 1)
				&& _fetched_entry._process_id == wr_page_identifier._process_id
				&& _fetched_entry._virtual_page_no == _virtual_page_no
				&&& _fetched_entry._physical_frame_no matches tagged Valid (._physical_frame_no)
			)
			begin
				isHitWay[count]		=	True;
				if(	(wr_page_identifier._user_supervisor == 1 				// Check the permissions on the page
					&& (_fetched_entry._access_ctrl`_l1itlb_supervisor_read == 1))
				     	|| (wr_page_identifier._user_supervisor == 0 
				     	&& (_fetched_entry._access_ctrl`_l1itlb_user_read == 1)))
				begin
					$display("Clock = %d\tStep  5:\tITLB:\tHIT from %d Virtual page = %h Physical Page Number = %h",rg_clock,count,_virtual_page_no,_fetched_entry._physical_frame_no);
					isInterrupt[count]	=	0;
					frameOutput[count]	=	tagged Valid Frame_Output_Type	{// send the frame details to L1 ICache
						    _physical_frame_no:fromMaybe(0,_fetched_entry._physical_frame_no),
						   _write_back_through:_fetched_entry._write_back_through,	     
						       _write_allocate:_fetched_entry._write_allocate,	     
							   _inhibiting:_fetched_entry._inhibiting,             
						   _coherancy_required:_fetched_entry._coherancy_required	};
				end
				else
				begin
					$display("Clock = %d\tStep  5:\tITLB:\tPermission denied from %d Virtual page = %h",rg_clock,count,_virtual_page_no); 
					isInterrupt[count]	=	1;				// set the interrupt bit
				end
			end
		end
		Integer hitWay = getHitWay(isHitWay,`_l1itlb_associativity);
		if(hitWay != `_l1itlb_associativity)
		begin
			rg_enable			<=	True;
			rg_state			<=	Handle_Request;				// Set the state of the TLB to handle request
			rg_virtual_page_no_frm_cpu	<=	tagged Invalid;				// Invalidate the virtual page from the CPU
			wr_frame_no_to_l1_icache	<=	frameOutput[hitWay];			// send the frame details to L1 ICache
			rg_interrupt			<=	isInterrupt[hitWay];
			// update the pseudo LRU policy
			plru[_virtual_page_no`_l1itlb_page_index]	<=	updatePseudoLRU(plru[_virtual_page_no`_l1itlb_page_index],hitWay);
			rg_hit_count			 <=	rg_hit_count + 1;		// Update the hit count
		end
		else
		begin	
			rg_enable			<=	False;
			rg_interrupt			<=	0;					// Reset the interrupt
			rg_state			<=	Handle_Response;			// Change the state to perform permission check
			rg_virtual_page_no_frm_cpu	<=	wr_virtual_page_no_frm_cpu;		// Store the virtual page from the CPU
			rg_page_identifier		<=	wr_page_identifier;			// Store the page identifier
		end
	endrule

	/*
		This rule fires if the following conditions are satisfied	:
		*	Register that stores the virtual page number from CPU is valid.
		*	State of the TLB is to "Handle_Response".

		This rule performs the following actions	:
		*	Invalidate virtual page number from the CPU.
		*	Change the state of the TLB to "Stall".
		*	Calculate the victim way that needs to be replaced.
		*	Increment the miss count.
		*	Send the virtual page number to L2 TLB.
	*/
	rule rl_miss(rg_virtual_page_no_frm_cpu matches tagged Valid ._virtual_page_no &&& !wr_flush && rg_state == Handle_Response);

		ReplacementInformation replacementInfo = victimWay(plru[_virtual_page_no`_l1itlb_page_index]);
		$display("Clock = %d\tStep  5:\tITLB:\tMiss Virtual page = %h Virtual Index = %h Victim Way : %d",rg_clock,_virtual_page_no,_virtual_page_no`_l1itlb_page_index,replacementInfo.way); 		      
		rg_virtual_page_no_frm_cpu	 <=	tagged Invalid;				// Register that contains the virtual page from the CPU
		wr_frame_no_to_l1_icache	 <=	tagged Invalid;				// Invalidate the signals to L1 ICache
		rg_miss_count			 <=	rg_miss_count + 1;			// Increment the miss count		
		rg_state			 <=	Stall;

		rg_virtual_page_no_to_l2_tlb	 <=	tagged Valid Translation_Request{	// send the request to L2 TLB
					_virtual_page_no	:	_virtual_page_no,
					_page_identifier	:	rg_page_identifier,
					     _readWrite		:	0,
					       _tlb_type	:	0};
		plru[_virtual_page_no`_l1itlb_page_index]	<=	replacementInfo.plru;
		rg_victim_way			<=	replacementInfo.way;			// get the way that needs to be replaced
	endrule

	/*
		This rule fires if the following conditions are satisfied	:
		*	State of the TLB is set to "Stall".
		*	Virtual address sent to the L2 TLB is valid.
		*	L2 TLB has responded with a physical frame.

		This rule performs the following actions	:
		*	Change the state of the TLB to "Handle_Request".
		*	Enable the TLB to accept new request.
		*	Send the frame details to L1 Instruction Cache.
		*	Update the TLB table with given index and the way that needs to be replaced with the frame details from L2 TLB.
	*/
	rule rl_handle_miss(!wr_flush && rg_state == Stall &&& rg_virtual_page_no_to_l2_tlb matches tagged Valid ._l2_tlb_request
			&&& wr_l1itlb_frame_details matches tagged Valid ._physical_frame);

		$display("Clock = %d\tStep 14:\tITLB:\tVirtual page = %h TLB Physical Frame = %h",rg_clock,_l2_tlb_request._virtual_page_no,_physical_frame._physical_frame_no);
		rg_virtual_page_no_to_l2_tlb	<=	tagged Invalid;					// Invalid signal to L2 TLB
		rg_state			<=	Handle_Request;					// Reset state of the TLB
		rg_enable			<=	True;
		wr_frame_no_to_l1_icache	<=	tagged Valid Frame_Output_Type	{		// Send the miss result i.e the frame number
				    _physical_frame_no:_physical_frame._physical_frame_no,		// to the ICache
				   _write_back_through:_physical_frame._write_back_through,	     
				       _write_allocate:_physical_frame._write_allocate,	     
					   _inhibiting:_physical_frame._inhibiting,             
			           _coherancy_required:_physical_frame._coherancy_required	};
		tlb_table[_l2_tlb_request._virtual_page_no`_l1itlb_page_index][rg_victim_way]	<=	Itlb_level1_type	{// Update the TLB array
				_guest_state:_physical_frame._guest_state,
		       _logical_partition_id:_physical_frame._logical_partition_id,
			 	 _process_id:_physical_frame._process_id,
			  _physical_frame_no:tagged Valid _physical_frame._physical_frame_no,
			    _virtual_page_no:_l2_tlb_request._virtual_page_no,
				_access_ctrl:_physical_frame._l1itlb_access_ctrl,
			 _write_back_through:_physical_frame._write_back_through,	     
			     _write_allocate:_physical_frame._write_allocate,	     
				 _inhibiting:_physical_frame._inhibiting,             
		         _coherancy_required:_physical_frame._coherancy_required	};
	endrule	

/******************************************** Methods ************************************************************/

	/*
		Get the Virtual page from the CPU along with the page identifiers
	*/
	method Action _virtual_page_no_from_cpu(Maybe#(Virtual_Page_No_Type) _virtual_page_no,Page_Identifier _page_identifier);
		wr_virtual_page_no_frm_cpu 	 <= 	_virtual_page_no;	// Get the virtual page from CPU
		wr_page_identifier	 	 <=	_page_identifier;	// Get the page identifier from the CPU
	endmethod
	
	/*
		Get the physical frame details from the L2 TLB
	 */
	method Action _physical_frame_from_l2_tlb(Maybe#(IFrame_details_type) _physical_frame);
		wr_l1itlb_frame_details	 	<= 	_physical_frame;	// Get the frame details from the page table
	endmethod

	/* 
		Flush the TLB
	*/
	method Action _flush();
		wr_flush 			<=  	True;			// Reset the Flush Mode
	endmethod

	/* 
		The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc
	 */	
	method Maybe#(Frame_Output_Type) _physical_frame_to_l1_icache();
		return wr_frame_no_to_l1_icache;
	endmethod

	/*
		Till the FIFO that stores the virtual address that needs to be forwarded to the page table has free space,
		then it enables MMU to take new request from the CPU.
	 */
	method Bool _is_enable_get_page_from_mmu();
		return rg_enable;
	endmethod

	/*
		Forward the translation request to the L2 TLB
	 */
	method Maybe#(Translation_Request) _get_Translation_Request_for_l2_TLB();
		return rg_virtual_page_no_to_l2_tlb;
	endmethod

	/*
		If the FIFO that stores the virtual address that needs to be forwarded to the page table contains an item,
		then it requests the MMU to send request to the L2 TLB.
	 */
	method Bool _is_enable_get_frame_from_l2_tlb();
		return rg_state == Stall;
	endmethod
	
	/*	
		Gets if the interrupt is generated or not
	*/
	method bit _interrupt();
		return rg_interrupt;
	endmethod

	/*
		Counts the number of hits and misses in L1 ITLB
	*/
	method Combined_Counter_Type _hit_miss_count();
		return {rg_miss_count,rg_hit_count};				// Send the number of hits and misses
	endmethod

	method Counter_Type _access_count();
		return rg_access_count;						// Send the number of hits and misses
	endmethod
endmodule
endpackage
