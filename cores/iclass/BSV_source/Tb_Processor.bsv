package  Tb_Processor;

// BLUESPEC IMPORTS
import RegFile			:: *; 
import ConfigReg		:: *;
import DReg				:: *;
import Connectable		:: *;
import FIFOF			:: *;
import SpecialFIFOs		:: *;
import DefaultValue		:: *;

// PROJECT IMPORTS
import TLM2 			:: *;	// Using local copy
import Axi				:: *;	// Using local copy
import Utils			:: *;
import Processor		:: *;
import riscv_types		:: *;
import TLM_ReqRsp		:: *;
import All_types		:: *;
import All_types_d		:: *;
import Vector			:: *;

`include "defined_parameters.bsv"
`include "TLM.defines"
`include "Axi.defines"

`include "TLM2_Iclass.defines"
`include "Axi_Iclass.defines"

//(*preempts="rl_dcache_to_memory,rl_read_write_back_data"*)
//(*preempts="rl_dcache_to_memory,rl_update_mem_with_write_back"*)

module mkTb_Processor();

// Misc. registers
Reg #(int) rg_counter <- mkReg(0); 

rule rl_counter_terminate;
	if(rg_counter == 15) $finish;
	else rg_counter <= rg_counter + 1;
endrule

Ifc_cache_to_memory processor <- mkProcessor();
//Memory_I_IFC mem_i <- mk_Memory_I ("Instruction_Memory");
//Memory_D_IFC mem_d <- mk_Memory_D ("Data_Memory");

   function Bool addrMatch0(AxiAddr#(`TLM_TYPES) a);
      return (a[63] == 0);
   endfunction
   
   function Bool addrMatch1(AxiAddr#(`TLM_TYPES) a);
      return (a[63] == 1);
   endfunction

   // Read Master Transactor
   AxiRdMasterXActorIFC#(`AXI_XTR_MEM_I) rd_master_i <- mkAxiRdMaster;
   AxiRdMasterXActorIFC#(`AXI_XTR_MEM_D) rd_master_d <- mkAxiRdMaster;

   // Write Master Transactor
   AxiWrMasterXActorIFC#(`AXI_XTR_MEM_D) wr_master_d <- mkAxiWrMaster;
   
   // Read Slave Transactor
   AxiRdSlaveXActorIFC#(`AXI_XTR_MEM_I)  rd_slave_i <- mkAxiRdSlave(1, addrMatch0);
   AxiRdSlaveXActorIFC#(`AXI_XTR_MEM_D)  rd_slave_d <- mkAxiRdSlave(1, addrMatch1);
   
  // Write Slave Transactor
   AxiWrSlaveXActorIFC#(`AXI_XTR_MEM_D)  wr_slave_d <- mkAxiWrSlave(1, addrMatch1);
   
	Memory_I_IFC mem_i <- mk_Memory_I ("Instruction_Memory");
	Memory_D_IFC mem_d <- mk_Memory_D ("Data_Memory");

   // Connections between Read masters and transactor's tlm interface
   mkConnection(processor.mem_i_rd_ifc, rd_master_i.tlm);
   mkConnection(processor.mem_d_rd_ifc, rd_master_d.tlm);

   // Connections between Write masters and transactor's tlm interface
   mkConnection(processor.mem_d_wr_ifc, wr_master_d.tlm);

   // Instantiating read bus fabric
   Vector #(1, AxiRdFabricMaster#(`AXI_PRM_MEM_I)) rd_master_fabric_i = newVector;
   Vector #(1, AxiRdFabricMaster#(`AXI_PRM_MEM_D)) rd_master_fabric_d = newVector;
   Vector #(1, AxiRdFabricSlave#(`AXI_PRM_MEM_I))  rd_slave_fabric_i  = newVector;
   Vector #(1, AxiRdFabricSlave#(`AXI_PRM_MEM_D))  rd_slave_fabric_d  = newVector;
   
   // Instantiating write bus fabric
   Vector #(1, AxiWrFabricMaster#(`AXI_PRM_MEM_D)) wr_master_fabric_d = newVector;
   Vector #(1, AxiWrFabricSlave#(`AXI_PRM_MEM_D))  wr_slave_fabric_d  = newVector;

   // Linking read sources with bus fabric through read bus transactors
   rd_master_fabric_i[0] = rd_master_i.fabric;
   rd_master_fabric_d[0] = rd_master_d.fabric;

   // Linking read targets with bus fabric through read bus transactors
   rd_slave_fabric_i[0] = rd_slave_i.fabric;
   rd_slave_fabric_d[0] = rd_slave_d.fabric;

   // Linking write sources with bus fabric through write bus transactors
   wr_master_fabric_d[0] = wr_master_d.fabric;
   
   // Linking write targets with bus fabric through write bus transactors
   wr_slave_fabric_d[0] = wr_slave_d.fabric;
   
   // Instantiating read bus (channel)
   mkAxiRdBus(rd_master_fabric_i, rd_slave_fabric_i);
   mkAxiRdBus(rd_master_fabric_d, rd_slave_fabric_d);

   // Instantiating write bus (channel)
   mkAxiWrBus(wr_master_fabric_d, wr_slave_fabric_d);
   
   // Connections between read slaves and transactor's tlm interface
   mkConnection(rd_slave_i.tlm, mem_i.bus_rd_ifc);
   mkConnection(rd_slave_d.tlm, mem_d.bus_rd_ifc);

   // Connections between write slaves and transactor's tlm interface
   mkConnection(wr_slave_d.tlm, mem_d.bus_wr_ifc);
//	mkConnection (processor.mem_i_rd_ifc, mem_i.bus_rd_ifc);
//	mkConnection (processor.mem_d_rd_ifc, mem_d.bus_rd_ifc);
//	mkConnection (processor.mem_d_wr_ifc, mem_d.bus_wr_ifc);

endmodule

/*

module mkTb_Processor();

Ifc_cache_to_memory processor <- mkProcessor();
RegFile#(UInt#(`ICACHE_SIZE),Bit#(`INSTR_WIDTH)) instr_memory <- mkRegFileLoad(
	  "input.hex",0,`NUM_INSTRS);
RegFile#(UInt#(`DCACHE_SIZE), Bit#(`REG_WIDTH)) data_memory <- mkRegFileLoad(
	  "rtl_mem_init.txt",0,`MEM_INIT_SIZE-1);
Reg#(Bit#(128))  rg_block <- mkReg(0); 
Reg#(Bit#(256))  rg_dblock <- mkReg(0); 
Reg#(Bit#(`ADDRESS_WIDTH)) rg_in_address <- mkReg(0);
Reg#(bit)				   rg_cnt		 <- mkDReg(0);
Reg#(Bit#(`ADDRESS_WIDTH)) rg_in_daddress <- mkReg(0);
Reg#(bit)				   rg_cnt_d		 <- mkDReg(0);
Reg#(Bit#(5)) rg_token<-mkReg(0);
Reg#(Bit#(3)) rg_write_back_loop <-mkReg(0);
Reg#(Bit#(256)) rg_write_back_data <-mkReg(0);
Reg#(Bit#(64)) rg_write_back_address <-mkReg(0);

rule rl_icache_to_memory(rg_cnt == 0 && !processor.flush_icache);
	    let x <- processor.request_to_memory_from_icache; 
        Bit#(128) lv_block = 0;
	    Bit#(`ADDRESS_WIDTH) icache_address = 0;
	    rg_in_address <= x.address;
		rg_token<=x.token;
	    icache_address = x.address >> 2;
	    $display("icache_request_to_memory data_block requested original address :%d with_shifted_address %d token :%d",x.address, icache_address,x.token);
	   	lv_block[31:0] = instr_memory.sub(unpack(icache_address));
	   	lv_block[63:32] = instr_memory.sub(unpack(icache_address+1));
	   	lv_block[95:64] = instr_memory.sub(unpack(icache_address+2));
	   	lv_block[127:96] = instr_memory.sub(unpack(icache_address+3));
	    rg_block <= lv_block;
		rg_cnt <= rg_cnt + 1;
endrule

rule rl_memory_to_icache(rg_cnt == 1 && !processor.flush_dcache);
		
	    $display("response_memory data_block obtained with_data %h Address :%h token : %d", rg_block,rg_in_address,rg_token);
	    processor.response_from_memory_to_icache(From_Memory{bus_error : 0, data_line : rg_block, address : rg_in_address, token:rg_token});
endrule

rule rl_dcache_to_memory(rg_cnt_d == 0 && !processor.flush_dcache);
	    let x <- processor.request_to_memory_from_dcache; 
	   Bit#(256) lv_block = 0;
	   Bit#(`ADDRESS_WIDTH) dcache_address = 0;
	    rg_in_daddress <= x.address;
	    dcache_address = x.address >> 3;
		if(x.ld_st == Load) begin
			if(dcache_address <= 'd1048576 && dcache_address >= 'd0) begin
	    		$display("dcache_request_to_memory data_block requested original address :%h with_shifted_address %h unpack_version %h",x.address, dcache_address, unpack(dcache_address));
	   			lv_block[63:0] = data_memory.sub(unpack(dcache_address));
	   			lv_block[127:64] = data_memory.sub(unpack(dcache_address+1));
	   			lv_block[191:128] = data_memory.sub(unpack(dcache_address+2));
	   			lv_block[255:192] = data_memory.sub(unpack(dcache_address+3));
			end
	    	rg_dblock <= lv_block;
		end
		else
		data_memory.upd(unpack(dcache_address), x.write_data);
		rg_cnt_d <= rg_cnt_d + 1;
endrule


rule rl_memory_to_dcache(rg_cnt_d == 1 && !processor.flush_dcache);
		
	    $display("response_memory_dcache data_block obtained with_data %h Address :%h", rg_dblock,rg_in_daddress);
	    processor.response_from_memory_to_dcache(From_Memory_d{bus_error : 0, data_line : rg_dblock, address : rg_in_daddress});
endrule

rule rl_read_write_back_data(rg_write_back_loop==0);
	let x <- processor.write_back_data;
	rg_write_back_loop<=1;
	rg_write_back_data<=x.data_line;
	rg_write_back_address<=(x.address)>>3;
	$display("Write Back Data reached memory and updated");
endrule

rule rl_update_mem_with_write_back(rg_write_back_loop!=0);
	data_memory.upd(unpack(rg_write_back_address),rg_write_back_data[63:0]);
	rg_write_back_data<=rg_write_back_data>>64;
	rg_write_back_address<=rg_write_back_address+1;
	if(rg_write_back_loop==4)
		rg_write_back_loop<=0;
	else 
		rg_write_back_loop<=rg_write_back_loop+1;
	$display("TB: Writing into memory for address: %h with data :%h:",rg_write_back_address,rg_write_back_data[63:0],$time);
endrule

endmodule
*/

interface Memory_I_IFC;
	interface TLMRecvIFC #(Req_Mem_I, Rsp_Mem_I) bus_rd_ifc;
endinterface

module mk_Memory_I #(String name) (Memory_I_IFC);

FIFOF #(Req_Mem_I) ff_rd_reqs <- mkFIFOF;
FIFOF #(Rsp_Mem_I) ff_rd_rsps <- mkFIFOF;

RegFile#(UInt#(`ICACHE_SIZE), Bit#(`INSTR_WIDTH)) memory_i <- mkRegFileLoad ("input.hex",0,`NUM_INSTRS);

rule rl_process_requests;
	
	Req_Mem_I request = ff_rd_reqs.first; ff_rd_reqs.deq;
    Bit#(`ADDRESS_WIDTH) icache_address = 0;
	Rsp_Mem_I rsp = defaultValue;

	if (request matches tagged Descriptor .r) begin
    	icache_address = r.addr >> 2;
		rsp.transaction_id = r.transaction_id;
		rsp.command = r.command;

		if (r.command == READ) begin
			rsp.data = memory_i.sub(unpack(icache_address));
			rsp.status = SUCCESS;
			$display($time, " %s: READ: Slave responding", name);
		end
		else if (r.command == WRITE) begin
			memory_i.upd(unpack(icache_address), r.data);
			rsp.status = SUCCESS;
			$display($time, " %s: WRITE: Slave Descriptor responding", name);
		end
		else begin
			rsp.status = ERROR;
		end
	end
	ff_rd_rsps.enq (rsp);

endrule

interface bus_rd_ifc = toRecvIFC (ff_rd_reqs, ff_rd_rsps);

endmodule

interface Memory_D_IFC;
	interface TLMRecvIFC #(Req_Mem_D, Rsp_Mem_D) bus_rd_ifc;
	interface TLMRecvIFC #(Req_Mem_D, Rsp_Mem_D) bus_wr_ifc;
endinterface

module mk_Memory_D #(String name) (Memory_D_IFC);

FIFOF #(Req_Mem_D) ff_rd_reqs <- mkBypassFIFOF;
FIFOF #(Rsp_Mem_D) ff_rd_rsps <- mkBypassFIFOF;

FIFOF #(Req_Mem_D) ff_wr_reqs <- mkBypassFIFOF;
FIFOF #(Rsp_Mem_D) ff_wr_rsps <- mkBypassFIFOF;

RegFile#(UInt#(`DCACHE_SIZE), Bit#(`REG_WIDTH)) memory_d <- mkRegFileLoad("rtl_mem_init.txt",0,`MEM_INIT_SIZE-1);

rule rl_process_read_requests;
	
	Req_Mem_D request = ff_rd_reqs.first; ff_rd_reqs.deq;
    Bit#(`ADDRESS_WIDTH) dcache_address = 0;
	Rsp_Mem_D rsp = defaultValue;
	if (request matches tagged Descriptor .r) begin
    	dcache_address = r.addr >> 3;
$display("TB: Memody_D Process READ requests - Addr: %h", dcache_address);
		rsp.transaction_id = r.transaction_id;
		rsp.command = r.command;

		if (r.command == READ) begin
			rsp.data = memory_d.sub(unpack(dcache_address));
			rsp.status = SUCCESS;
			$display($time, " %s: READ: Slave responding", name);
		end
		else begin
			rsp.status = ERROR;
		end
	end
	ff_rd_rsps.enq (rsp);

endrule

rule rl_process_write_requests;
		
	Req_Mem_D request = ff_rd_reqs.first; ff_rd_reqs.deq;
    Bit#(`ADDRESS_WIDTH) dcache_address = 0;
	Rsp_Mem_D rsp = defaultValue;

	if (request matches tagged Descriptor .r) begin
    	dcache_address = r.addr >> 3;
		rsp.transaction_id = r.transaction_id;
	 	rsp.command = r.command;
$display("TB: Memody_D Process requests - Addr: %h", dcache_address);

		if (r.command == WRITE) begin
			memory_d.upd(unpack(dcache_address), r.data);
			rsp.status = SUCCESS;
			$display($time, " %s: WRITE: Slave Descriptor responding", name);
		end
		else begin
			rsp.status = ERROR;
		end
	end
	ff_wr_rsps.enq (rsp);

endrule

interface bus_rd_ifc = toRecvIFC (ff_rd_reqs, ff_rd_rsps);
interface bus_wr_ifc = toRecvIFC (ff_wr_reqs, ff_wr_rsps);

endmodule
endpackage 
