package TLM_ReqRsp;

import TLM2	:: *;
`include "TLM2_Iclass.defines"
// TLM2 Request Response types for SHAKTI I-Class Processor

// Request Respose types for Instruction Memory
typedef TLMRequest #(`TLM_PRM_MEM_I_REQ) 			Req_Mem_I;
typedef RequestDescriptor #(`TLM_PRM_MEM_I_REQ) Req_Desc_Mem_I;
typedef RequestData #(`TLM_PRM_MEM_I_REQ) 	   Req_Data_Mem_I;

typedef TLMResponse #(`TLM_PRM_MEM_I_RSP) 		Rsp_Mem_I;

// Request Respose types for Data Memory
typedef TLMRequest #(`TLM_PRM_MEM_D_REQ) 			Req_Mem_D;
typedef RequestDescriptor #(`TLM_PRM_MEM_D_REQ) Req_Desc_Mem_D;
typedef RequestData #(`TLM_PRM_MEM_D_REQ) 	   Req_Data_Mem_D;

typedef TLMResponse #(`TLM_PRM_MEM_D_RSP) 		Rsp_Mem_D;

endpackage
