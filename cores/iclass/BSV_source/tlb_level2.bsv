/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Author Names : Rahul Bodduna, Debiprasanna Sahoo
Email ID : rahul.bodduna@gmail.com


	Description : 
		It consist of n-way configurable entry TLB array. Each entry consist of virtual page number and corresponding physical
		frame number that can be used for address translation. It also consist of the page identifiers that includes
		*	logical partition identifier	:	Logical partition identifier of the guest operating system
		*	process identifier
		*	Guest state			:	States if hypervisor or Guest OS is running

		In order to provide protection to the given page, protection bits associated with the page	:
		*	supervisor read access
		*	supervisor write access	
		*	User read access
		*	User write access
	
		It also stores informations about the given page
		*	Write through / Write back
		*	Write allocate / no-allocate
		*	Inhibiting i.e. page is cacheable or not
		*	Cache coherancy support for the page is available for the page or not

	Replacement Policy	:

	It uses Tree based Pseudo LRU policy to replace given entry in a set.

	Working	:

	Step 1:	It receives the virtual page number, page identifier and read/write signal. Indexing is done using the lower order bits of the
		TLB. It matches the page identifiers and virtual page number of all the ways in the given entry in parallel. In the same cycle
		when a given way matches the access rights are checked against the store bits. If access is valid the physical page number and
		information stored about the page are forwarded to the MMU Controller otherwise interrupt is generated. In case of a miss the 
		request is automatically forwarded to the page table by the MMU because the MMU doesnot get a reply with valid physical page 
		number but the TLB is stalled till the request is not served, in other words it doesn't take any more request.

	Step 2: In case of miss when the MMU controller forwards the response from the page table, the array is updated with the incoming details
		and the Pseudo LRU policy is also updated.

*/
package tlb_level2;

import ConfigReg::*;
import DReg::*;

import riscv_types::*;
import replacement_policy::*;

`include "defined_parameters.bsv"
/****************************************************** Functions ************************************************************/

function Integer getHitWay(Bool isWayHit[], Integer no_of_ways);
	Integer way = no_of_ways;
	for(Integer count = 0; count < no_of_ways; count = count + 1)
		if(isWayHit[count])
			way	=	count;
	return way;
endfunction : getHitWay


/*
	Specifies the Input and output interfaces to the L2 TLB
*/
interface Ifc_tlb_level2;
	/* Input Methods */
	/* input port from the cpu: Virtual page,Page Identifier */
	method Action _virtual_page_no_from_cpu(Maybe#(Virtual_Page_No_Type) _virtual_page_no,Page_Identifier _page_identifier, bit _readWrite);
	/* The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc */
	method Action _physical_frame_from_page_table(Maybe#(Frame_details_type) _frame_details);
	/* Flush the TLB */
	method Action _flush();

	method Action _response_forwarded_to_l1_tlb();

	/* Output Methods */
	/* The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc */
	method Maybe#(Frame_details_type) _physical_frame_details_to_l1_tlb_cache();
	/* Notifies the MMU to get address from page table */	
	method Bool _is_enable_get_frame_from_page_table();

	method Bool _is_enable_get_next_request();
	
	/* Gets if the interrupt is generated or not */
	method bit _interrupt();

	/* Counts the number of hits and misses in L2 TLB */
	method Combined_Counter_Type _hit_miss_count();	

	method Counter_Type _access_count();
endinterface : Ifc_tlb_level2

(*synthesize*)
module mktlb_level2(Ifc_tlb_level2);
	// Input Components
	Reg#(Maybe#(Virtual_Page_No_Type))	   rg_virtual_page_no_frm_l1_tlb	<-	mkReg(tagged Invalid);	// Wire that contains the virtual page from the CPU
	Reg#(Page_Identifier) 	     		   rg_page_identifier_frm_l1_tlb	<-	mkReg(Page_Identifier{
								_user_supervisor	:	0,
								    _guest_state	:	0,
							   _logical_partition_id	:	0,
								     _process_id	:	0,
								  _address_space	:	0});	// get the page identifier like process id, partition id etc
	Reg#(bit)				         rg_readWrite_frm_l1_tlb	<-	mkReg(0);	// Get the read / write signal from CPU

	Wire#(Maybe#(Frame_details_type)) 		  wr_l2tlb_frame_details	<-	mkDWire(tagged Invalid);// get the frame from the MMU that comes from page table

	Reg#(Maybe#(Virtual_Page_No_Type))	      rg_virtual_page_no_frm_cpu	<-	mkReg(tagged Invalid);	// Register that contains the virtual page from the CPU
	Reg#(Page_Identifier)  	     		      	      rg_page_identifier	<-	mkReg(Page_Identifier{
								_user_supervisor	:	0,
								    _guest_state	:	0,
							   _logical_partition_id	:	0,
								     _process_id	:	0,
								  _address_space	:	0});	// get the page identifier like process id, partition id etc
	Reg#(bit)						    rg_readWrite	<-	mkReg(0);	// Store the read / write signal from CPU

	Wire#(Bool) 							wr_flush	<-	mkDWire(False);	// the flush signal

	// Output Components
	ConfigReg#(Maybe#(Frame_details_type))	rg_frame_details_to_l1_tlb_cache	<-	mkConfigReg(tagged Invalid);// Stores the frame details to be sent to the L1 TLB

	// Internal Components
	Reg#(Tlb_level2_type) 		        tlb_table[`_l2tlb_total_entries][`_l2tlb_associativity];	// TLB Table
	Reg#(PseudoLRU_type)	            	     plru[`_l2tlb_total_entries];				// PSeudo LRU Policy	

	ConfigReg#(Counter_Type)					rg_clock	<-	mkConfigReg(0);	// Clock counter
	ConfigReg#(State)						rg_state	<-	mkConfigReg(Handle_Request); 		// Stores the state of the L2 TLB
	Reg#(bit)					   	    rg_requester	<-	mkReg(0);	// Stores which L1 TLB ITLB or DTLB made the request

	Reg#(Bool)						       rg_enable	<-	mkReg(True);
	// Counters to count the number of hits and misses in the TLB
	Reg#(Counter_Type)					 rg_access_count	<-	mkReg(0);	// Access counter
	Reg#(Counter_Type)					    rg_hit_count	<-	mkReg(0);	// Hit counter
	Reg#(Counter_Type)					   rg_miss_count	<-	mkReg(0);	// Miss counter
	
	Reg#(bit)						    rg_interrupt	<-	mkReg(0);	// Interrupt notifier	
	Reg#(Way_Type)						   rg_victim_way	<-	mkReg(0);	// Way to be replaced

	Reg#(Maybe#(Virtual_Page_No_Type))	rg_virtual_page_no_to_page_table	<-	mkReg(tagged Invalid);	// Stores the virtual address from CPU

	// Initialize the entries of the ITLB
	for (Integer entry = 0; entry < `_l2tlb_total_entries; entry = entry + 1)
	begin
		plru[entry] <- mkReg(PseudoLRU_type {_ab_cd_select:0,_ab_select:0,_cd_select:0});
		for (Integer wayCounter = 0; wayCounter < `_l2tlb_associativity; wayCounter = wayCounter + 1)
		begin
		      	tlb_table [entry][wayCounter]	<-	mkReg (Tlb_level2_type {
	_virtual_page_no  : 0,	_physical_frame_no	: tagged Invalid, _guest_state     : 0,_logical_partition_id: 0,_process_id	    : 0,
	_access_ctrl      : 0,  _write_back_through	: 0		,_write_allocate   : 0,_inhibiting	    : 0,_coherancy_required : 0});
		end
	end

/**************************************************            RULES         **************************************************/

	/*
		Tracks the clock
	*/
	rule rl_count_clock;
		rg_clock <= rg_clock + 1;
	endrule
	
	/*
		If the flush signal is set the TLB is flushed i.e. the content of TLB are erased and all registers are tagged as invalid
	 */
	rule rl_flush(wr_flush);
		rg_frame_details_to_l1_tlb_cache<=	tagged Invalid;		// Invalidate the frame details to be sent to L1 TLB and L1 Cache
		rg_state			<=	Handle_Request;		// Set the TLB to be Handle_Request
		rg_virtual_page_no_to_page_table<=	tagged Invalid;		// Invalidate the virtual page register
		rg_virtual_page_no_frm_cpu	<=	tagged Invalid;		// Invalidate the signal from CPU
		rg_interrupt			<=	0;			// Reset the interrupt
		rg_requester			<=	0;			// Set the requester as ITLB for default
		rg_enable			<=	True;
		rg_page_identifier		<=	Page_Identifier{	// Reset the page Identifier
								_user_supervisor	:	0,
								    _guest_state	:	0,
							   _logical_partition_id	:	0,
								  _address_space	:	0,
								     _process_id	:	0};
		// Reset all entries in the TLB and Pseudo LRU policy		
		for (Integer entry = 0; entry < `_l2tlb_total_entries; entry = entry + 1)
		begin
			plru[entry] <= PseudoLRU_type{_ab_cd_select:0,_ab_select:0,_cd_select:0};
			for (Integer wayCounter = 0; wayCounter < `_l2tlb_associativity; wayCounter = wayCounter + 1)
			begin
			      	tlb_table [entry][wayCounter] <= Tlb_level2_type {
		_virtual_page_no  : 0,	_physical_frame_no	: tagged Invalid, _guest_state     : 0,_logical_partition_id: 0,_process_id	    : 0,
		_access_ctrl      : 0,  _write_back_through	: 0		,_write_allocate   : 0,_inhibiting	    : 0,_coherancy_required : 0};
			end
		end
	endrule

	/*
		This rule performs the following actions:
		*	Checks if a request has been received by the TLB
		*	Checks if the state of the TLB is to Handle_Request
		*	Set the state to perform permission check
		*	Store the virtual page number, page identifier and read/write signal
		*	Match the stored guest state,logical partition ID,process ID,Virtual Page Number with the incoming request
		*	Set the way that matched
	 */
	rule rl_handle_request(rg_virtual_page_no_frm_l1_tlb matches tagged Valid ._virtual_page_no 
		&&& rg_frame_details_to_l1_tlb_cache matches tagged Invalid &&& rg_state == Handle_Request && !wr_flush);

		rg_access_count		<=	rg_access_count + 1;

		$display("Clock = %d\tStep  7:\tL2 TLB:\tState = Handle Request with Virtual page = %h  Address Space = %h",rg_clock,rg_virtual_page_no_frm_l1_tlb,rg_page_identifier_frm_l1_tlb._address_space);
		Bool isHitWay[`_l2tlb_associativity];
		Maybe#(Frame_details_type) frameOutput[`_l2tlb_associativity];		
		bit isInterrupt[`_l2tlb_associativity];
	
		/* 
			If the guest state is set to 0 i.e guest operating system is running, then the logical partition identifier
			is matched against the virtual identifier generated by the CPU. The process and virtual page number are matched against
			the entry stored in the TLB.It is also checked that the physical page number is valid or not in Ways respectively
		 */
		for(Integer count = 0; count < `_l2tlb_associativity; count = count + 1)
		begin
			isHitWay[count]			=	False;
			frameOutput[count]		=	tagged Invalid;
			Tlb_level2_type	_fetched_entry		=	tlb_table[_virtual_page_no`_l2tlb_page_index][count];
			if(	_fetched_entry._guest_state == rg_page_identifier_frm_l1_tlb._guest_state
				&& ((_fetched_entry._guest_state == 0 && _fetched_entry._logical_partition_id == rg_page_identifier_frm_l1_tlb._logical_partition_id)	
				|| _fetched_entry._guest_state == 1)
				&& _fetched_entry._process_id == rg_page_identifier_frm_l1_tlb._process_id
				&& _fetched_entry._virtual_page_no == _virtual_page_no
				&&& _fetched_entry._physical_frame_no matches tagged Valid (._physical_frame_no)
			)
			begin
				isHitWay[count]		=	True;
				if(
					rg_page_identifier_frm_l1_tlb._user_supervisor == 0 && ((rg_readWrite_frm_l1_tlb == 0 && _fetched_entry._access_ctrl`_l2tlb_user_read == 1) || (rg_readWrite_frm_l1_tlb == 1 && _fetched_entry._access_ctrl`_l2tlb_user_write == 1))
					||	rg_page_identifier_frm_l1_tlb._user_supervisor == 1 && ((rg_readWrite_frm_l1_tlb == 0 && _fetched_entry._access_ctrl`_l2tlb_supervisor_read == 1) || (rg_readWrite_frm_l1_tlb == 1 && _fetched_entry._access_ctrl`_l2tlb_supervisor_write == 1))
				)
				begin
					$display("Clock = %d\tStep  8:\tL2 TLB:\tHIT from %d Virtual page = %h Physical Page Number = %h  Address Space = %h",rg_clock,count,_virtual_page_no,_fetched_entry._physical_frame_no,rg_page_identifier._address_space); 		      
					isInterrupt[count]	=	0;					
					frameOutput[count]	=	tagged Valid Frame_details_type		{
				 		_physical_frame_no	:	fromMaybe(0,_fetched_entry._physical_frame_no),
						_process_id		:	_fetched_entry._process_id,
				      		_l1tlb_type		:	rg_page_identifier._address_space,
				     		_logical_partition_id	:	_fetched_entry._logical_partition_id,
						_guest_state		:	_fetched_entry._guest_state,
						_l2tlb_access_ctrl	:	_fetched_entry._access_ctrl,
						_write_back_through	:	_fetched_entry._write_back_through,
						_write_allocate		:	_fetched_entry._write_allocate,
						_inhibiting		:	_fetched_entry._inhibiting,
					 	_coherancy_required	:	_fetched_entry._coherancy_required	};
				end
				else
				begin
					$display("Clock = %d\tStep  8:\tL2 TLB:\tPermission denied from %d Virtual page = %h",rg_clock,count,_virtual_page_no); 
					isInterrupt[count]		=	1;
				end
			end
		end
		Integer hitWay = getHitWay(isHitWay,`_l2tlb_associativity);

		if(hitWay != `_l2tlb_associativity)
		begin
			rg_enable			<=	True;
			rg_state			<=	Handle_Request;				// Set the state of the TLB to handle request
			rg_virtual_page_no_frm_cpu	<=	tagged Invalid;				// Invalidate the virtual page from the CPU
			rg_frame_details_to_l1_tlb_cache<=	frameOutput[hitWay];			// send the frame details to L1 ICache
			rg_interrupt			<=	isInterrupt[hitWay];
			rg_virtual_page_no_to_page_table<=	tagged Invalid;
			rg_virtual_page_no_frm_l1_tlb	<=	tagged Invalid;
			// update the pseudo LRU policy
			plru[_virtual_page_no`_l2tlb_page_index]	<=	updatePseudoLRU(plru[_virtual_page_no`_l2tlb_page_index],hitWay);
			rg_hit_count			 <=	rg_hit_count + 1;		// Update the hit count
		end
		else
		begin	
			rg_enable			<=	False;
			rg_interrupt			<=	0;					// Reset the interrupt
			rg_state			<=	Handle_Response;			// Change the state to perform permission check
			rg_virtual_page_no_frm_cpu	<=	rg_virtual_page_no_frm_l1_tlb;		// Store the virtual page from the CPU
			rg_page_identifier		<=	rg_page_identifier_frm_l1_tlb;			// Store the page identifier
			rg_requester			<=	rg_page_identifier_frm_l1_tlb._address_space;
			rg_readWrite			<=	rg_readWrite_frm_l1_tlb;
			rg_frame_details_to_l1_tlb_cache       <=	tagged Invalid;				// Invalidate the signals to L1 TLB and L1 Cache
		end
	endrule
	
	/*
		In case of a miss i.e. when all the ways get a miss match, then the requested address is enqueued in a buffer
		and frame to be sent to the icache is invalidated
	 */
	rule rl_miss(rg_virtual_page_no_frm_cpu matches tagged Valid ._virtual_page_no &&& !wr_flush  && rg_state ==	Handle_Response);

		ReplacementInformation replacementInfo = victimWay(plru[_virtual_page_no`_l2tlb_page_index]);
		$display("Clock = %d\tStep  8:\tL2 TLB:\tMiss Virtual page = %h Virtual Index = %h Victim Way : %d",rg_clock,_virtual_page_no,_virtual_page_no`_l2tlb_page_index,replacementInfo.way); 		      
		rg_state 			       <= 	Stall;					// Change the state to Handle Stall
		rg_miss_count			       <=	rg_miss_count + 1;			// Increment the miss count		
		rg_virtual_page_no_to_page_table       <= 	rg_virtual_page_no_frm_cpu;		// Stores the virtual page number in a register 
		rg_virtual_page_no_frm_cpu	       <=	tagged Invalid;
		
		rg_frame_details_to_l1_tlb_cache       <=	tagged Invalid;				// Invalidate the signals to L1 TLB and L1 Cache
		rg_requester			       <=	rg_page_identifier._address_space;
		rg_interrupt			       <=	0;
		plru[_virtual_page_no`_l2tlb_page_index]	<=	replacementInfo.plru;
		rg_victim_way				<=	replacementInfo.way;			// get the way that needs to be replaced
	endrule

	/*
		When it is in stall state, get a valid physical page from the page table, reset the state to Handle_Request
		send the valid output to cache and update the TLB entry with the new logical partition, process, physical page number
		and virtual page number from the page table
	 */
	rule rl_handle_miss(rg_state == Stall && !wr_flush && !rg_enable 
			&&& wr_l2tlb_frame_details matches tagged Valid ._physical_frame
			&&& rg_virtual_page_no_to_page_table matches tagged Valid ._virtual_page_no);

		$display("Clock = %d\tStep 12:\tL2 TLB:\tVirtual page = %h TLB Physical Frame = %h Requester = %h",rg_clock,_virtual_page_no,_physical_frame._physical_frame_no,rg_requester);
		rg_enable				<=	True;
		rg_state 			 	<=	Handle_Request;				// set the state to Handle_Request until other request comes
		rg_virtual_page_no_to_page_table 	<=	tagged Invalid;				// Invalidate the register to send virtual page to page table
		rg_virtual_page_no_frm_l1_tlb		<=	tagged Invalid;
		rg_frame_details_to_l1_tlb_cache	<=	tagged Invalid;
		rg_virtual_page_no_frm_cpu		<=	tagged Invalid;
		tlb_table[_virtual_page_no`_l2tlb_page_index][rg_victim_way]	<=	Tlb_level2_type	{
				_guest_state:_physical_frame._guest_state,
			 	 _process_id:_physical_frame._process_id,
		       _logical_partition_id:_physical_frame._logical_partition_id,
			  _physical_frame_no:tagged Valid _physical_frame._physical_frame_no,
			    _virtual_page_no:_virtual_page_no,
			        _access_ctrl:_physical_frame._l2tlb_access_ctrl,
			 _write_back_through:_physical_frame._write_back_through,	     
			     _write_allocate:_physical_frame._write_allocate,	     
				 _inhibiting:_physical_frame._inhibiting,             
		         _coherancy_required:_physical_frame._coherancy_required	};
	endrule	

/******************************************** Methods ************************************************************/

	/*
		Get the Virtual page from the CPU along with the page identifiers
	*/
	method Action _virtual_page_no_from_cpu(Maybe#(Virtual_Page_No_Type) _virtual_page_no,Page_Identifier _page_identifier,bit _readWrite) if(rg_enable &&& rg_virtual_page_no_frm_l1_tlb matches tagged Invalid);
		rg_virtual_page_no_frm_l1_tlb 	 <= 	_virtual_page_no;	// Get the virtual page from CPU
		rg_page_identifier_frm_l1_tlb 	 <=	_page_identifier;	// Get the page identifier from the CPU
		rg_readWrite_frm_l1_tlb		 <=	_readWrite;		// Gets if it is a read request or write request
	endmethod
	
	/*
		Get the physical frame details from the Page Table
	 */
	method Action _physical_frame_from_page_table(Maybe#(Frame_details_type) _physical_frame);
		wr_l2tlb_frame_details 	 	 <= 	_physical_frame;	// Get the frame details from the page table
	endmethod

	/* 
		Flush the TLB
	*/
	method Action _flush();
		wr_flush 			<=  	True;			// Reset the Flush Mode
	endmethod

	method Action _response_forwarded_to_l1_tlb() if(rg_frame_details_to_l1_tlb_cache matches tagged Valid ._frame);
		rg_frame_details_to_l1_tlb_cache	<=	tagged Invalid;
	endmethod

	/* 
		The frame details which includes the access control details and the frame identifiers like physical frame number, logical partition number, process id etc
	*/
	method Maybe#(Frame_details_type) _physical_frame_details_to_l1_tlb_cache();
		return rg_frame_details_to_l1_tlb_cache;
	endmethod

	/*
		Notifies the MMU to get address from page table
	 */
	method Bool _is_enable_get_frame_from_page_table();
		return isValid(rg_virtual_page_no_to_page_table);
	endmethod

	method Bool _is_enable_get_next_request();
		return rg_enable;
	endmethod
	
	/*	
		Gets if the interrupt is generated or not
	*/
	method bit _interrupt();
		return rg_interrupt;
	endmethod

	/*
		Counts the number of hits and misses in L2 TLB
	*/
	method Combined_Counter_Type _hit_miss_count();
		return {rg_miss_count,rg_hit_count};				// Send the number of hits and misses
	endmethod

	method Counter_Type _access_count();
		return rg_access_count;						// Send the number of hits and misses
	endmethod
endmodule
endpackage
