/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package CPU_Decode_Execute;

// ================================================================

// This package models Decode and Execute stage of a RISC-V CPU

// ================================================================
// Bluespec libraries

import RegFile      :: *;
import FIFOF        :: *;
import DefaultValue :: *;
import SpecialFIFOs :: *;
import DefaultValue :: *;

// ================================================================
// Project imports

import TLM2		    :: *;	// Using local copy
import Req_Rsp      :: *;
import Sys_Configs  :: *;

import ISA_Defs     :: *;

`include "TLM.defines"
`include "RVC.defines"
// ================================================================


// structure to pass load/store information to function fn_mk_ls
typedef struct {
	TLMCommand cmd;
	GPR_Data   data;
	Addr 	   addr;
	UInt #(8)  bsize;
	LS_Remark  remark;
} LS_temp deriving (Bits);

interface Execute_IFC;
	method Action							write_executeData(IDEX_Data   execData);			// Read the data to be executed
    method Action      	    				write_ifr 	     (IFID_Data   regdata_dex);			// Read Instruction fetch register
	method Action							write_gpr        (RegName rd, GPR_Data data);		// Update Regfile. Used by commit stage
	method Action							write_verbosity  (int verbosity);					// Read verbosity

	method GPR_Data 						read_gpr         (RegName regname);
	method Wire #(Maybe #(CommitData))  	read_cd;											// Write Commit Register
	method Wire #(CPU_Stop_Reason)	        read_stop_reason;
endinterface

module mkExecute (Execute_IFC);
    
    // Registers
	Wire #(GPR_Data)        	  rg_pc			 	 <- mkWire;
    Wire #(Bool)	       		  rg_instr_valid  	 <- mkWire;
	Wire #(CPU_Stop_Reason) 	  rg_stop_reason	 <- mkBypassWire;
    Reg  #(Maybe #(Forward_Data)) rg_fwd_data_dex    <- mkReg(tagged Invalid);
    Wire #(Maybe #(Forward_Data)) wr_fwd_data_commit <- mkDWire(tagged Invalid);
    
	// Constant Register, Not to be changed.
//	Wire #(Instr)          rg_instr 		 <- mkBypassWire;
	Wire #(int)			   rg_verbosity		 <- mkBypassWire;
	Reg  #(Bool)		   rg_dbg_mode	 	 <- mkReg(False);
	Reg  #(Bit #(64))      rg_cycle			 <- mkRegU;

	// Register to store the commit data to be output by the Dec_Ex unit
	Wire #(Maybe #(CommitData)) rg_cd 	    <- mkWire;

	// The below five variables of Maybe type are exclusive.
	// Only on will be valid at a time.
	// Depending upon their validity, request of either write-back, load-store or jump/branch is made.

	Wire #(Maybe #(GPR_Data))    val_wb    <- mkDWire(tagged Invalid);
	Wire #(Maybe #(GPR_Data))    val_wb_3  <- mkDWire(tagged Invalid);
	Wire #(Maybe #(LS_temp))     val_ls    <- mkDWire(tagged Invalid);
	Wire #(Maybe #(RequestJump)) val_jmp   <- mkDWire(tagged Invalid);
	Wire #(Maybe #(Bool)) 		 val_halt  <- mkDWire(tagged Invalid);

	Wire #(Opcode)    opcode  <- mkWire;
	Wire #(RegName)   rs1	  <- mkWire;
	Wire #(RegName)   rs2	  <- mkWire;
	Wire #(RegName)   rd 	  <- mkWire;
	Wire #(Bit #(12)) imm_I	  <- mkWire;
	Wire #(Bit #(12)) imm_S	  <- mkWire;
	Wire #(Bit #(13)) imm_B	  <- mkWire;
	Wire #(Bit #(32)) imm_U	  <- mkWire;
	Wire #(Bit #(20)) imm_J	  <- mkWire;
	Wire #(Bit #(3))  funct3  <- mkWire;
	Wire #(Bit #(7))  funct7  <- mkWire;

	Wire #(GPR_Data) v1 <- mkWire;
	Wire #(GPR_Data) v2 <- mkWire;

	// General Purpose Register File declaration
    RegFile #(RegName, GPR_Data) regfile    <- mkRegFileFullLoad("regfile_initialize.txt");

	function GPR_Data fn_read_gpr (RegName r);
      return ((r == 0) ? 0 : regfile.sub (r));
   	endfunction
	
   	// ---------
    // Utility functions

	// Creates and returns a write back packet.
	// Contains destination register name and data
	function RequestWriteBack fn_mk_wb (RegName regname, GPR_Data data);
		return RequestWriteBack { data: data, rd: regname };
	endfunction

	// Creates and returns a Load-Store packet.
	// Contains memory request packet for Data Memory.
	function ReqLS fn_mk_ls (RegName rwb, LS_temp lst, TLMId #(`TLM_PRM_REQ_CPU) tid);
		
		// Create request description for data memory request
		Req_Desc_CPU req_desc  	= defaultValue;
		req_desc.command		= lst.cmd;
		req_desc.addr 			= lst.addr;
		req_desc.data 			= lst.data;
		req_desc.transaction_id = tid;
		req_desc.burst_size 	= reqSz_bytes_i(lst.bsize);

		Req_CPU req    			= tagged Descriptor req_desc;
	
		// Create CommitData for load store requests
		ReqLS reqls				= ReqLS { tlmreq: req, rd: rwb, remark: lst.remark };
	
		return reqls;
	endfunction

	function fn_report_exec_instr;
		case (opcode)
			op_IMM    : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_RR     : $display ("OPCODE: %b FUNCT7: %h FUNCT3: %h RD: %d RS1: %d RS2: %d", opcode, funct7, funct3, rd, rs1, rs2);
			op_LOAD   : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_STORE  : $display ("OPCODE: %b FUNCT3: %h RS1: %d RS2: %d IMM: %h", opcode, funct3, rs1, rs2, imm_S);
			op_BRANCH : $display ("OPCODE: %b FUNCT3: %h RS1: %d IMM: %h", opcode, funct3, rs1, rs2, imm_B);
			op_JALR	  : $display ("OPCODE: %b FUNCT3: %h RD: %d RS1: %d IMM: %h", opcode, funct3, rd, rs1, imm_I);
			op_JAL	  : $display ("OPCODE: %b RD: %d IM: %h", opcode, rd, imm_J);
			op_LUI	  : $display ("OPCODE: %b RD: %d IMM: %h", opcode, rd, imm_U);
			op_AUIPC  : $display ("OPCODE: %b RD: %d IMM: %h", opcode, rd, imm_U);
//			default	  : $display ("Instruction reporting failed for instruction %h", rg_instr);
		endcase
	endfunction

	// ---------
	// Execute Instructions
	
	// Declarations
	// Variables to store computed values from imm and registers to be written back into register file
	// At the end of the rule, these are checked for validity. Atmost 1 can be
	// if val_wb   == tagged Valid, then create a WriteBack request for Commit with rd = rd
	// if val_wb_3 == tagged Valid, then create a WriteBack request for Commit with rd = _rd
	// if val_ls   == tagged Valid, then create a LoadStore request for Commit with rd = rd	

	// ---------
	// OPCODE = BRANCH

	rule rl_operand_fetch (rg_instr_valid);
		v1 <= fn_read_gpr(rs1);
		v2 <= fn_read_gpr(rs2);
	endrule

	rule rl_temp_c00 (opcode == 'b00 && rg_instr_valid);
		val_halt <= tagged Valid True;
		rg_stop_reason <= CPU_STOP_EXIT;
	endrule

	// The following function performs right arithmetic shift on 'in' by the amount 'amt'
	// It is currently defined only for 32 bit values
	function GPR_Data fn_right_arith_shift (GPR_Data in, Bit #(5) amt);
		Bit #(64) _in;
		_in = (in[31] == 0) ? {zeroExtend(in)} : {signExtend(in)};
		return ((_in >> amt)[31:0]);
	endfunction

	rule rl_exec_IMM (opcode == op_IMM && rg_instr_valid);
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		//GPR_Data v1 = fn_init_src_reg (rs1);	// To store value of register source 1
		//GPR_Data v2 = fn_init_src_reg (rs2);	// To store value of register source 2

		Bit #(5) shift_amt = imm_I[4:0];
		Bit #(7) right_shift_type = imm_I[11:5];

		GPR_Data_S  sign_v1  = unpack(v1);
		GPR_Data_U  usign_v1 = unpack(v1);
	
		Bool no_match_1 = False, no_match_2 = False;
		case (funct3)
			f3_ADDI  : val_wb <= tagged Valid (v1 + signExtend (imm_I));
			f3_SLTI  : begin
						if (sign_v1 < unpack(signExtend(imm_I))) val_wb <= tagged Valid (1);
						else val_wb <= tagged Valid (0);
					  end
			f3_SLTIU : begin
						if (usign_v1 < unpack(signExtend(imm_I))) val_wb <= tagged Valid (1);
						else val_wb <= tagged Valid (0);
					  end
			f3_XORI  : val_wb <= tagged Valid (v1 ^ signExtend(imm_I));
			f3_ANDI  : val_wb <= tagged Valid (v1 & signExtend(imm_I));
			f3_ORI	 : val_wb <= tagged Valid (v1 | signExtend(imm_I));
			default  : no_match_1 = True;
		endcase

		case ({funct3, funct7})
			{f3_SLLI, f7_SLLI} : val_wb <= tagged Valid (v1 << shift_amt);
			{f3_SRLI, f7_SRLI} : val_wb <= tagged Valid (v1 >> shift_amt);
			{f3_SRAI, f7_SRAI} : val_wb <= tagged Valid (fn_right_arith_shift(v1, shift_amt));
			default : no_match_2 = True;
		endcase

		if (no_match_1 && no_match_2) rg_stop_reason <= CPU_STOP_INSTR_ERR;	// Instruction not supported

		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	rule rl_exec_load (opcode == op_LOAD && rg_instr_valid);
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		//GPR_Data v1 = fn_init_src_reg (rs1);	// To store value of register source 1
		//GPR_Data v2 = fn_init_src_reg (rs2);	// To store value of register source 2
		
		LS_temp ls_temp = LS_temp {cmd: READ, data: ?, addr: v1 + signExtend(imm_I), bsize: ?, remark: ?};

		case (funct3)
			f3_LB   : begin ls_temp.bsize = sz_byte;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LH   : begin ls_temp.bsize = sz_hword; ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LW   : begin ls_temp.bsize = sz_word;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			//f3_LD : RV64 only
			f3_LBU  : begin ls_temp.bsize = sz_byte;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LHU  : begin ls_temp.bsize = sz_hword; ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LWU  : begin ls_temp.bsize = sz_word;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			default : rg_stop_reason <= CPU_STOP_INSTR_ERR;	// Instruction not supported
		endcase
		
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	rule rl_exec_store (opcode == op_STORE && rg_instr_valid);
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		//GPR_Data v1 = fn_init_src_reg (rs1);	// To store value of register source 1
		//GPR_Data v2 = fn_init_src_reg (rs2);	// To store value of register source 2
		
		LS_temp ls_temp = LS_temp {cmd: WRITE, data: v2, addr: v1 + signExtend(imm_I), bsize: ?, remark: ?};

		case (funct3)
			f3_SB   : begin ls_temp.bsize = sz_byte;   val_ls <= tagged Valid ls_temp; end
			f3_SH   : begin ls_temp.bsize = sz_hword;  val_ls <= tagged Valid ls_temp; end
			f3_SW   : begin ls_temp.bsize = sz_word;   val_ls <= tagged Valid ls_temp; end
			//f3_SD	: RV64 only
			default : rg_stop_reason <= CPU_STOP_INSTR_ERR; // Instruction not supported
		endcase
		
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	rule rl_exec_branch (opcode == op_BRANCH && rg_instr_valid);
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		//GPR_Data v1 = fn_init_src_reg (rs1);	// To store value of register source 1
		//GPR_Data v2 = fn_init_src_reg (rs2);	// To store value of register source 2
		
		GPR_Data_S sign_v1 = unpack(v1);
		GPR_Data_S sign_v2 = unpack(v2);

		GPR_Data_U usign_v1 = unpack(v1);
		GPR_Data_U usign_v2 = unpack(v2);
		
		case (funct3)
			f3_BEQ : begin
						if (v1 == v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid}); 
					end

			f3_BNE : begin
						if (v1 != v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid}); 
					end

			f3_BLT : begin
						if (sign_v1 < sign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end
			
			f3_BGE : begin
						if (sign_v1 > sign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			// @TODO: unsigned comparision?
			f3_BLTU : begin
						if (usign_v1 < usign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			f3_BGEU : begin
						if (usign_v1 > usign_v2) val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid (rg_pc + signExtend(imm_B)), rd: ?, lr: tagged Invalid});
						else val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Invalid, rd: ?, lr: tagged Invalid});
					end

			default : rg_stop_reason <= CPU_STOP_INSTR_ERR;		// Instruction not found
		endcase

		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule
	
	rule rl_exec_jalr ((opcode == op_JALR && funct3 == f3_JALR) && rg_instr_valid);
		//GPR_Data v1 = fn_read_gpr(rs1);
		Addr target_addr = v1 + signExtend(imm_I);
		target_addr[0] = 0;
		Addr lr = rg_pc + zeroExtend(instlen_byte);
		val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid target_addr, rd: rd, lr: tagged Valid lr});
	endrule

	rule rl_exec_jal (opcode == op_JAL && rg_instr_valid);
		GPR_Data target_addr = rg_pc + signExtend(imm_J);
		Addr lr = rg_pc + zeroExtend(instlen_byte);
		val_jmp <= tagged Valid (RequestJump {branchAddr: tagged Valid target_addr, rd: rd, lr: tagged Valid lr});
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	rule rl_exec_lui (opcode == op_LUI && rg_instr_valid);
		Data lui_imm = imm_U;
		val_wb <= tagged Valid lui_imm;
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	rule rl_exec_auipc (opcode == op_AUIPC && rg_instr_valid);
		Addr lui_imm = (imm_U << 12);
		lui_imm = rg_pc + extend(lui_imm);
		val_wb <= tagged Valid lui_imm; 
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule

	// @TODO Define SYSTEM instructions
	/*
	rule rl_exec_system( opcode == op_SYSTEM && rg_instr_valid);
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		GPR_Data v1  = fn_read_gpr (rs1);	// To store value of register source 1
		GPR_Data v2  = fn_read_gpr (rs2);	// To store value of register source 2
		
		case (funct3)
			f3_SCALL  : begin ls_temp.bsize = BYTE;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_SBREAK : begin ls_temp.bsize = HWORD; ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			f3_SRET   : begin ls_temp.bsize = WORD;  ls_temp.remark = SIGNED; val_ls <= tagged Valid ls_temp; end
			//f3_LD   : RV64 only
			f3_LBU    : begin ls_temp.bsize = BYTE;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LHU    : begin ls_temp.bsize = HWORD; ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			f3_LWU    : begin ls_temp.bsize = WORD;  ls_temp.remark = UNSIGNED; val_ls <= tagged Valid ls_temp; end
			default   : rg_stop_reason <= CPU_STOP_INSTR_ERR;	// Instruction not supported
		endcase
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end
	endrule
	*/

	rule rl_mkCommitData (rg_instr_valid);
		Bool flag = False;
		CommitData cd = ?;
$display("v1: %d, v2: %d, opcode: %h, rs1: %b, rs2: %b", v1, v2, opcode, rs1, rs2);
		// Following three cases are always mutually exclusive
		if (val_wb  matches tagged Valid .v)		begin cd = tagged WB fn_mk_wb ( rd, v);    rg_fwd_data_dex <= tagged Valid Forward_Data{ rd: rd, data: v }; $display("1"); end
		else if (val_wb_3 matches tagged Valid .v)	begin cd = tagged WB fn_mk_wb ( rd, v);    rg_fwd_data_dex <= tagged Valid Forward_Data{ rd: rd, data: v }; $display("2");end
		else if (val_ls   matches tagged Valid .v)	begin cd = tagged LS fn_mk_ls ( rd, v, 1); rg_fwd_data_dex <= tagged Invalid; $display("3");end
		else if (val_jmp  matches tagged Valid .v)	begin cd = tagged JMP  v; rg_fwd_data_dex <= tagged Invalid; $display("4");end
		else if (val_halt matches tagged Valid .v)  begin cd = tagged Halt v; rg_fwd_data_dex <= tagged Invalid; $display("5");end
		else begin flag = True; rg_fwd_data_dex <= tagged Invalid; $display("E");end

		if (!flag) rg_cd <= tagged Valid cd;
		else rg_cd <= tagged Invalid;
		
		if (rg_verbosity > 1)
			$display($time," CPU: DEX: Commit Ready for PC: %h", rg_pc);
	endrule

	// ================ M Extension: MUL/DIV =========================================
	
	// =============== RV32M =========================================================

	function GPR_Data fn_mul_result (Bit #(XPRLEN_2) op1, Bit #(XPRLEN_2) op2, Bool take_complement, Bit #(1) low_high);
		Bit #(XPRLEN_2) mul = op1 * op2;
		Integer xlen = valueOf(XPRLEN), xlen_2 = valueOf(XPRLEN_2);
		GPR_Data _result = ?;
		if (take_complement == True) mul = ~mul+1;
		case (low_high)
			0: _result = truncate(mul);
			1: _result = mul[xlen_2-1:xlen];
		endcase
	
		return _result;
	endfunction

	rule rl_exec_mul_32 ( opcode == op_MUL_32 && rg_instr_valid );
		// Re-initialize to eliminate accidental use of garbage/previous iteration's value
		//GPR_Data v1 = fn_init_src_reg (rs1);	// To store value of register source 1
		//GPR_Data v2 = fn_init_src_reg (rs2);	// To store value of register source 2
	
		Bit #(XPRLEN_2) op1 = ?;
		Bit #(XPRLEN_2) op2 = ?;
		Bool take_complement = False;
		Integer xlen = valueOf(XPRLEN), xlen_2 = valueOf(XPRLEN_2);
		Bit #(1) sn_v1 = v1[xlen-1], sn_v2 = v2[xlen-1];

		GPR_Data_S  sign_v1  = unpack(v1);
		GPR_Data_S  sign_v2  = unpack(v2);

		GPR_Data_U  usign_v1 = unpack(v1);
		GPR_Data_U  usign_v2 = unpack(v2);

		Bit #(5) shift_amt = v2[4:0];
		
		GPR_Data div_result = ?;

		$display($time, " EXECUTE: PC: %h", rg_pc);
		// @TODO What if f3 matches but f7 does not. Error output?
		// @TODO Replace the default operations with custom made, more efficient multiplier/divider
		// @TODO Replace ADD/SUB with more efficient modules as well. Defined in other rules
		case ({funct3, funct7})
			// MUL/DIV Instructions
			{f3_MUL,    f7_MUL}     : begin
							  	 		op1 = signExtend(v1);
								 		op2 = signExtend(v2);
								 		val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 0);
							   		  end
			{f3_MULH,   f7_MULH}    : begin
								  		op1 = extend((sn_v1 == 1)?(~v1+1):(v1));
								  		op2 = extend((sn_v2 == 1)?(~v2+1):(v2));
								  		take_complement = (sn_v1 == sn_v2)?False:True;
								  		val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
			                	      end
			{f3_MULHSU, f7_MULHSU}  : begin
										op1 = extend((sn_v1 == 1)?(~v1+1):v1);
										op2 = extend(v2);
										take_complement = (sn_v1 == 1)?True:False;
										val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
								   	  end
			{f3_MULHU,  f7_MULHU}   : begin
										op1 = extend(v1);
										op2 = extend(v2);
										val_wb <= tagged Valid fn_mul_result(op1, op2, take_complement, 1);
				                      end
			{f3_DIV,    f7_DIV}     : if (sign_v2 != 0) begin div_result = (pack)(sign_v1 / ((sign_v2==0)?1:sign_v2));   val_wb <= tagged Valid div_result; end
			{f3_DIVU,   f7_DIVU}    : if (usign_v2 != 0) begin div_result = (pack)(usign_v1 / ((usign_v2==0)?1:usign_v2)); val_wb <= tagged Valid div_result; end
			{f3_REM,    f7_REM}     : if (sign_v2 != 0) begin div_result = (pack)(sign_v1 % ((sign_v2==0)?1:sign_v2));   val_wb <= tagged Valid div_result; end
			{f3_REMU,   f7_REMU}    : if (usign_v2 != 0) begin div_result = (pack)(usign_v1 % ((usign_v2==0)?1:usign_v2)); val_wb <= tagged Valid div_result; end
			
			// ADD/SUB Instructions
			{f3_ADD, f7_ADD}  		: val_wb <= tagged Valid (v1+v2);
			{f3_SUB, f7_SUB}  		: val_wb <= tagged Valid (v1-v2);
			{f3_SLL, f7_SLL}  		: begin val_wb <= tagged Valid (v1 << shift_amt); $display("SLL v1: %d, shift_amt: %d", v1, shift_amt); end
			{f3_SRL, f7_SRL}  		: val_wb <= tagged Valid (v1 >> shift_amt);
			{f3_SRA, f7_SRA}  		: val_wb <= tagged Valid (fn_right_arith_shift(v1, shift_amt));
			{f3_SLT, f7_SLT}  		: begin
										if (sign_v1 < sign_v2) val_wb <= tagged Valid (1);
										else val_wb <= tagged Valid (0);
					 				  end
	  		{f3_SLTU, f7_SLTU} 		: begin
										if (usign_v1 < usign_v2) val_wb <= tagged Valid (1);
										else val_wb <= tagged Valid (0);
							     	  end
			{f3_XOR, f7_XOR}  		: val_wb <= tagged Valid (v1 ^ v2);
			{f3_AND, f7_AND}  		: val_wb <= tagged Valid (v1 & v2);
			{f3_OR, f7_OR}    		: val_wb <= tagged Valid (v1 | v2);
			default   				: rg_stop_reason <= CPU_STOP_INSTR_ERR;
		endcase
		if (rg_verbosity > 1) begin $write($time, " CPU: DEX: "); fn_report_exec_instr(); end

	endrule

	// =============== END: RV32M ====================================================

	// =============== END: M Extension: MUL/DIV =====================================


	method Action write_executeData(IDEX_Data executeData);
		opcode <= executeData.opcode;
		rs1	   <= executeData.rs1;
		rs2	   <= executeData.rs2;
		rd	   <= executeData.rd;
		imm_I  <= executeData.imm_I;
		imm_S  <= executeData.imm_S;
		imm_B  <= executeData.imm_B;
		imm_U  <= executeData.imm_U;
		imm_J  <= executeData.imm_J;
		funct3 <= executeData.funct3;
		funct7 <= executeData.funct7;
		rg_pc  <= executeData.instrnAddr;
		rg_instr_valid <= True;
//		$display($time, "EXECTUE: Opcode: %b, rs1: %b, rs2: %b, rd: %b", opcode, rs1, rs2, rd);
	endmethod

	method Action write_ifr (IFID_Data data);
		rg_pc		   <= data.pc;
		rg_instr_valid <= True;
		//rg_instr 	   <= data.instr;
	endmethod
/*
	method Action write_cycle (Bit #(64) cycle);
		rg_cycle <= cycle;
	endmethod
	
	method Action write_cpu_mode (Bool cpu_mode);
		rg_dbg_mode <= cpu_mode;
	endmethod
*/
	method GPR_Data read_gpr (RegName regname);
		return fn_read_gpr(regname);
	endmethod

	method Action write_gpr (RegName regd, GPR_Data data);
		regfile.upd(regd, data);
	endmethod

	method Action write_verbosity (int verbosity);
		rg_verbosity <= verbosity;
	endmethod

	method Wire #(Maybe #(CommitData)) read_cd;		// write commit register
		return rg_cd; 
	endmethod

	method Wire #(CPU_Stop_Reason) read_stop_reason;
		return rg_stop_reason;
	endmethod

endmodule



endpackage
