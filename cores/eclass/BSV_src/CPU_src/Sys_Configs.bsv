/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package Sys_Configs;

// ================================================================

// This package defines the SoC configuration

// ================================================================
// bsc lib imports

import Vector :: *;
import TLM2   :: *;

`include "TLM.defines"
// ================================================================
// Project imports

import Req_Rsp :: *;

`include "RVC.defines"
`include "macro.defines"

// ================================================================
// Addresses, Data for this SoC

typedef 32  ASZ;
typedef 32  DSZ;
typedef TAdd #(TLog #(TDiv #(DSZ, 8)), 1) NBYTES; // Burst size in bytes as interpreted by memory

typedef Bit #(ASZ)     Addr;
typedef Bit #(DSZ)     Data;
typedef Bit #(NBYTES)  N_Bytes;

typedef enum { BYTES1, BYTES2, BYTES3, BYTES4 } TLMBurstSize_Bytes deriving (Eq, Bits);


// Function to convert Data and Instruction lengths into bytes format as maintained by TLMRequest.burst_size
// TLMBurstSize #(`TLM_PRM) --> Bit #(TLog #(TDiv #(data_size), 8))
// For data_size = 32, TLMBurstSize --> Bit #(2)
//		       2'b00 => 1 byte burst
//		       2'b01 => 2 byte burst
//		       2'b10 => 3 byte burst
//		       2'b11 => 4 byte burst

//function TLMBurstSize #(`TLM_PRM) reqSz_bytes_i (Integer sz) provisos (Bits #(TLMBurstSize_Bytes, TLog #(TDiv #(data_size, 8))));
function TLMBurstSize #(`TLM_PRM_REQ_CPU) reqSz_bytes_i (UInt#(8) sz);
   UInt #(8) bytes = (sz/8);
   TLMBurstSize_Bytes ret;
   case (bytes) matches
       1: ret = BYTES1;
       2: ret = BYTES2;
       3: ret = BYTES3;
	   4: ret = BYTES4;
	   default: ret = BYTES1; //@TODO
   endcase
   return pack(ret);
endfunction 


// Function to convert request size forwarded by CPU into Memory acceptable format
// For CPU: 'b00 -> 1 byte; For Mem: 'b01 -> 1 byte;
// This is required as TLMBurstSize follows this format

//function N_Bytes reqSz_bytes_mem (TLMBurstSize #(`TLM_PRM) sz) provisos (Add #(TLog #(TDiv #(data_size, 8)), k, NBYTES));
function N_Bytes reqSz_bytes_mem (TLMBurstSize #(`TLM_PRM_REQ_CPU) sz);
   N_Bytes n_b = extend(sz);
   return n_b + 1;
endfunction

// Number of Memory ports
// For now, memory is the only target. CPU is the only initiator

typedef 2  N_Mem_Ports;	
typedef TAdd #(N_Mem_Ports, 0) Max_Targets;

typedef 2  N_CPU_Ports;
typedef TAdd #(N_CPU_Ports, 0) Max_Initiators;

// Port mappings for Memory
// Two memory ports are defined. Can be changed by increasing the N_Mem_Ports above
// Port 0 responds to CPU Instruction requests
// Port 1 responds to CPU Data requests

Integer mem_instr_port = 0;
Integer mem_data_port = 1;

// Port mappings for Bus
// For CPU
Integer bus_cpu_instr_port = 0;
Integer bus_cpu_data_port = 1;

// For MEM
Integer bus_mem_instr_port = 0;
Integer bus_mem_data_port = 1;

// Instruction Memory base address and size
// Required since dynamic linker, loader are not available. Code is statically compiled
Addr imem_base_addr = fromInteger(valueOf(`INSTRUCTION_MEM_BASE_ADDRESS));
Addr imem_size      = fromInteger(valueOf(`INSTRUCTION_MEM_SIZE));
Addr imem_max_addr  = imem_base_addr + imem_size - 1;

// Data Memory base address and size
// Required since dynamic linker, loader are not available. Code is statically compiled
Addr dmem_base_addr = fromInteger(valueOf(`DATA_MEM_BASE_ADDRESS));
Addr dmem_size      = fromInteger(valueOf(`DATA_MEM_SIZE));
Addr dmem_max_addr  = dmem_base_addr + dmem_size - 1;

endpackage: Sys_Configs
