/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package CPU;

// ================================================================

// This package models a CPU based on RISC-V ISA
// Supported RISC-V Extensions are Base Integer Instruction Set, 32-Bit, (RV32I) and Standard Extension for Multiplication and Division, 32-Bit, (RV32M)
// Basic configuration -
// 		1. Data Width: 32-bit
//		2. Address Width: 32-bit
//		3. Number of General: Purpose Registers: 32
//		4. Width of each General: Purpose Register: 32-bit
//		5. Number of Pipeline stages: 3
// Pipeline Stages - Fetch:   Fetches next instruction. Update PC
//					 Decode:  Prepares signals for execute stage
// 					 Execute: Performs ALU operations for integer computation instructions
//							  Performs Address calculation and manipulates data for LOAD/STORE instructions
//							  Evaluates Branch condition and calculates target address for Branch Instructions
//							  Performs write-back into regfile at the end of instruction execution
// Always Not-taken static branch prediction is in place
// As no stage before execute modifies the involved operands, or the architectural state of the processor, no data hazard will occur
// Only implemented Privilege-Mode is Machine-Level Privilege mode
// Specification: RISC-V User-Level ISA Specification 2.0, RISC-V Privileged ISA Specification 1.7

// ================================================================
// Bluespec libraries

import RegFile      :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import DefaultValue :: *;
import SpecialFIFOs :: *;

// ================================================================
// Project imports and includes

import TLM2	   			  :: *;	// Using local copy
import Utils       		  :: *; // Utility functions and typeclasses to zip TLMRecvIFC and TLMSendIFC with FIFOs
import Req_Rsp      	  :: *; // Request and Response packet types
import Sys_Configs  	  :: *; // Overall System Configuration file
import CPU_Execute		  :: *; // Execute Stage of CPU (Here, responsible for ALU operations and address calculation)
import ISA_Defs 	      :: *; // RISC-V Instruction specifications
import ISA_Defs_PRV		  :: *;	// RISC-V Privilege ISA specifications

`include "TLM.defines"			// Parameters for TLM packets
`include "RVC.defines"			// Local Definitions
`include "macro.defines"		// System Configuration Parameters

// ================================================================
// Reasons why the CPU is currently stopped
// ----------------------------------------
// This error reporting is solely for debugging purposes and should be used only in debug mode
// These error messages are not a part of the RISC-V ISA specification
// RISC-V supported exceptions are implemented separately

function Action display_stop_reason (String pre, CPU_Stop_Reason reason, String post);
   action
      $write ($time, pre);
      case (reason)
	 CPU_STOP_BREAK:           $write ("CPU_STOP_BREAK");			// A stop was explicitly requested (for ex. by testbench)
	 CPU_STOP_EXIT:            $write ("CPU_STOP_EXIT");			// Exited as the end of instruction stream was reached
	 CPU_STOP_INSTR_ERR:       $write ("CPU_STOP_INSTR_ERR");		// An error was encountered while executing an instruction (Maybe unrecognized instruction)
	 CPU_STOP_INSTR_UNIMPLEM:  $write ("CPU_STOP_INSTR_UNIMPLEM");	// An instruction that is supposed to be supported but has not yet been implemented was encountered
	 CPU_STOP_MEM_ERR:         $write ("CPU_STOP_MEM_ERR");			// Memory read/write resulted in an error
      endcase
      $write (post);
   endaction
endfunction

// ================================================================
// CPU interface

// Interface layout for CPU
interface CPU_IFC;
   // Ports for interacting with devices and peripherals
   interface TLMSendIFC #(Req_CPU, Rsp_CPU)  imem_ifc;	// For Instruction Memory
   interface TLMSendIFC #(Req_CPU, Rsp_CPU)  dmem_ifc;  // For Data Memory

   // GDB handling
   // Conditionally compiled. Used only when operating in Debug or Verification Mode

   `ifndef CPU_MODE_NORMAL // Conditionally compile only when not operating in normal mode
   method Action           			reset;										// Reset CPU to Initial state
   method Action           			run_continue (Maybe #(GPR_Data) mpc);		// Execute all instructions until the end of instruction stream
   method Action           			run_step     (Maybe #(GPR_Data) mpc);		// Execute next instruction only
   method Action           			stop;										// Stop CPU
   method CPU_Stop_Reason  			stop_reason;								// Returns the reason why CPU is currently stopped
   method GPR_Data         			read_pc;									// Read Current PC. (Address of instruction to be fetched)
   method GPR_Data         			read_exec_pc;								// Read Currently executed PC. (Address of instruction currently being executed, i.e., is in third stage)
   method Action           			write_pc (GPR_Data d);						// Update PC. Used by testbench to provide initial PC
   method GPR_Data         			read_gpr (RegName r);						// Read a General-Purpose Register
   method Action           			write_gpr (RegName r, GPR_Data d);			// Write into a General-Purpose Register
   method Action           			req_read_memW (GPR_Data addr);				// Request to read a word (or range) in data memory
   method ActionValue #(GPR_Data)	rsp_read_memW ();							// Response for a request generated using req_read_memW method
   method Action           			write_memW (GPR_Data addr, GPR_Data d);		// Write a word (or range) in data memory
   method UCSR_Data_64     			read_instret;								// Read the number of instructions retired. This is the number of instructions already committed
   method UCSR_Data_64     			read_cycle;									// Read number of cycles past

   // Misc.
   method Action set_verbosity (int verbosity);		// Defines the frequency with which management information is printed
   `endif	// !CPU_MODE_NORMAL
endinterface

// ================================================================
// CPU model

// Current state of the processor. Fetches next instruction in Fetch mode. In Stopped mode, waits for a debug method to request further operation
typedef enum {CPU_FETCH, CPU_STOPPED} CPU_State deriving (Eq, Bits);

// Mode of operation of CPU. In DBG mode, the execution halts after every instruction (until run_continue is called by the debugger) and
// the control passes to the testbench which requests another operation through the debug methods
// In normal mode, all instructions until the end of instruction stream are executed. Thereafter, CPU waits for new instructions to arrive
typedef enum {CPU_DBG, CPU_NORM} CPU_Mode deriving (Eq, Bits);

// Attributes to the CPU_Model
// conflict_free = "rl_execute, rl_commit_1, rl_commit_2" was introduced because of a known bluespec error.
(* synthesize, conflict_free = "rl_read_execute, rl_commit_1, rl_commit_2" *)
module mkCPU_Model (CPU_IFC);

   // CPU Architectural state
   Reg   #(GPR_Data) rg_pc    <- mkRegU;	// Program Counter. Holds address of instruction to be fetched next

   // Misc. micro-architectural state
   Reg #(int)              rg_verbosity      <- mkReg(0);			 // Frequency of error messages to be printed 
   Reg #(Bool)             rg_stop_requested <- mkReg(False);		 // True if a stop was requested by the previous instruction
   Reg #(CPU_State)        rg_cpu_state      <- mkReg(CPU_STOPPED);  // State of the CPU
   Reg #(CPU_Mode)	       rg_cpu_mode       <- mkReg(CPU_DBG);		 // Mode of operation of CPU 
   Reg #(CPU_Stop_Reason)  rg_stop_reason    <- mkRegU;				 // Reason why CPU is currently stopped. Used only in debug mode
   Reg #(GPR_Data)		   rg_cycle			 <- mkReg(0);			 // Number of clock cycles executed
   Reg #(GPR_Data)		   rg_instret		 <- mkReg(0);			 // Number of instructions retired

   // Global Reset Signal
   PulseWire pwr_sysReset <- mkPulseWire;

   // FIFOFs for requests to/responses from memory
   FIFOF #(Req_CPU) f_imem_reqs <- mkBypassFIFOF;	// Enqueued to make a request to instruction memory
   FIFOF #(Rsp_CPU) f_imem_rsps <- mkBypassFIFOF;	// Repsonse from Instruction memory arrives in this FIFO

   FIFOF #(Req_CPU) f_dmem_reqs <- mkBypassFIFOF;	// Enqueued to make a request to Data memory
   FIFOF #(Rsp_CPU) f_dmem_rsps <- mkBypassFIFOF;	// Repsonse from Data memory arrives in this FIFO

   // Pipeline FIFOFs between stages
   FIFOF #(IFID_Data)  f_if_id  <- mkPipelineFIFOF; // FIFOF between IF and ID Stage.
   FIFOF #(IDEX_Data)  f_id_ex  <- mkPipelineFIFOF; // FIFOF between ID and EX Stage.

   // ------------
   // Module instantiations

   // Instantiate Decode-Execute stage module
   Execute_IFC execute <- mkExecute;   

   // ----------------
   // Misc Registers


   Reg #(Maybe #(GPR_Data)) rg_branchTarget[2] <- mkCReg(2, tagged Invalid); // Valid if a branch was to be taken, and holds the target address. Invalid otherwise

   Wire #(Bool) wr_flush <- mkDWire(False);	// Signal to flush pipeline stages. Used in case of a mis-predicted branch

   Wire #(CommitData) wr_commit_data <- mkWire;

   PulseWire pwr_instrnRetired <- mkPulseWire;
   
   // ---------
   // Helper functions

   // Helper function to evaluate explicit rule conditions
   function Bool fn_fetch_condition;
	   Bool dbg = ((rg_cpu_mode == CPU_DBG)  && (rg_cpu_state == CPU_FETCH));
	   Bool nrm = ((rg_cpu_mode == CPU_NORM) && (rg_cpu_state == CPU_FETCH));
	   return (dbg || nrm);
   endfunction

   function Bool fn_decode_condition;
	   Bool dbg = (rg_cpu_mode == CPU_DBG)  && (rg_cpu_state != CPU_STOPPED);
	   Bool nrm = (rg_cpu_mode == CPU_NORM) && (rg_cpu_state != CPU_STOPPED);
	   return (dbg || nrm);
   endfunction

   function Bool fn_execute_condition;
	   Bool dbg = (rg_cpu_mode == CPU_DBG)  && (rg_cpu_state != CPU_STOPPED);
	   Bool nrm = (rg_cpu_mode == CPU_NORM) && (rg_cpu_state != CPU_STOPPED);
	   return (dbg || nrm);
   endfunction

   function Bool fn_debug_method_cond;
	   Bool dbg = (rg_cpu_mode == CPU_DBG) && (rg_cpu_state == CPU_STOPPED);
	   return dbg;
   endfunction

   Bool  	   fetch_cond = fn_fetch_condition;
   Bool    	  decode_cond = fn_decode_condition;
   Bool      execute_cond = fn_execute_condition;
   Bool debug_method_cond = fn_debug_method_cond;

   // Update clock cycles
   rule rl_count_cycles (rg_cpu_state != CPU_STOPPED);
      rg_cycle [0] <= rg_cycle [0] + 1;
   endrule
   

   // ----------------
   // Misc. rules for debugger

   rule cpu_stop (rg_stop_requested == True && pwr_instrnRetired);
	  rg_cpu_state <= CPU_STOPPED;
   endrule

   // =================================================================================
   // STAGE-1: FETCH
   // --------------
   // Request instruction pointed to by PC from Instruction Memory. Done by rule 'rl_fetch_mkreq'
   // Wait for the instruction memory to respond. Update PC as 'PC + instruction length' if no branch was to be taken
   // Else update it with value held in 'rg_branchTarget'. Done by rule 'rl_fetch_getrsp'
   // =================================================================================

   /* =================================================================================
   // Note on TLM Transactions
   // ------------------------
   // All exchanges external to CPU are made in the form of TLM transactions
   // Bluespec provides TLM library to standardize exchange of data between modules
   // This implementation uses TLM version 2
   // Two type of TLM transactions exist - 1. Request 2. Response
   // 1. Request: TLMRequest is a union that can take one of the following two forms
   // ----------- RequestDescriptor: For single transfers or first transfer of a burst
   // 			  Contains relevant fields like Addr, Data, Burst_length, transcation_id
   // 			  required to initiate a transaction
   //			  RequestData: For follow-up transfers of a burst mode. Contains only data
   //			  and transaction_id field as other information was set previously
   // 2. Response: TLMResponse is the response generated by the slave
   // ------------
   // Both of these packets are parameterised with the following parameters -
   // (id_size, addr_size, data_size, uint_size, cstm_type)
   // We have used TLM transactions to leverage standard libraries for components like
   // Buses provided by Bluespec
   // =================================================================================
   */

   // -------------
   // Setup and send instruction fetch request to memory
   rule rl_fetch_mkreq (fetch_cond);
      Req_Desc_CPU req_desc;	// Make a TLM Descriptor to initiate a transfer
      req_desc = defaultValue;	// Currently, only single length transfers are used. At a time, a word is fetched from instruction memory instead of a page
      req_desc.command    = READ;								  // Read from instruction memory
      req_desc.data       = ?;									  // No data to be sent for 'Read' request
      req_desc.addr       = rg_pc;  							  // Read from location pointed to by PC (rg_pc)
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // 'burst_size' field for 32-bit wide data channel is 2-bits long. Therefore, '00' encodes a 'byte' transfer whereas '11' indicates 'word'
	  															  // Therefore 'reqSz_bytes_i' function is required to convert the length of requested data to standard TLM format
	  req_desc.burst_mode = INCR;								  // In INCR burst mode, address for all requests after the first one is sequentially incremented. This is irrelevant for transfers with burst_length = 1
	  req_desc.transaction_id = 0;								  // Used to identify the source of request
      Req_CPU req = tagged Descriptor req_desc;				      // Only TLMRequest packet is sent over a connection. So RequestDescriptor or RequestData requests are converted to TLMRequest before being sent

	  f_imem_reqs.enq (req); // Enquing into 'f_imem_reqs' automatically sends a request to instruction memory slave
	  						 // This happens as an enque operation on this FIFO triggers the put method of interface 'imem_ifc' of module CPU_Model

	  if (rg_verbosity > 1)
	 	$display ($time," CPU: FETCH: Requesting Instruction Memory. PC = %0h", rg_pc);
   endrule

   // Wait for instruction memory to respond. Setup Decode stage
   rule rl_fetch_getrsp (fetch_cond);
	  Rsp_CPU rsp = f_imem_rsps.first;	// 'f_imem_rsps' is automatically enqued with response generated by instruction memory
      f_imem_rsps.deq;					// This happens as this FIFO is linked with the get method of interface 'imem_ifc' of module CPU_Model

	  IFID_Data decode_data = IFID_Data { pc: rg_pc, instr: truncate(rsp.data) };

      f_if_id.enq(decode_data);

	  if (rg_branchTarget[1] matches tagged Valid .v) begin 
	  		rg_pc <= v;
			rg_branchTarget[1] <= tagged Invalid;
	  end
	  else rg_pc <= rg_pc + extend(instlen_byte);

	  if(rg_verbosity > 1)	
	  	$display($time," CPU: FETCH: Response received from Instruction Memory for PC: %h. Recived Data: %h\n\n", rg_pc,rsp.data);
   endrule
   
   // ===================== END: FETCH =============================================

   // ==============================================================================
   // Stage-2: INSTRUCTION DECODE
   // ---------------------------
   // Sets up data for execute stage
   // ==============================================================================

   	rule rl_decode (decode_cond);
		let decodeData = f_if_id.first;	f_if_id.deq;
		let instrnData = decodeData.instr;
		let instrnAddr = decodeData.pc;

		IDEX_Data executeData;

    	// Decode instruction
		executeData.opcode  = instr_opcode  (instrnData);
		executeData.rs1	    = instr_rs1     (instrnData);
		executeData.rs2	    = instr_rs2     (instrnData);
		executeData.rd	    = instr_rd      (instrnData);
		executeData.imm_I   = instr_imm_I   (instrnData);
		executeData.imm_S   = instr_imm_S   (instrnData);
		executeData.imm_B   = instr_imm_B   (instrnData);
		executeData.imm_U   = instr_imm_U   (instrnData);
		executeData.imm_J   = instr_imm_J   (instrnData);
		executeData.funct3  = instr_funct3  (instrnData);
		executeData.funct7  = instr_funct7  (instrnData);
		executeData.instrnAddr = instrnAddr;
		executeData.illegal = (instrnData == 'h0000006f) ? True : False;	// According to RISC-V ISA, '0000006F' is a self-loop instruction and is therefore treated as illegal
																			// For verification purposes, we append this instruction to the instruction stream to indicate its end
		f_id_ex.enq(executeData);
		
		if (rg_verbosity > 1)$display($time, " CPU: DECODE: Instruction decoded. PC: %h, Instruction: %h", instrnAddr, instrnData);
		if (rg_verbosity > 2)$display($time, " CPU: DECODE: Opcode: %b, rs1: %d, rs2: %d, rd: %d, f3: %b, f7: %b", executeData.opcode, executeData.rs1, executeData.rs2, executeData.rd, executeData.funct3, executeData.funct7);
	endrule

   // ===================== END: INSTRUCTION DECODE=================================
   
   // ==============================================================================
   // Stage-3: EXECUTE
   // ==============================================================================

    // EXECUTE UNIT (ALU)
	// ==================
    
	// SET-UP ALU
	rule rl_update_execute (execute_cond);
		let executeData = f_id_ex.first;
		
		execute.write_executeData (executeData);
		execute.write_verbosity (rg_verbosity);

		if (rg_verbosity > 1) begin
			$display($time," CPU: EXECUTE: Setting up EXECUTE unit. PC: %h", f_id_ex.first.instrnAddr);
		end
	endrule

	// Receive response from ALU and setup Write-Back
	rule rl_read_execute (execute_cond);
		case (execute.read_cd) matches

			tagged Valid   .v: begin
									if(v matches tagged Halt .hlt) $finish(); 
									wr_commit_data <= v;
							   end

			tagged Invalid .v: begin
							   		rg_stop_reason <= execute.read_stop_reason;
							   		if(rg_verbosity > 1) display_stop_reason(" CPU: Stop/Err Reason: ", execute.read_stop_reason, "\n");
									f_id_ex.deq;
							   end

		endcase
	endrule

  
   // MEMORY-ACCESS and WRITE-BACK
   // ======================================
    
   // Rule to commit request
   // Behaviour: if writeback request is recieved, write back to register file using methods provided 
   // 				by dex module, deq from dex_commit register
   //			 else if halt is encountered, implement $finsih
   //			 else if load/store is encountered, enque request to f_dmem, do not deque from dex_commit register until load/store is completed

   rule rl_commit_1 (execute_cond);
		CommitData req = wr_commit_data;
		Bool branch_taken = False;
		Maybe #(GPR_Data) next_pc = tagged Invalid;

		// check for different types of packets
		case (req) matches
			tagged WB    .v: begin
								// Write-back the computed data to ARF
								execute.write_gpr (v.rd, v.data);

								// Indicate that an instruction has retired
								pwr_instrnRetired.send();

								// Decode pipeline FIFO between Decode and Execute stage
							    f_id_ex.deq;

							  	if (rg_verbosity > 1) $display($time, " CPU: EXECUTE: Instruction performed an arithematic or logical operation. ARF Updated. REG: %d Value: %h", v.rd, v.data);
							 end

			tagged LS    .v: begin 
								// Enque LOAD/STORE request to 'f_dmem_reqs' FIFO. Thereafter, the CPU waits until a response is receieved in 'f_dmem_rsps' which fires rule 'rl_commit_2'
								Req_CPU r = v.tlmreq;
								f_dmem_reqs.enq(r);

							 end

			tagged Halt  .v: if (v == True) begin
							    f_id_ex.deq;
								// Indicate that an instruction has retired
								pwr_instrnRetired.send();
								$finish();
							   	if(rg_verbosity > 1) display_stop_reason(" CPU: Stop/Err Reason: ", execute.read_stop_reason, "\n");
		                     end
			tagged JMP   .v: begin
					 			f_id_ex.deq;
					 			if (v.branchAddr matches tagged Valid .npc) begin
									next_pc = v.branchAddr;
							  		if (rg_verbosity > 1) $display($time, " CPU: EXECUTE: Branch instruction encountered. Taken. Updated PC: %h", npc);
									if (v.lr matches tagged Valid ._lr) begin
										execute.write_gpr (v.rd, _lr);
									end
									branch_taken = True;
									wr_flush <= True;
								end
								else if (rg_verbosity > 2) $display($time, " CPU: EXECUTE: Branch instruction encountered. Not Taken");
								// Indicate that an instruction has retired
								pwr_instrnRetired.send();
							 end
		endcase
		
		// Update branch taken register
		// Written separately out of the case to validate rg_branch_taken for all cases, ie, when instruction is other than jump
		if(branch_taken == True) begin rg_branchTarget[0] <= next_pc; end
		else rg_branchTarget[0] <= tagged Invalid; 

   endrule

   // Rule to process load/store requests
	rule rl_commit_2 (execute_cond);
		let req = wr_commit_data;
		Rsp_CPU rsp = f_dmem_rsps.first; f_dmem_rsps.deq;

		if (req matches tagged LS .reqls) begin	
			GPR_Data temp_data;
			Bit #(8)  data_8  = truncate(rsp.data);
			Bit #(16) data_16 = truncate(rsp.data);
			Bit #(24) data_24 = truncate(rsp.data);
			Bit #(32) data_32 = truncate(rsp.data);
			let rsize = reqls.tlmreq.Descriptor.burst_size;
			case (reqls.remark)
				UNSIGNED : temp_data = zeroExtend(rsp.data);
				SIGNED   : case (rsize)
							0: temp_data = signExtend(data_8);
							1: temp_data = signExtend(data_16);
							2: temp_data = signExtend(data_24);
							3: temp_data = signExtend(data_32);
							default: temp_data = signExtend(rsp.data);
						   endcase 
				default  : temp_data = extend(rsp.data);
			endcase
			if (rsp.command == READ) execute.write_gpr (reqls.rd, temp_data);
			if (rg_verbosity > 1) begin
					if (rsp.command == READ)  $display($time, " CPU: EXECUTE: Instruction performed a LOAD operation. Addr: %h, REG: %d, Value: %h", reqls.tlmreq.Descriptor.addr, reqls.rd, temp_data);
					if (rsp.command == WRITE) $display($time, " CPU: EXECUTE: Instruction performed a STORE operation. Addr: %h, Value: %h", reqls.tlmreq.Descriptor.addr, reqls.tlmreq.Descriptor.data);
			end
		end
		f_id_ex.deq;
		// Indicate that an instruction has retired
		pwr_instrnRetired.send();
	endrule
  
   // Rule to flush all stages
   rule rl_flush (wr_flush);
		f_if_id.clear;
		f_id_ex.clear;
		f_dmem_reqs.clear;
		f_dmem_rsps.clear;
		f_imem_reqs.clear;
		f_imem_rsps.clear;
   endrule
   // ===================== END: EXECUTE ===========================================
  
   // INTERFACE
   // ----------------
   // Instruction and Data memories
   interface imem_ifc = toSendIFC (f_imem_reqs, f_imem_rsps);
   interface dmem_ifc = toSendIFC (f_dmem_reqs, f_dmem_rsps);


   // =====================================================================================================================
   // ----------------
   // GDB control
   // Work in progress. Not final yet.

   `ifndef CPU_MODE_NORMAL
   
   method Action reset if (rg_cpu_state == CPU_STOPPED);
      rg_cycle [0]		  <= 0;
      rg_instret          <= 0;
      rg_verbosity        <= 3;
	  `ifdef CPU_MODE_NORMAL
	 	 rg_cpu_mode <= CPU_NORM;
	  `else
	  	 rg_cpu_mode <= CPU_DBG;
	  `endif
	  wr_flush			  <= True;
	  pwr_sysReset.send();
   endmethod

   method Action run_continue (Maybe #(GPR_Data) mpc) if (debug_method_cond);
      if (mpc matches tagged Valid .new_pc) rg_pc <= new_pc;
      rg_stop_requested <= False;
      rg_stop_reason    <= CPU_STOP_BREAK;
      rg_cpu_state      <= CPU_FETCH;
   endmethod

   method Action run_step (Maybe #(GPR_Data) mpc) if (debug_method_cond);
      if (mpc matches tagged Valid .new_pc) rg_pc <= new_pc;
      rg_stop_requested <= True;
      rg_stop_reason    <= CPU_STOP_BREAK;
      rg_cpu_state      <= CPU_FETCH;
   endmethod

   method Action stop;
      rg_stop_requested <= True;      // Requests stop before next instr
   endmethod

   method CPU_Stop_Reason stop_reason () if (debug_method_cond);
      return rg_stop_reason;
   endmethod

   method GPR_Data read_pc () if (debug_method_cond);
      return rg_pc;
   endmethod

   method GPR_Data read_exec_pc () if (debug_method_cond);
      return execute.read_exec_pc;
   endmethod

   method Action write_pc (GPR_Data d) /*if (rg_cpu_mode == CPU_NORM ||debug_method_cond)*/;
      rg_pc <= d;
      if (rg_cpu_mode == CPU_NORM)
			  rg_cpu_state <= CPU_FETCH;
   endmethod

   method GPR_Data read_gpr (RegName r) if (debug_method_cond);
	  return execute.read_gpr (r);
   endmethod

   method Action write_gpr (RegName r, GPR_Data d) if (debug_method_cond);
      execute.write_gpr (r, d);
   endmethod


   method Action req_read_memW (GPR_Data addr) if (debug_method_cond);
      Req_Desc_CPU req_desc;
      req_desc = defaultValue;
      req_desc.command    = READ;
      req_desc.data       = ?;
      req_desc.addr       = addr;
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // burst_size field for ASZ 32-bits is 2-bits long. b'01 -> 2 bytes of data
	  req_desc.burst_mode = INCR;
	  req_desc.transaction_id = 1;
      Req_CPU req = tagged Descriptor req_desc;

      f_dmem_reqs.enq (req);
   endmethod

   method ActionValue #(GPR_Data) rsp_read_memW () if (debug_method_cond);
      Data d = f_dmem_rsps.first.data; f_dmem_rsps.deq;
	  rg_stop_requested <= True;
      return d;
   endmethod

	method Action write_memW (GPR_Data addr, GPR_Data d) if (debug_method_cond);
	  Req_Desc_CPU req_desc;
      req_desc = defaultValue;
      req_desc.command    = WRITE;
      req_desc.data       = d;
      req_desc.addr       = addr;
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // burst_size field for ASZ 32-bits is 2-bits long. b'01 -> 2 bytes of data
	  req_desc.burst_mode = INCR;
	  req_desc.transaction_id = 1;
      Req_CPU req = tagged Descriptor req_desc;

      f_dmem_reqs.enq (req);
	  rg_stop_requested <= True;
   endmethod

   method GPR_Data read_instret if (rg_cpu_state == CPU_STOPPED);
      return execute.read_instret;
   endmethod

   method GPR_Data read_cycle if (rg_cpu_state == CPU_STOPPED);
      return execute.read_cycle;
   endmethod

   // ----------------
   // Misc.

   method Action set_verbosity (int verbosity);
      rg_verbosity <= verbosity;
   endmethod

   `endif // !CPU_MODE_NORMAL
endmodule

// ================================================================

endpackage: CPU
