/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package Memory_Model;

import RegFile 		:: *;
import TLM2 		:: *;
import Utils 		:: *;
import FIFOF		:: *;
import SpecialFIFOs	:: *;
import DefaultValue :: *;
import Vector		:: *;
import Req_Rsp		:: *;
import ISA_Defs		:: *;
import Sys_Configs  :: *;

interface Memory_IFC;
	interface TLMRecvIFC #(Req_MEM, Rsp_MEM) bus_ifc;
endinterface

module mkMemory_Model #(String mem_init_file, Addr mem_base_address, Addr mem_size) (Memory_IFC);

FIFOF #(Req_MEM) ff_reqs <- mkBypassFIFOF;
FIFOF #(Rsp_MEM) ff_rsps <- mkBypassFIFOF;

RegFile #(UInt #(32), Instr) memory <- mkRegFileFullLoad (mem_init_file);//, unpack(mem_base_address) >> 2, unpack(mem_size));

rule rl_process_requests;
	
	Req_MEM request = ff_reqs.first; ff_reqs.deq;
    Addr address;
	Rsp_MEM rsp = defaultValue;

	if (request matches tagged Descriptor .r) begin
//    	address = r.addr >> 2;
		address = {12'b0, r.addr[19:0]};
		let alignment = address[1:0];
		address = address >> 2;
		rsp.transaction_id = r.transaction_id;
		rsp.command = r.command;
		Bit #(6) rsize = (zeroExtend(r.burst_size) << 3) + 8;

		if (r.command == READ) begin
			Data data = memory.sub(unpack(address));
			if (alignment == 2'b01) data = data >> 8;
			if (alignment == 2'b10) data = data >> 16;
			if (alignment == 2'b11) data = data >> 24;
			rsp.data = data[rsize-1:0];
			rsp.status = SUCCESS;
		end
		else if (r.command == WRITE) begin
			Data data = memory.sub(unpack(address));
			if (alignment == 2'b00) begin
				case (rsize)
					32: data = r.data;
					24: data = {data[31:24], r.data[23:0]};
					16: data = {data[31:16], r.data[15:0]};
					8:  data = {data[31:8], r.data[7:0]};
				endcase
			end
			if (alignment == 2'b01) begin
				case (rsize)
					24: data = {r.data[23:0], data[7:0]};
					16: data = {data[31:24], r.data[15:0], data[7:0]};
					8:  data = {data[31:16], r.data[7:0], data[7:0]};
				endcase
			end
			if (alignment == 2'b10) begin
				case (rsize)
					16: data = {r.data[15:0], data[15:0]};
					8:  data = {data[31:24], r.data[7:0],  data[15:0]};
				endcase
			end
			if (alignment == 2'b11) begin
				case (rsize)
					8: data = {r.data[7:0], data[23:0]};
				endcase
			end
			// data = data << rsize;
			// data = data | extend(r.data);
			memory.upd(unpack(address), data);
			rsp.status = SUCCESS;
		end
		else begin
			rsp.status = ERROR;
		end
	end
	ff_rsps.enq (rsp);

endrule

interface bus_ifc = toRecvIFC (ff_reqs, ff_rsps);

endmodule

endpackage
