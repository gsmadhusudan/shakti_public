package Memory_Model;

// ================================================================
// Copyright (c) 2013-2014 Bluespec, Inc. All Rights Reserved.

// This package models a Memory serving read/write traffic from a bus.
// It implements two parallel ports into memory.

// 1 Sept 2015: Only NONSEQ data transfers are supported.
//		For RVC, burst_size must be Word (32-bits) or Half-word (16-bits) long.

// ================================================================
// Bluespec libraries

import Vector       :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import DefaultValue :: *;
import SpecialFIFOs :: *;

// ================================================================
// Project imports

import TLM2	    	  :: *;    // Using local copy.
import Utils          :: *;
import Req_Rsp        :: *;
import Sys_Configs    :: *;
import C_import_decls :: *;

`include "TLM.defines"
`include "RVC.defines"

// ================================================================
// Memory model interface

interface Memory_IFC;
   interface Vector #(N_Mem_Ports, TLMRecvIFC #(Req_MEM, Rsp_MEM)) bus_ifc;

   // Debug interfaces
   method Action  initialize  (Addr base_addr, Addr n_bytes, Bool init_from_file);
   method Data    debug_load  (Addr addr, TLMBurstSize #(`TLM_PRM_REQ_MEM) sz);
   method Action  debug_store (Addr a, Data d, TLMBurstSize #(`TLM_PRM_REQ_MEM) sz);
endinterface

// ================================================================
// Memory model

(* synthesize *)
module mkMemory_Model (Memory_IFC);

   Integer verbosity = 0;

   // Incoming memory requests, outgoing responses
   Vector #(N_Mem_Ports, FIFOF #(Req_MEM)) vf_reqs <- replicateM (mkBypassFIFOF);
   Vector #(N_Mem_Ports, FIFOF #(Rsp_MEM)) vf_rsps <- replicateM (mkBypassFIFOF);

   // Memory parameters
   Reg #(Bool) rg_initialized <- mkReg (False);
   Reg #(Addr) rg_base        <- mkRegU;
   Reg #(Addr) rg_limit       <- mkRegU;
   Reg #(Addr) rg_size        <- mkRegU;

   // Pointer to C malloc'd memory
   Reg #(Bit #(64)) rg_p <- mkRegU;	// NOTE: The test machine has an address space of 64-bits. Not the RVC ISA

   // ----------------
   // Check for legal addresses (in range [base..limit-3])

   function ActionValue #(Bool) fn_addr_ok (RequestDescriptor#(`TLM_PRM_REQ_MEM) req);
      actionvalue
	 if (   (req.addr < rg_base)
	     || ((req.addr + extend (reqSz_bytes_mem(req.burst_size)) - 1) > rg_limit)) begin
	    $display ("%0d: ERROR: Memory_model [%0h..%0h]: req addr out of bounds",
		      cur_cycle, rg_base, rg_limit);
	    //display_Req (req);
	    $display ("");
	    return False;
	 end
	 else
	    return True;
      endactionvalue
   endfunction

   // ----------------
   // RULES

   for (Integer j = 0; j < valueOf (N_Mem_Ports); j = j + 1)
      rule rl_handle_reqs (rg_initialized);
	 let request = vf_reqs[j].first; vf_reqs[j].deq;
	 if(request matches tagged Descriptor .req) begin		// Note: Support for Descriptor requests only
 	    let addr_ok <- fn_addr_ok (req);
	    let rsz = reqSz_bytes_mem(req.burst_size);
	    let addr_align = (rsz == 2 || rsz == 4);	// Note: Support for 2 or 4 bytes aligned memory addresses only. Changes would require changes in C_imports.c used to simulate and malloc memory
	    if (!addr_align) $display("%0d: ERROR: Memory_model [%0h..%0h]: req addr should be 2 or 4 bytes aligned", cur_cycle, rg_base, rg_limit);
	    addr_ok = (addr_ok && addr_align);
	    let offset = req.addr - rg_base;
	    Bit #(64) p = rg_p + extend (offset);
		$display($time, " MEM: REQUEST ADDRESS: %h at PORT: %0d", req.addr, j);
	    Rsp_MEM rsp = defaultValue;
	    if (req.command == UNKNOWN) begin
	    	rsp.command = req.command;
		rsp.data    = extend (req.addr);
	       	rsp.status  = UNKNOWN;
	    end
	    else if (addr_ok && (req.command == READ)) begin
		Data x <- c_read (p, extend (rsz));     
		rsp.command = READ;
		rsp.data    = x;
	       	rsp.status  = SUCCESS;
	    end
	    else if (addr_ok && (req.command == WRITE)) begin
		c_write (p, extend (req.data), extend (rsz));
	        rsp.command = WRITE;
	       	rsp.data    = 0;
		rsp.status  = SUCCESS;
	    end
	    else begin
		rsp.command = req.command;
	       	rsp.data    = extend (req.addr);
		rsp.status  = ERROR;
	    end
	    rsp.transaction_id = req.transaction_id;
	    vf_rsps[j].enq (rsp);
	    if (verbosity > 0) begin
		$display ("%0d: Memory_model, port %0d:", cur_cycle, j);
	        //$write   ("    "); display_cpu_Req (desc); $display ();
		$write   ("    "); display_mem_Rsp (rsp); $display ();
	    end
 	 end
	 // Note: Add else part to add support for TLMRequest .Data
      endrule

   // ----------------
   // INTERFACE

   interface bus_ifc = zipWith (toRecvIFC, vf_reqs, vf_rsps);

   method Action initialize  (Addr base_addr, Addr n_bytes, Bool init_from_file);
      Bit #(64) b64 = extend (pack (init_from_file));
      let p    <- c_malloc_and_init (extend (n_bytes), b64);
      rg_p     <= p;
      rg_base  <= base_addr;
      rg_limit <= base_addr + n_bytes - 1;
      rg_size  <= n_bytes;
      rg_initialized <= True;
   endmethod

   method Data debug_load  (Addr addr, TLMBurstSize#(`TLM_PRM_REQ_MEM) sz) if (rg_initialized);
      let offset = addr - rg_base;
      Bit #(64) p = rg_p + extend (offset);
      Data x = c_read_fn (p, extend (reqSz_bytes_mem(sz)));
      return x;
   endmethod

   method Action debug_store (Addr addr, Data d, TLMBurstSize#(`TLM_PRM_REQ_MEM) sz) if (rg_initialized);
      let offset = addr - rg_base;
      Bit #(64) p = rg_p + extend (offset);
      c_write (p, d, extend (reqSz_bytes_mem(sz)));
   endmethod
endmodule

// ================================================================
endpackage: Memory_Model
